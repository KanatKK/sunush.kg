const router = require("express").Router();
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Answer = require("../models/Answer");
const User = require("../models/User");
const Question = require("../models/Question");
const Rating = require("../models/Rating");

router.get('/', async (req, res) => {
    try {
        let query;

        if (req.query.user) {
            query = {user: req.query.user};
        }

        const limit = 4;
        const page = req.query.page;

        let options;

        options = {
            page: page,
            limit: limit,
            populate: [
                "user",
                {
                    path: "question",
                    populate: [
                        'user',
                    ]
                }
            ],
            sort: {datetime: 1},
        };

        const answers = await Answer.paginate(query, options);

        const result = answers.docs.map(async item => {
            const answer = await Answer.find({question: item.question._id});
            item.question.answers = answer.length;
            return item;
        });

        const newResult = await Promise.all(result);
        const finalResult = [];
        const totalCount = await Answer.find(query);
        finalResult.push(newResult);
        finalResult.push({totalCount: totalCount.length})
        res.send(finalResult);
    } catch {
        res.sendStatus(500);
    }
});

router.get("/question/:id", async (req, res) => {
    try {
        const answers = await Answer.find({question: req.params.id}).populate("user");
        const newAnswers = answers.map(async item => {
            const answerRate = await Rating.find({answer: item._id});
            if (answerRate.length > 0) {
                item.rate = answerRate[0].rate;
            } else {
                item.rate = 0;
            }
            return item;
        });

        const newResult = await Promise.all(newAnswers);

        res.send(newResult);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", [auth, permit("lawyer","accountant")], async (req, res) => {
  const answerData = req.body;
  const token = req.get('Authorization');
  const user = await User.findOne({token});

    answerData.datetime = new Date();
    const question = await Question.findById(answerData.question);
    answerData.user = user._id;
    answerData.question = question;
    answerData.rate = 0;

    question.answers = question.answers + 1;
    await Question.findByIdAndUpdate(answerData.question, question);

    const answer = new Answer(answerData);
    try {
        await answer.save();
        res.send(answer);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;