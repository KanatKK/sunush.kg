const router = require("express").Router();
const User = require("../models/User");
const config = require("../config");
const auth = require("../middleware/auth");
const Rating = require("../models/Rating");
const Answer = require("../models/Answer");
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;

router.get("/", async (req, res) => {
    let query;

    const limit = 4;
    const page = req.query.page;

    let options;

    options = {
        page: page,
        limit: limit,
        populate: ["category", "region"],
    };

    if (req.query.category) {
        query = {category: req.query.category};
    }
    if (req.query.phone) {
        query = {phone: req.query.phone};
    }
    if (req.query.region) {
        query = {region: req.query.region};
    }
    if (req.query.role === "lawyer" || req.query.role === "accountant") {
        query = {role: req.query.role};
    }
    if (req.query.role === "lawyer_accountant") {
        query = {role: ["lawyer", "accountant"]};
    }

    try {

        let role;
        if (req.query.role === "lawyer_accountant") {
            role = ["lawyer", "accountant"];
        }
        if (req.query.role !== "lawyer_accountant") {
            role = req.query.role;
        }
        if (req.query.category && req.query.role) {
            query = {$and: [{category: req.query.category}, {role: role}]};
        }
        if (req.query.region && req.query.role) {
            query = {$and: [{region: req.query.region}, {role: role}]};
        }
        if (req.query.region && req.query.category) {
            query = {$and: [{category: req.query.category}, {region: req.query.region}, {role: role}]};
        }
        if (req.query.role && req.query.region && req.query.category) {
            query = {$and: [{role: role}, {region: req.query.region}, {category: req.query.category}]};
        }

        const users = await User.paginate(query, options);
        const result = users.docs.map(async item => {
            const answer = await Answer.find({user: item._id});
            const rating = await Rating.find({lawyer: item._id});
            let ratingValue = 0;
            for (let i = 0; i < rating.length; i++) {
                ratingValue = ratingValue + rating[i].rate;
            }
            item.rating = ratingValue / rating.length;
            item.answers = answer.length;
            return item;
        });
        const newResult = await Promise.all(result);
        const finalResult = [];
        finalResult.push(newResult);
        finalResult.push({totalDocs: users.totalDocs});
        res.send(finalResult);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const result = await User.findById(req.params.id).populate("region category");
        const rating = await Rating.find({lawyer: req.params.id});
        let ratingValue = 0;
        for (let i = 0; i < rating.length; i++) {
            ratingValue = ratingValue + rating[i].rate;
        }
        result.rating = ratingValue / rating.length;
        if (result) {
            res.send(result);
        } else {
            res.sendStatus(404);
        }
    } catch {
        res.sendStatus(500);
    }
});

router.post("/", config.upload.single("image"), async (req, res) => {
    try {
        if (req.body.role === "admin") {
            return res.status(400).send({error: "You can't register this role"});
        }

        let image = req.body.image;

        if (req.file) {
            image = req.file.filename;
        }

        const user = new User({
            fullName: req.body.fullName,
            phone: req.body.phone,
            password: req.body.password,
            email: req.body.email,
            role: req.body.role,
            address: req.body.address,
            website: req.body.website,
            category: req.body.category,
            image: image,
            region: req.body.region
        });

        // const message = {
        //     from: "Sunush.kg <sunush.kg@gmail.com>",
        //     to: req.body.email,
        //     subject: 'Успешная регистрация на сайте Sunush.kg',
        //     html: `<h2>Успешная регистрация на сайте Sunush.kg !!!</h2>
        //            <p>Добро пожаловать на сайт юридических консультаций онлайн!
        //            Если Вам требуется квалифицированная юридическая помощь, то опытные юристы Кыргызстана помогут в этом.
        //            Если же Вы юрист, адвокат или бухгалтер, мы поможем Вам в продвижении услуг, а также в поиске работы.
        //            </p>`
        // }
        // mailer(message);

        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post("/sessions", async (req, res) => {
  const user = await User.findOne({phone: req.body.phone});
  if (!user) {
    return res.status(400).send({error: "User not found"});
  }
  const isMatch = await user.checkPassword(req.body.password);
  if (!isMatch) {
    return res.status(400).send({error: "Password is wrong"});
  }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send(user);
});

router.put('/', config.upload.single("image"), auth, async (req, res) => {
    if (req.file) {
        req.body.image = req.file.filename;
    }

    const token = req.get('Authorization');
    const user = await User.findOne({token});

    try {
        await req.user.update(req.body);
        const updatedUser = await User.findById(user._id);
        res.send({message: "The data is successfully changed", user: updatedUser});
    } catch (e) {
        res.status(400).send(e);
    }
});

router.put('/password', auth, async (req, res) => {
    const token = req.get("Authorization");
    const user = await User.findOne({token});

    if (!user) {
        return res.status(400).send({error: "User not found"});
    }

    console.log(user);

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(req.body.password, salt);
    req.body.password = hash;

    try {
        await User.findByIdAndUpdate(user._id, {password: req.body.password});
        res.send({message: "The data is successfully changed"});
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/sessions", async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: "Success"};

    if (!token) return res.send(success);
    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    user.save({validateBeforeSave: false});

    return res.send(success);
});

router.patch("/chats", auth, async (req, res) => {
    const token = req.get("Authorization");
    const user = await User.findOne({token});
    const chatWith = await User.findOne({_id: req.body.with});
    const userChek = user.chats.find(chat => chat.with.equals(req.body.with));
    const chatWithChek = chatWith.chats.find(chat => chat.with.equals(user._id));
    const chatForAnotherUser = {
        with: user._id,
        nameWith: user.fullName
    };
    const chatWithMe = {
        with: req.body.with,
        nameWith: req.body.nameWith
    }
    if (!userChek && !chatWithChek) {
        try {
            await user.updateOne({$push: {chats: chatWithMe}});
            await chatWith.updateOne({$push: {chats: chatForAnotherUser}});
            res.send({message: "Success"});
        } catch (e) {
            res.status(400).send(e);
        }
    } else {
        res.send({message: "This chat exists"});
    }
});

router.patch("/chats/remove/:id", auth, async (req, res) => {
    const token = req.get("Authorization");
    const user = await User.findOne({token});
    const chatWith = await User.findOne({_id: req.params.id});
    const userChek = user.chats.find(chat => chat.with.equals(req.params.id));
    const chatWithChek = chatWith.chats.find(chat => chat.with.equals(user._id));

    if (userChek && chatWithChek) {
        try {
            await user.updateOne({$pull: {chats: {with: req.params.id}}});
            await chatWith.updateOne({$pull: {chats: {with: user._id}}});
            res.send({message: "Success"});
        } catch (e) {
            res.status(400).send(e);
        }
    } else {
        res.send({message: "This chat does not exists"});
    }
});

module.exports = router;
