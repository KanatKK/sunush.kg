const router = require("express").Router();
const Message = require('../models/Message')
const Notification = require("../models/Notification");
const NewMessageNotice = require("../models/NewMessageNotice");
const User = require("../models/User");

const activeConnections = {};

router.ws("/", async (ws, res) => {
    const from = res.query.from;
    const to = res.query.to;
    const id = res.query.id
    activeConnections[id] = ws;

    ws.on("message", async msg => {
        const decodedMessage = JSON.parse(msg);
        switch (decodedMessage.type) {
            case "GET_ALL_MESSAGES":
                const result = await Message.find({
                    $or: [{$and: [{from: id}, {to: to}]}, {$and: [{from: to}, {to: id}]}]
                }).sort({datetime: 1});

                if (result) {
                    ws.send(JSON.stringify({type: "ALL_MESSAGES", result}));
                } else {
                    ws.send(JSON.stringify({type: "ERROR"}));
                }
                break;
            case "CREATE_MESSAGE":
                let messageData = {};
                messageData.text = decodedMessage.text;
                messageData.from = decodedMessage.from;
                messageData.to = decodedMessage.to;
                messageData.datetime = new Date();
                const newMessage = new Message(messageData);

                try {
                    await newMessage.save();
                } catch (e) {
                    console.log(e);
                }
                activeConnections[id].send(JSON.stringify({
                    type: "NEW_MESSAGE",
                    message: newMessage
                }));

                if (activeConnections[to]) {
                    activeConnections[to].send(JSON.stringify({
                        type: "NEW_MESSAGE",
                        message: newMessage,
                    }));
                }
                break;
            case "CREATE_MESSAGE_WITH_IMAGE":
                const msgImg = await Message.findOne({text: decodedMessage.text});
                activeConnections[id].send(JSON.stringify({
                    type: "NEW_MESSAGE",
                    message: msgImg
                }));

                if (activeConnections[to]) {
                    activeConnections[to].send(JSON.stringify({
                        type: "NEW_MESSAGE",
                        message: msgImg,
                    }));
                }
                break;
            case "START_TYPING":
                if (activeConnections[to]) {
                    activeConnections[to].send(JSON.stringify({
                        type: "START_TYPING",
                    }));
                }
                break;
            case "STOP_TYPING":
                if (activeConnections[to]) {
                    activeConnections[to].send(JSON.stringify({
                        type: "STOP_TYPING",
                    }));
                }
                break;
            case "PUBLIC_QUESTION":
                let notificationData = {};
                notificationData.question = decodedMessage.question;
                notificationData.user = decodedMessage.to;
                notificationData.rusState = decodedMessage.rusState;
                notificationData.datetime = new Date();
                const newNotification = new Notification(notificationData);
                try {
                    await newNotification.save()
                } catch (e) {
                    console.log(e);
                }
                const notif = await Notification.findOne(newNotification)
                    .populate("user").populate("question");
                if (activeConnections[decodedMessage.to]) {
                    activeConnections[decodedMessage.to].send(JSON.stringify({
                        type: "PUBLIC_YOUR_QUESTION",
                        data: notif
                    }));
                }
                break;
            case "DELETE_QUESTION":
                let notificationDelete = {};
                notificationDelete.question = decodedMessage.question;
                notificationDelete.user = decodedMessage.to;
                notificationDelete.rusState = decodedMessage.rusState;
                notificationDelete.datetime = new Date();
                const delNotification = new Notification(notificationDelete);
                try {
                    await delNotification.save()
                } catch (e) {
                    console.log(e);
                }
                const delNotif = await Notification.findOne(delNotification)
                    .populate("user").populate("question");
                if (activeConnections[decodedMessage.to]) {
                    activeConnections[decodedMessage.to].send(JSON.stringify({
                        type: "DELETE_YOUR_QUESTION",
                        data: delNotif
                    }));
                }
                break;
            case "NEW_ANSWER":
                let notificationAnswer = {};
                notificationAnswer.question = decodedMessage.question;
                notificationAnswer.user = decodedMessage.to;
                notificationAnswer.rusState = decodedMessage.rusState;
                notificationAnswer.datetime = new Date();
                const answerNotification = new Notification(notificationAnswer);
                try {
                    await answerNotification.save()
                } catch (e) {
                    console.log(e);
                }
                const notific = await Notification.findOne(answerNotification)
                    .populate("user").populate("question");
                if (activeConnections[decodedMessage.to]) {
                    activeConnections[decodedMessage.to].send(JSON.stringify({
                        type: "ANSWER_YOUR_QUESTION",
                        data: notific
                    }));
                }
                break;
            case "YOU_HAVE_NEW_MESSAGE":
                let newNotice = {};
                newNotice.from = decodedMessage.from;
                newNotice.to = decodedMessage.to;
                const newMessageNotice = new NewMessageNotice(newNotice);

                const noticeChek = await NewMessageNotice.findOne(newNotice);

                if (!noticeChek) {
                    try {
                        await newMessageNotice.save();
                    } catch (e) {
                        console.log(e);
                    }
                    const newNoticeMsg = await NewMessageNotice.findOne(newMessageNotice);
                    const interlocutor = await User.findOne({_id: decodedMessage.from});

                    if (activeConnections[decodedMessage.to]) {
                        activeConnections[decodedMessage.to].send(JSON.stringify({
                            type: "HAVE_NEW_MESSAGE",
                            notice: newNoticeMsg,
                            interlocutor: {with: interlocutor._id, nameWith: interlocutor.fullName}
                        }));
                    }
                } else {
                    console.log("Notice already exist...");
                }
                break;
            default:
                console.log("Unknown message type:", decodedMessage.type);
        }
    });

    ws.on("close", msg => {
        delete activeConnections[id];
    });
});

module.exports = router;