const express = require('express');
const router = express.Router();
const config = require("../config");
const Message = require("../models/Message");
const auth = require('../middleware/auth');

router.post('/', [auth, config.upload.single("image")], async (req, res) => {
    const messageData = req.body;
    messageData.datetime = new Date();
    if (req.file) {
        messageData.image = req.file.filename;
    }
    const message = new Message(messageData)
    try {
        await message.save();
        res.send(message);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;