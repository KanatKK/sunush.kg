const router = require("express").Router();
const Category = require("../models/Category");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");


router.get('/', async (req, res) => {
    try {
        const categories = await Category.find();
        res.send(categories);
    } catch {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const result = await Category.findById(req.params.id);
        if (result) {
            res.send(result);
        } else {
            res.sendStatus(404);
        }
    } catch {
        res.sendStatus(500);
    }
});

router.post("/", [auth, permit("admin")], async (req, res) => {
    const categoryData = req.body;
    const category = new Category(categoryData);
    try {
        await category.save();
        res.send(category);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', [auth, permit("admin")], async (req, res) => {
    const result = await Category.findByIdAndRemove({_id: req.params.id});
    if (result) {
        res.send("Category removed");
    } else {
        res.sendStatus(404);
    }
});

module.exports = router;