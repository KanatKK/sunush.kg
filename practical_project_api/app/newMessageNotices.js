const router = require("express").Router();
const auth = require("../middleware/auth");
const NewMessageNotice = require("../models/NewMessageNotice");

router.get("/", auth, async (req, res) => {
    const result = await NewMessageNotice.find({to: req.user._id});
    try {
        res.send(result);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete("/", auth, async (req, res) => {
    try {
        await NewMessageNotice.findOneAndDelete(req.body);
        res.send({message: "Success"});
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;