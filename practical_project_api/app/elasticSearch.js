const router = require("express").Router();
const elasticsearch = require("elasticsearch");
const Question = require("../models/Question");
const Answer = require("../models/Answer");

const esClient = elasticsearch.Client({
    host: "http://es:9200"
});

router.get("/", async (req, res) => {
    let searchText = req.query.text
    const response = await esClient.search({
        index: "questions",
        body: {
            "query": {
                    wildcard: {
                        "title": {
                            "value": `*${searchText}*`,
                        }
                    }
                }
        }
    });

    const newResponse = response.hits.hits;

    let newQuery = [];

    for (let i = 0; i < newResponse.length; i++) {
        const questions = await Question.findOne({newId : newResponse[i]._source.newId, published: true, deleted: false}).sort({datetime: 1}).populate("user").populate("category").populate("region");
        if(questions !== null) {
            newQuery.push(questions)
        }
    }

    const result = newQuery.map(async item => {
        const answer = await Answer.find({question: item._id});
        item.answers = answer.length;
        return item;
    });
    const newResult = await Promise.all(result);

    res.send(newResult);
});

module.exports = router;