const path = require("path");
const rootPath = __dirname;
const multer = require("multer");
const {nanoid} = require("nanoid");
let elasticsearch = require("elasticsearch");

let client = new elasticsearch.Client({
  hosts: ["http://es:9200"]
});

client.ping(
    {
      requestTimeout: 30000
    },
    function(error) {
      if (error) {
        console.error("Cannot connect to Elasticsearch.");
        console.error(error);
      } else {
        console.log("Connected to Elasticsearch was successful!");
      }
    }
);

module.exports = client;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, path.join(rootPath, "public/uploads"));
  },
  filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname));
  }
});

let dbName = "practical_project_api";

if (process.env.NODE_ENV === "test") {
  dbName = "practical_project_api_test";
}

const upload = multer({storage});

module.exports = {
  upload,
  rootPath,
  options: {
    useNewUrlParser: true,
    autoIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
  },
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "practical_project_api",
    url: "mongodb://mongo"
  }
};