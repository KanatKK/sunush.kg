const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");
const Category = require('./models/Category');
const User = require('./models/User');
const Question = require("./models/Question");
const Answer = require("./models/Answer");
const Region = require("./models/Region");
const axios = require("axios");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

const elasticsearch = require("elasticsearch")

db.once("open", async () => {
    try {
        await axios.delete("http://es:9200/_all");
        await db.dropCollection("categories");
        await db.dropCollection("questions");
        await db.dropCollection("answers");
        await db.dropCollection("users");
        await db.dropCollection("regions");
        await db.dropCollection("messages");
        await db.dropCollection("ratings");
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const esClient = elasticsearch.Client({
        host: "http://localhost:9200",
    });

    const elasticQuestions = [{
        title: "Question 1",
        newId: "Eh_Hooq0Cwm2A1Haef7f6"
    }, {
        title: "Question 2",
        newId: "Eh_Hooq0Cwm2A1Haef7f7"
    }, {
        title: "Question 3",
        newId: "Eh_Hooq0Cwm2A1Haef7f8"
    }, {
        title: "Question 4",
        newId: "Eh_Hooq0Cwm2A1Haef7f9"
    }];

    for (let i = 0; i < elasticQuestions.length; i++) {
        esClient.index({
            index: 'questions',
            body: {
                "title": elasticQuestions[i].title,
                "newId": elasticQuestions[i].newId
            }
        })
    }

    const [civilLaw, administrativeLaw, another, landLaw] = await Category.create({
        rusTitle: "Гражданское право",
        kgTitle: "Жарандык укук",
        engTitle: "Civil law",
    }, {
        rusTitle: "Административное право",
        kgTitle: "Административдик укук",
        engTitle: "Administrative law",
    }, {
        rusTitle: "Другое",
        kgTitle: "Башка",
        engTitle: "Others",
    }, {
        rusTitle: "Земельное право",
        kgTitle: "Жер укук",
        engTitle: "Land law",
    });

    const [ChuiRegion, BatkenRegion, JalalAbadRegion, NarynRegion, IssykKulRegion, OshRegion, TalasRegion] = await Region.create(
        {
            rusTitle: "Чуйская область",
            kgTitle: "Чуй облусу",
            engTitle: "Chui region",
        }, {
            rusTitle: "Баткенская область",
            kgTitle: "Баткен облусу",
            engTitle: "Batken region",
        }, {
            rusTitle: "Джалал-Абадская право",
            kgTitle: "Джалал-Абад облусу",
            engTitle: "Jalal-Abad region",
        }, {
            rusTitle: "Нарынская область",
            kgTitle: "Нарын облусу",
            engTitle: "Naryn region",
        }, {
            rusTitle: "Иссык-Кульская область",
            kgTitle: "Ыссык-Кол облусу",
            engTitle: "Issyk-Kol region",
        }, {
            rusTitle: "Ошская область",
            kgTitle: "Ош облусу",
            engTitle: "Osh region",
        }, {
            rusTitle: "Таласская область право",
            kgTitle: "Талас облусу",
            engTitle: "Talas region",
        });

    const [user, user2, admin, moderator, lawyer, lawyer2, accountant] = await User.create({
        username: "user_anakin",
        phone: "555123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "user",
        chats: [],
        fullName: "Энакин Скайуокер",
    }, {
        username: "user_dart",
        phone: "777123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "user",
        chats: [],
        fullName: "Дарт Вейдер",
    }, {
        username: "admin",
        phone: "111111111",
        password: "12345678Kk",
        token: nanoid(),
        role: "admin",
        fullName: "admin",
        chats: [],
    }, {
        username: "moderator",
        phone: "222222222",
        password: "12345678Kk",
        token: nanoid(),
        role: "moderator",
        fullName: "moderator",
    }, {
        username: "lawyer",
        phone: "707123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "lawyer",
        category: civilLaw._id,
        city: "Бишкек",
        website: "law.kg",
        chats: [],
        address: "Советская 88",
        fullName: "Иванов Иван Иванович",
        image: "1.jpeg",
        experience: "5",
        email: "ivanov@gmail.com",
        region: ChuiRegion._id,
    }, {
        phone: "705123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "lawyer",
        category: civilLaw._id,
        city: "Бишкек",
        website: "law.kg",
        chats: [],
        address: "Советская 88",
        fullName: "No Name 1",
        image: "1.jpeg",
        experience: "5",
        email: "ivanov@gmail.com",
        region: JalalAbadRegion._id,
    }, {
        phone: "505123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "lawyer",
        category: landLaw._id,
        city: "Бишкек",
        chats: [],
        website: "law.kg",
        address: "Советская 99",
        fullName: "Антонов Антон Антонович",
        image: "1.jpeg",
        experience: "5",
        email: "antonov@gmail.com",
        region: NarynRegion._id,
    }, {
        phone: "501123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "lawyer",
        category: landLaw._id,
        city: "Бишкек",
        website: "law.kg",
        chats: [],
        address: "Советская 99",
        fullName: "No Name 2",
        image: "1.jpeg",
        experience: "5",
        email: "antonov@gmail.com",
        region: IssykKulRegion._id,
    }, {
        phone: "504123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "lawyer",
        category: landLaw._id,
        city: "Бишкек",
        website: "law.kg",
        chats: [],
        address: "Советская 99",
        fullName: "No Name 3",
        image: "1.jpeg",
        experience: "5",
        email: "antonov@gmail.com",
        region: OshRegion._id,
    }, {
        phone: "701123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "accountant",
        category: administrativeLaw._id,
        city: "Бишкек",
        chats: [],
        website: "accountant.kg",
        address: "Советская 99",
        fullName: "Даниилов Даниил Даниилович",
        image: "1.jpeg",
        experience: "5",
        email: "danilov@gmail.com",
        region: NarynRegion._id,
    }, {
        phone: "704123456",
        password: "12345678Kk",
        token: nanoid(),
        role: "accountant",
        category: administrativeLaw._id,
        city: "Бишкек",
        website: "accountant.kg",
        address: "Советская 99",
        fullName: "No Name 4",
        image: "1.jpeg",
        experience: "5",
        email: "danilov@gmail.com",
        region: BatkenRegion._id,
    });
    const [
        question1, question2,
        question3, question4,
        question5, question6,
        question7, question8] = await Question.create({
        title: "Question 1",
        description: "question 1 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:23:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: ChuiRegion._id,
        deleted: false,
        newId: "Eh_Hooq0Cwm2A1Haef7f6",
        answers: 1,
    }, {
        title: "Question 2",
        description: "question 2 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:24:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: true,
        region: OshRegion._id,
        deleted: false,
        answers: 1,
        newId: "Eh_Hooq0Cwm2A1Haef7f7"
    }, {
        title: "Question 3",
        description: "question 3 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:25:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: TalasRegion._id,
        deleted: false,
        answers: 1,
        newId: "Eh_Hooq0Cwm2A1Haef7f8"
    }, {
        title: "Question 4",
        description: "question 4 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:26:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: true,
        region: BatkenRegion._id,
        deleted: false,
        newId: "Eh_Hooq0Cwm2A1Haef7f9",
        answers: 1,
    },{
        title: "Question 5",
        description: "question 1 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:27:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: ChuiRegion._id,
        deleted: false,
        answers: 1,
    }, {
        title: "Question 6",
        description: "question 2 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:28:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: true,
        region: OshRegion._id,
        deleted: false,
        answers: 1,
    }, {
        title: "Question 7",
        description: "question 3 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:29:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: TalasRegion._id,
        deleted: false,
        answers: 1,
    }, {
        title: "Question 8",
        description: "question 4 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:30:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: true,
        region: BatkenRegion._id,
        deleted: false,
        answers: 1,
    },{
        title: "Question 9",
        description: "question 1 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:31:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: ChuiRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 10",
        description: "question 2 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:32:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: false,
        region: OshRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 11",
        description: "question 3 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:33:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: false,
        region: TalasRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 12",
        description: "question 4 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:34:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: false,
        region: BatkenRegion._id,
        deleted: false,
        answers: 0,
    },{
        title: "Question 13",
        description: "question 1 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:35:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: true,
        region: ChuiRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 14",
        description: "question 2 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:36:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: false,
        region: OshRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 15",
        description: "question 3 description",
        category: civilLaw._id,
        datetime: "Thu Dec 24 2020 20:37:05 GMT+0600 (Kyrgyzstan Time)",
        user: user._id,
        published: false,
        region: TalasRegion._id,
        deleted: false,
        answers: 0,
    }, {
        title: "Question 16",
        description: "question 4 description",
        category: administrativeLaw._id,
        datetime: "Thu Dec 24 2020 20:38:05 GMT+0600 (Kyrgyzstan Time)",
        user: user2._id,
        published: false,
        region: BatkenRegion._id,
        deleted: false,
        answers: 0,
    });

    await Answer.create({
            answer: "answer to question 1",
            question: question1._id,
            datetime: "Thu Dec 24 2020 21:26:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer._id,
            rate: 0
        }, {
            answer: "answer to question 2",
            question: question2._id,
            datetime: "Thu Dec 24 2020 22:26:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer2._id,
            rate: 0
        }, {
            answer: "answer to question 3",
            question: question3._id,
            datetime: "Thu Dec 24 2020 23:26:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer._id,
            rate: 0
        }, {
            answer: "answer to question 4",
            question: question4._id,
            datetime: "Thu Dec 24 2020 23:36:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer2._id,
            rate: 0
        }, {
            answer: "answer to question 4",
            question: question5._id,
            datetime: "Thu Dec 24 2020 23:39:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer._id,
            rate: 0
        }, {
            answer: "answer to question 4",
            question: question6._id,
            datetime: "Thu Dec 24 2020 23:40:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer2._id,
            rate: 0
        }, {
            answer: "answer to question 4",
            question: question7._id,
            datetime: "Thu Dec 24 2020 23:43:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer._id,
            rate: 0
        }, {
            answer: "answer to question 4",
            question: question8._id,
            datetime: "Thu Dec 24 2020 23:56:05 GMT+0600 (Kyrgyzstan Time)",
            user: lawyer2._id,
            rate: 0
        }
    )
    db.close();
});