const mongoose = require("mongoose");
const {app, port} = require("./app");
const config = require("./config");

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, config.options);

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);
