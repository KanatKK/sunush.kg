const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const NewMessageNoticeSchema = new Schema({
    from: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    to: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
});

const NewMessageNotice = mongoose.model("NewMessageNotice", NewMessageNoticeSchema);
module.exports = NewMessageNotice;