const mongoose = require("mongoose");
const mongoosePagination = require("mongoose-paginate-v2");

const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
    answer: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    question: {
        type: Schema.Types.ObjectId,
        ref: "Question",
        required: true
    },
    datetime: String,
    image: String,
    rate: Number
});

AnswerSchema.plugin(mongoosePagination);

const Answer = mongoose.model("Answer", AnswerSchema);
module.exports = Answer;