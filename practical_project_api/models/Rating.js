const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const RatingSchema = new Schema({
    lawyer: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    answer: {
        type: Schema.Types.ObjectId,
        ref: "Answer",
        required: true
    },
    rate: {
        type: Number,
        required: true
    },
});

const Rating = mongoose.model("Rating", RatingSchema);
module.exports = Rating;