const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const Answer = require("../models/Answer");
const Question = require("../models/Question");
const { app, port } = require("../app");

describe("route users", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение ответов", async () => {
        const res = await request(app).get("/answers");
        expect(res.statusCode).toBe(200);
        expect(res.body.length).toBeGreaterThan(0);
    });

    test("успешное добавление ответа", async () => {
        const user = {
            phone: "707123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const question = await Question.find();
        const answer = {
            answer: "test answer",
            user: login.body._id,
            question: question[0]._id
        };
        const res = await request(app).post("/answers").send(answer).set({"Authorization": login.body.token});
        expect(res.body.answer).toBe(answer.answer);
    });

    test("неуспешное добавление ответа", async () => {
        const user = {
            phone: "707123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const question = await Question.find();
        const answer = {
            user: login.body._id,
            question: question[0]._id
        };
        const res = await request(app).post("/answers").send(answer).set({"Authorization": login.body.token});
        expect(res.body.errors.answer.message).toBe('Path `answer` is required.');
    });

    test("успешное получение ответов на вопрос", async () => {
        const question = await Question.find();
        const res = await request(app).get("/answers/question/" + question[0]._id);
        expect(res.body.length).toBeGreaterThan(0);
        expect(res.statusCode).toBe(200);
    });

    test("неуспешное получение ответов на вопрос", async () => {
        const res = await request(app).get("/answers/question/" + "602e52a913286d4b35a07");
        expect(res.statusCode).toBe(500);
    });
});

