const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const User = require("../models/User");
const Answer = require("../models/Answer");
const Rating = require("../models/Rating");
const {app, port} = require("../app");

describe("route categories", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение оценок", async () => {
        const res = await request(app).get("/ratings");
        expect(res.statusCode).toBe(200);
    });

    test("оценка ответа", async () => {
        const lawyer = await User.find({role: 'lawyer'});
        const user = await User.find({role: 'user'});
        const answer = await Answer.find({user: lawyer[0]._id});
        const rate = {
            user: user[0]._id,
            lawyer: lawyer[0]._id,
            rate: 3,
            answer: answer[0]._id
        };
        const res = await request(app).post("/ratings/").send(rate);
        expect(res.body.message).toBe('Success');
        const removeRate = Rating.find(rate);
        await Rating.findByIdAndRemove(removeRate._id);
    });
});