const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const Region = require("../models/Region");
const { app, port } = require("../app");

describe("route regions", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение списка регионов", async () => {
        const res = await request(app).get("/regions");
        expect(res.statusCode).toBe(200);
        expect(res.body.length > 0);
    });

    test("получение региона по id", async () => {
        const region = await Region.create({
            rusTitle: "rus test region",
            kgTitle: "kg test region",
            engTitle: "eng test region"
        });

        const res = await request(app).get("/regions/" + region._id);

        expect(res.body.rusTitle).toBe("rus test region");
        expect(res.body.kgTitle).toBe("kg test region");
        expect(res.body.engTitle).toBe("eng test region");

        await Region.findByIdAndRemove(region._id);
    });

    test("неуспешное получение региона по id", async () => {
        const res = await request(app).get("/regions/" + "6026a94d8298f363cf66a737");
        expect(res.statusCode).toBe(404);
    });
});