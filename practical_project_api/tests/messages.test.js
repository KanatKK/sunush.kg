const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const User = require("../models/User");
const { app, port } = require("../app");

describe("route messages", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("отправление сообщения", async () => {
        const user = {
            phone: "707123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const interlocutor = await User.findOne({phone: "555123456"});

        const message = {
            from: login.body._id,
            to: interlocutor._id,
            text: 'test message'
        };

        const res = await request(app).post("/messages").send(message).set({"Authorization": login.body.token});

        expect(res.body.text).toBe(message.text);
    });
});