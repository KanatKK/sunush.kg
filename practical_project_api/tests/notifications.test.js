const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const User = require("../models/User");
const Question = require("../models/Question");
const Notification = require("../models/Notification");
const {app, port} = require("../app");

describe("route messages", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение уведомлений", async () => {
        const user = {
            phone: "707123456",
            password: "12345678Kk",
        };
        const questions = await Question.find();
        const login = await request(app).post("/users/sessions").send(user);
        const notice = await Notification.create({
            viewed: false,
            question: questions[0].id,
            user: login.body._id,
            rusState: "У вас есть новые ответы",
            datetime: "2021-02-12T16:22:42.278+00:00"
        });

        const res = await request(app).get("/notifications/" + login.body._id).set({"Authorization": login.body.token});
        expect(res.body.length).toBeGreaterThan(0);
        await Notification.findByIdAndRemove(notice._id);
    });

    test("просмотр уведомления", async () => {
        const user = {
            phone: "707123456",
            password: "12345678Kk",
        };
        const questions = await Question.find();
        const login = await request(app).post("/users/sessions").send(user);
        const notice = await Notification.create({
            viewed: false,
            question: questions[0].id,
            user: login.body._id,
            rusState: "У вас есть новые ответы",
            datetime: "2021-02-12T16:22:42.278+00:00"
        });

        const res = await request(app).patch("/notifications/" + notice._id).send({viewed: true})
            .set({"Authorization": login.body.token});
        expect(res.body.message).toBe("Success");
    });
});