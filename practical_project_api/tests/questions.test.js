const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const Question = require("../models/Question");
const Region = require("../models/Region");
const Category = require("../models/Category");
const User = require("../models/User");
const {app, port} = require("../app");

describe("route questions", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("успешное отправоение вопроса", async () => {
        const user = {
            phone: "777123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const category = await Category.find();
        const region = await Region.find();
        const question = {
            title: 'test title',
            description: 'test Description',
            category: category[0]._id,
            region: region[0]._id,
        };

        const res = await request(app).post("/questions").send(question).set({"Authorization": login.body.token});
        expect(res.body.title).toBe("test title");
        await request(app).delete("/users/sessions");
    });

    test("неуспешное отправление вопроса", async () => {
        const user = {
            phone: "777123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const category = await Category.find();
        const region = await Region.find();
        const question = {
            description: 'test Description',
            category: category[0]._id,
            region: region[0]._id,
        };

        const res = await request(app).post("/questions").send(question).set({"Authorization": login.body.token});
        expect(res.body.errors.title.message).toBe('Path `title` is required.');
        await request(app).delete("/users/sessions");
    });

    test("успешное редактирование вопроса", async () => {
        const user = {
            phone: "222222222",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const category = await Category.find();
        const region = await Region.find();
        const question = await Question.create({
            title: 'test title',
            datetime: "Thu Dec 24 2020 20:23:05 GMT+0600 (Kyrgyzstan Time)",
            category: category[0]._id,
            description: 'test description',
            user: login.body._id,
            published: true,
            region: region[0]._id,
            deleted: false,
        });

        const editedQuestion = {
            title: 'edited test title',
        };

        const res = await request(app).patch("/questions/" + question._id).send(editedQuestion).set({"Authorization": login.body.token});

        expect(res.body.message).toBe('Success');
    });

    test("получение вопросов", async () => {
       const res = await request(app).get("/questions");
        expect(res.body.length).toBeGreaterThan(0);
    });
    test("получение вопросов с ответами", async () => {
        const res = await request(app).get("/questions/sort?answers=with_answers");
        expect(res.body.length).toBeGreaterThan(0);
    });
    test("получение вопросов без ответов", async () => {
        const res = await request(app).get("/questions/sort?answers=no_answers");
        expect(res.body.length).toBeGreaterThan(0);
    });
    test("удаление вопроса", async () => {
        const user = {
            phone: "777123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const category = await Category.find();
        const region = await Region.find();
        const question = await Question.create({
            title: 'test title',
            datetime: "Thu Dec 24 2020 20:23:05 GMT+0600 (Kyrgyzstan Time)",
            category: category[0]._id,
            description: 'test description',
            user: login.body._id,
            published: true,
            region: region[0]._id,
            deleted: false,
        });
        const res = await request(app).delete("/questions/delete/" + question._id).set({"Authorization": login.body.token});

        expect(res.body.message).toBe('Deleted');
    });
    test("получение неопубликованных вопросов модератором", async () => {
        const user = {
            phone: "222222222",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const res = await request(app).get("/questions/unpublished/all").set({"Authorization": login.body.token});
        expect(res.body.length).toBeGreaterThan(0);
    });
    test("успешное получение одного вопроса", async () => {
        const user = await User.find();
        const category = await Category.find();
        const region = await Region.find();
        const question = await Question.create({
            title: 'test title',
            datetime: "Thu Dec 24 2020 20:23:05 GMT+0600 (Kyrgyzstan Time)",
            category: category[0]._id,
            description: 'test description',
            user: user[0]._id,
            published: true,
            region: region[0]._id,
            deleted: false,
        });
        const res = await request(app).get("/questions/" + question._id);
        expect(res.body.title).toBe('test title');
    });
});