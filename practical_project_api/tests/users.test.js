const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const User = require("../models/User");
const { app, port } = require("../app");

describe("route users", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("успешная регистрация пользователя", async () => {
        const userData = {
            fullName: "test User",
            phone: "707123434",
            password: "12345678Kk",
            role: "user",
        };

        const res = await request(app).post('/users').send(userData);

        expect(res.body.fullName).toBe(userData.fullName);
        await User.findByIdAndRemove(res.body._id)
    });

    test("попытка регистрации админа", async () => {
        const userData = {
            fullName: "test User",
            phone: "707123434",
            password: "12345678Kk",
            role: "admin",
        };

        const res = await request(app).post('/users').send(userData);
        expect(res.body.error).toBe("You can't register this role");
    });

    test("ошибка регистрации, слабый пароль", async () => {
        const userData = {
            fullName: "test User",
            phone: "707123434",
            password: "1278Kk",
            role: "user",
        };

        const res = await request(app).post('/users').send(userData);
        expect(res.body.errors);
    });

    test("получение всех пользователей", async () => {
        const res = await request(app).get('/users');
        expect(res.statusCode).toBe(200);
        expect(res.body.length > 0);
    });

    test("успешное получение пользователя по id", async () => {
       const testUser = await User.create({
           fullName: "test User",
           phone: "707123434",
           password: "12345678Kk",
           role: "user",
           token: 1234567890
       });

        const res = await request(app).get('/users/' + testUser._id);
        expect(res.body.phone).toBe(testUser.phone);
        await User.findByIdAndRemove(testUser._id);
    });

    test("неуспешное получение пользователя по id", async () => {
        const res = await request(app).get('/users/' + "6026a94d8298f363cf66a737");
        expect(res.statusCode).toBe(500);
    });

    test("успешный логин", async () => {
      const user = {
          phone: "555123456",
          password: "12345678Kk",
      };

        const res = await request(app).post("/users/sessions").send(user);
        expect(res.body.phone).toBe(user.phone);
    });

    test("неуспешный логин, неверный пароль", async () => {
        const user = {
            phone: "555123456",
            password: "12785678Kk",
        };

        const res = await request(app).post("/users/sessions").send(user);
        expect(res.body.error).toBe("Password is wrong");
    });

    test("неуспешный логин, неверное имя", async () => {
        const user = {
            phone: "55512456",
            password: "12345678Kk",
        };

        const res = await request(app).post("/users/sessions").send(user);
        expect(res.body.error).toBe('User not found');
    });

    test("выход из аккаунта", async () => {
        const user = {
            phone: "555123456",
            password: "12345678Kk",
        };

        const token = await request(app).post("/users/sessions").send(user);
        const res = await request(app).delete("/users/sessions");
        expect(res.body.message).toBe("Success");
    });

    test("добавление чата с пользователем", async () => {
        const user = {
            phone: "555123456",
            password: "12345678Kk",
        };

        const token = await request(app).post("/users/sessions").send(user);
        const interlocutor = await User.findOne({phone: 707123456});
        const chat = {with: interlocutor._id, withName: interlocutor.fullname};

        const res = await request(app).patch("/users/chats").send(chat).set({"Authorization": token.body.token});

        expect(res.body.message);
    });

    test("удаление чата с пользователем", async () => {
        const user = {
            phone: "555123456",
            password: "12345678Kk",
        };

        const token = await request(app).post("/users/sessions").send(user);
        const interlocutor = await User.findOne({phone: 707123456});

        const res = await request(app).patch("/users/chats/remove/" + interlocutor._id).set({"Authorization": token.body.token});
        expect(res.body.message);
    });
});