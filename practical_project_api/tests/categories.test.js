const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const Category = require("../models/Category");
const { app, port } = require("../app");

describe("route categories", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение списка категорий", async () => {
        const res = await request(app).get("/categories");
        expect(res.statusCode).toBe(200);
        expect(res.body.length > 0);
    });

    test("получение категории по id", async () => {
        const category = await Category.create({
            rusTitle: "rus test",
            kgTitle: "kg test",
            engTitle: "eng test"
        });

        const res = await request(app).get("/categories/" + category._id);
        expect(res.body.rusTitle).toBe("rus test");
        expect(res.body.kgTitle).toBe("kg test");
        expect(res.body.engTitle).toBe("eng test");

        await Category.findByIdAndRemove(category._id);
    });

    test("неуспешное получение категории по id", async () => {
        const res = await request(app).get("/categories/" + "6026a94d8298f363cf66a737");
        expect(res.statusCode).toBe(404);
    });
});