const request = require("supertest");
const mongoose = require("mongoose");
const config = require("../config");
const User = require("../models/User");
const NewMessageNotice = require("../models/NewMessageNotice");
const { app, port } = require("../app");

describe("route newMessageNotices", () => {
    const server = app.listen(port);

    beforeAll(async (done) => {
        await mongoose.connect("mongodb://localhost/" + config.db.name, config.options);
        done();
    });

    afterAll((done) => {
        mongoose.connection.close(() => done());
        server.close();
    });

    test("получение уведомления", async () => {
        const user = {
            phone: "777123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const interlocutor = await User.findOne({phone: "555123456"});

        const newNotice = await NewMessageNotice.create({
            to: login.body._id,
            from: interlocutor._id,
        });

        const res = await request(app).get("/newMessageNotices").set({"Authorization": login.body.token});
        expect(res.body.length).toBeGreaterThan(0);
        await NewMessageNotice.findByIdAndRemove(newNotice._id)
    });

    test("удаление уведомления", async () => {
        const user = {
            phone: "505123456",
            password: "12345678Kk",
        };

        const login = await request(app).post("/users/sessions").send(user);
        const interlocutor = await User.findOne({phone: "555123456"});

        const newNotice = await NewMessageNotice.create({
            to: login.body._id,
            from: interlocutor._id,
        });

        const res = await request(app).delete("/newMessageNotices").send({
            to: login.body._id,
            from: interlocutor._id,
        }).set({"Authorization": login.body.token});
        expect(res.body.message).toBe("Success");
    });
});