const express = require("express");
const cors = require("cors");

const app = express();
require("express-ws")(app);

const questions = require("./app/questions");
const categories = require("./app/categories");
const answers = require("./app/answers");
const ratings = require("./app/ratings");
const users = require("./app/users");
const regions = require("./app/regions");
const socket = require("./app/socket");
const notifications = require("./app/notifications");
const newMessageNotices = require("./app/newMessageNotices");
const messages = require("./app/messages");
const port = process.env.NODE_ENV === "test" ? 8020 : 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

app.use("/questions", questions);
app.use("/users", users);
app.use("/categories", categories);
app.use("/answers", answers);
app.use("/regions", regions);
app.use("/ratings", ratings);
app.use("/socket", socket);
app.use("/notifications", notifications);
app.use("/newMessageNotices", newMessageNotices);
app.use("/messages", messages);

module.exports = {app, port};
