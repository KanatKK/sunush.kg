import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {fetchUnpublishedQuestions, getUnpublishedQuestionsSuccess} from "../../store/actions/questionActions";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "120px"
    },
    card: {
        fullWidth: "100%",
        padding: "20px",
        backgroundColor: "#f6f2f2",
        marginBottom: "20px",
        boxShadow: "5px 5px 17px 0px rgba(60, 117, 50, 0.73)",
        borderRadius: "5px",
        position: "relative"
    },
}));

const UnderConsiderationQuestions = () => {
    const classes = useStyles();
    const questions = useSelector(state => state.questions.unpublishedQuestions);
    const user = useSelector(state => state.user.user);
    const totalQuestions = useSelector(state => state.questions.totalQuestions);
    const dispatch = useDispatch();

    const [currentPage, setCurrentPage] = useState(1);
    const [questionsLength, setQuestionsLength] = useState(0);
    const [fetching, setFetching] = useState(true);
    const [allQuestions, setAllQuestions] = useState([]);

    useEffect(() => {
        if (fetching) {
            setCurrentPage(prevState => prevState + 1);
            dispatch(fetchUnpublishedQuestions(currentPage));
            if (questions) {
                setQuestionsLength(prevState => prevState + questions.length);
            }
            setFetching(false);
        }
    }, [dispatch, fetching]);

    useEffect(() => {
        if (questions) {
            const allQuestionsCopy = [...allQuestions];
            questions.map(question => {
                allQuestionsCopy.push(question);
                return allQuestionsCopy;
            });
            setAllQuestions(allQuestionsCopy);
            dispatch(getUnpublishedQuestionsSuccess(null));
        }
    }, [questions]);

    useEffect(() => {
        document.addEventListener('scroll', scrollHandler);
        return function () {
            document.removeEventListener('scroll', scrollHandler);
        }
    });

    const scrollHandler = (e) => {
        if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
            && questionsLength < totalQuestions) {
            setFetching(true);
        }
    };

    return (
        <div className={classes.root}>
            {allQuestions && allQuestions.map(question => {
                if (question.published || user.role === 'admin' || user.role === 'moderator') {
                    return (
                        <QuestionCard
                            key={question._id}
                            classname={classes.card}
                            description={question.description}
                            username={question.user.fullName}
                            datetime={question.datetime}
                            id={question._id}
                            published={question.published}
                            title={question.title}
                            userId={question.user._id}
                        />
                    )
                } else return null
            })}
            <AboutProjectBtn/>
        </div>
    );
};

export default UnderConsiderationQuestions;