import React, {useEffect, useState, useRef} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import PostAddIcon from '@material-ui/icons/PostAdd';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch, useSelector} from 'react-redux';
import Container from '@material-ui/core/Container';
import {FormControl, InputLabel, MenuItem, Select} from '@material-ui/core';
import {fetchAddQuestion} from '../../store/actions/questionActions';
import {getCategories} from "../../store/actions/categoriesActions";
import {getRegions} from "../../store/actions/regionsActions";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation
    from "../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(12),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    remove: {
        cursor: "pointer",
        '&:hover': {
            color: "red"
        }
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(1, 0, 1),
    },
    formControl: {
        margin: "0 auto 15px",
        width: '98.5%'
    },
    label: {
        width: '100%',
        padding: '9px'
    },
    input: {
        '&::placeholder': {
            color: 'blue',
        }
    },
    imagesBlock: {
        display: "flex",
        flexWrap: "wrap",
        width: "98.5%",
        margin: "0 auto"
    },
    image: {
        display: "block",
        width: "150px",
        height: "150px",
        margin: "0 auto",
    },
    img: {
        marginRight: "10px",
        border: "1px solid black",
        width: "152px",
        height: "152px",
        position: "relative"
    },
    delete: {
        position: "absolute",
        top: "0",
        right: "0",
        '&:hover': {
            color: "red"
        }
    },
}));

export default function AddApplication() {
    const classes = useStyles();
    const inputRef = useRef();
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);
    const regions = useSelector(state => state.regions.regions);
    const [inputData, setInputData] = useState({
        title: '',
        description: '',
        category: '',
        region: '',
        image: [],
        answers: 0,
    });

    useEffect(() => {
        dispatch(getCategories());
        dispatch(getRegions());
    }, [dispatch]);

    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;
        setInputData({
            ...inputData,
            [name]: value,
        });
    };

    const [images, setImages] = useState({images: []});

    const fileChangeHandler = e => {
        const files = e.target.files;
        if (files) {
            const inputDataCopy = {...inputData};
            const copyImg = inputDataCopy.image;
            for (const key in files) {
                if (typeof files[key] === "object") {
                    copyImg.push(files[key]);
                }
            }
            setInputData({
                ...inputData,
                image: copyImg
            });
            if (window.FileList && window.File && window.FileReader) {
                for (const key in files) {
                    let reader = new FileReader();
                    reader.addEventListener('load', (event) => {
                        const imagesCopy = images.images;
                        imagesCopy.push(event.target.result);
                        setImages({
                            ...images,
                            images: imagesCopy,
                        });
                    });
                    if (typeof files[key] === "object") {
                        reader.readAsDataURL(files[key]);
                    }
                }
            }
        }
    };

  const changeCategory = event => {
    setInputData({
      ...inputData,
      category: event.target.value,
    });
  };

    const date = new Date();
    const changeRegion = event => {
        setInputData({
            ...inputData,
            region: event.target.value,
        });
    };

    const deleteImg = (ind) => {
        const inputDataCopy = {...inputData};
        const copyImg = inputDataCopy.image;
        copyImg.splice(ind, 1);
        setInputData({
            ...inputData,
            image: copyImg,
        });
        const imagesCopy = images.images;
        imagesCopy.splice(ind, 1);
        setImages({
            ...images,
            images: imagesCopy,
        });
    };

    const onClickAddQuestion = async e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('title', inputData.title);
        formData.append('description', inputData.description);
        formData.append('category', inputData.category);
        formData.append('datetime', date.toISOString());
        formData.append('region', inputData.region);
        formData.append('answers', inputData.answers);
        for (let i = 0; i < inputData.image.length; i++) {
            formData.append(`image`, inputData.image[i])
        }
        dispatch(fetchAddQuestion(formData));
    };

    return (
        <Container component="main">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <PostAddIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    <FormattedMessage
                        id="app.askQuestion_HeaderNavigation"
                        defaultMessage="Задать вопрос"/>
                </Typography>
                <form className={classes.form} onSubmit={onClickAddQuestion}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormattedMessage id="app.addApplication_Title_PlaceHolder" defaultMessage="Опишите вашу проблему в нескольких словах">
                                {(placeholder) => (
                                    <FormattedMessage id="app.title" defaultMessage="Заголовок">
                                        {(label) => (
                                            <TextField
                                                variant="outlined"
                                                required
                                                fullWidth
                                                id="title"
                                                placeholder={placeholder}
                                                label={label}
                                                name="title"
                                                className={classes.input}
                                                value={inputData.title}
                                                onChange={changeInputData}
                                                autoComplete="title"
                                            />
                                        )}
                                    </FormattedMessage>
                                )}
                            </FormattedMessage>
                        </Grid>
                        <Grid item xs={12}>
                            <FormattedMessage
                                id="app.addApplication_Description_PlaceHolder"
                                defaultMessage="Постарайтесь максимально развёрнуто описать вашу проблему">
                                {(placeholder) => (
                                    <FormattedMessage id="app.description" defaultMessage="Описание">
                                        {(label) => (
                                            <TextField
                                                variant="outlined"
                                                required
                                                fullWidth
                                                multiline
                                                rows={5}
                                                id="description"
                                                label={label}
                                                placeholder={placeholder}
                                                className={classes.input}
                                                name="description"
                                                value={inputData.description}
                                                onChange={changeInputData}
                                                autoComplete="description"
                                            />
                                        )}
                                    </FormattedMessage>
                                )}
                            </FormattedMessage>
                        </Grid>
                        <FormControl className={classes.formControl} variant="outlined">
                            <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                                <FormattedMessage
                                id="app.category"
                                defaultMessage="Категория"/>
                            </InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                required
                                fullWidth
                                label="Категория"
                                value={inputData.category}
                                onChange={changeCategory}
                            >
                                {categories && categories.map(c => {
                                    return <MenuItem key={c._id} id={c.title} value={c._id}>
                                        <CategoriesAndRegionsTranslation
                                            kgTitle={c.kgTitle}
                                            engTitle={c.engTitle}
                                            rusTitle={c.rusTitle}/>
                                    </MenuItem>
                                })}
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl} variant="outlined">
                            <InputLabel id="demo-simple-select-label">
                                <FormattedMessage id="app.region" defaultMessage="Регион"/>
                            </InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="region-select"
                                required
                                fullWidth
                                label="Регион:"
                                value={inputData.region}
                                onChange={changeRegion}
                            >
                                {regions && regions.map(c => {
                                    return <MenuItem key={c._id} id={c.title} value={c._id}>
                                        <CategoriesAndRegionsTranslation
                                            kgTitle={c.kgTitle}
                                            engTitle={c.engTitle}
                                            rusTitle={c.rusTitle}/>
                                    </MenuItem>
                                })}
                            </Select>
                        </FormControl>
                        <input
                            accept="image/*"
                            style={{ display: 'none' }}
                            id="raised-button-file"
                            multiple
                            name='image'
                            ref={inputRef}
                            onChange={fileChangeHandler}
                            type="file"
                        />
                        <h3
                            style={{
                                paddingLeft: "10px",
                                marginBottom: "0px",
                            }}
                        >
                            <FormattedMessage id="app.newImages" defaultMessage="Новые фотографии"/>:
                        </h3>
                        <div className={classes.imagesBlock}>
                            {images.images && images.images.map((img, ind) => {
                                return (
                                    <div key={ind}>
                                        {
                                            img &&
                                            <div className={classes.img}>
                                                <HighlightOffIcon
                                                    className={classes.delete}
                                                    value={ind} onClick={() => deleteImg(ind)}
                                                />
                                                <img
                                                    className={classes.image}
                                                    src={img} alt=""
                                                />
                                            </div>
                                        }
                                    </div>
                                );
                            })}
                            {!images.images[0] && <p>Если у вас есть фотографии связанные с вашим вопросом, загрузите их.</p>}
                        </div>
                        <label htmlFor="raised-button-file" className={classes.label}>
                            <Button variant="contained"
                                    component="span"
                                    id={"add_photo"}
                                    fullWidth
                                    color="primary"
                                    className={classes.submit}
                            >
                                <FormattedMessage
                                id="app.loadImage" defaultMessage="Загрузить фото"/>
                            </Button>
                        </label>

                    </Grid>
                    <Button
                        id={"add_answer"}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        <FormattedMessage
                        id="app.addApplication"
                        defaultMessage="Оставить заявку"/>
                    </Button>
                </form>
            </div>
            <AboutProjectBtn/>
        </Container>
    );
}