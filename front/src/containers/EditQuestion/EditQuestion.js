import React, {useEffect, useRef, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import {FormattedMessage} from "react-intl";
import TextField from "@material-ui/core/TextField";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {getCategories} from "../../store/actions/categoriesActions";
import {getRegions} from "../../store/actions/regionsActions";
import {editQuestion, getQuestion} from "../../store/actions/questionActions";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {englishLanguageCode, kyrgyzLanguageCode, russianLanguageCode} from "../../localization/Wrapper";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";


const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%',
        marginTop: "130px",
        padding: "0px 60px",
    },
    submit: {
        marginBottom: "50px",
    },
    formControl: {
        margin: "0 auto 15px",
        width: '98.5%'
    },
    label: {
        width: '20%',
        padding: '9px'
    },
    imagesBlock: {
        display: "flex",
        flexWrap: "wrap",
        width: "98.5%",
        margin: "0 auto"
    },
    image: {
        display: "block",
        width: "150px",
        height: "150px",
        margin: "0 auto",
    },
    img: {
        marginRight: "10px",
        border: "1px solid black",
        width: "152px",
        height: "152px",
        position: "relative"
    },delete: {
        position: "absolute",
        top: "0",
        right: "0",
        '&:hover': {
            color: "red"
        }
    },
}));

const EditQuestion = (props) => {
    const classes = useStyles();
    const inputRef = useRef();
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);
    const regions = useSelector(state => state.regions.regions);
    const question = useSelector(state => state.questions.singleQuestion);
    const language = localStorage.getItem("language");

    const [inputData, setInputData] = useState({
        title: "",
        description: "",
        category: "",
        region: "",
        image: [],
        newImages: [],
        published: false,
    });

    const [choosenImage, setChoosenImage] = useState({images: []});

    useEffect(() => {
        dispatch(getQuestion(props.match.params.id));
        dispatch(getCategories());
        dispatch(getRegions());
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        if (question.region) {
            if (!inputData.category) {
                setInputData({
                    ...inputData,
                    title: question.title,
                    description: question.description,
                    category: question.category._id,
                    region: question.region._id,
                    image: question.image,
                })
            }
        }
    }, [question, inputData, question.title, question.description, question.category, question.region, question.image]);

    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;
        setInputData({
            ...inputData,
            [name]: value,
        });
    };

    const fileChangeHandler = e => {
        const files = e.target.files;
        if (files) {
            const inputDataCopy = {...inputData};
            const copyImg = inputDataCopy.newImages;
            for (const key in files) {
                if (typeof files[key] === "object") {
                    copyImg.push(files[key]);
                }
            }
            setInputData({
                ...inputData,
                newImages: copyImg
            });

            if (window.FileList && window.File && window.FileReader) {
                for (const key in files) {
                    const reader = new FileReader();
                    reader.addEventListener('load', (event) => {
                        const choosenImagesCopy = choosenImage.images;
                        choosenImagesCopy.push(event.target.result);
                        setChoosenImage({
                            ...choosenImage,
                            images: choosenImagesCopy,
                        });
                    });

                    if (typeof files[key] === "object") {
                        reader.readAsDataURL(files[key]);
                    }
                }
            }
        }
    };

    const deleteImg = ind => {
        const copy = {...inputData};
        const copyImg = copy.image
        copyImg.splice(ind, 1);
        setInputData({
            ...inputData,
            images: copyImg,
        });
    };

    const deleteChoosenImg = ind => {
        const newImagesCopy = inputData.newImages;
        newImagesCopy.splice(ind, 1);
        setInputData({
            ...inputData,
            newImages: newImagesCopy
        });
        const copy = choosenImage.images;
        copy.splice(ind, 1);
        setChoosenImage({
            ...choosenImage,
            images: copy,
        });
    };


    const changeCategory = event => {
        setInputData({
            ...inputData,
            category: event.target.value,
        });
    };

    const date = new Date();
    const changeRegion = event => {
        setInputData({
            ...inputData,
            region: event.target.value,
        });
    };

    const editQuestionHandler = () => {
        const formData = new FormData();
        formData.append('title', inputData.title);
        formData.append('description', inputData.description);
        formData.append('category', inputData.category);
        formData.append('datetime', date.toISOString());
        formData.append('region', inputData.region);
        formData.append('published', inputData.published);
        formData.append('answers', question.answers);
        formData.append(`image`, JSON.stringify(inputData.image));
        for (let i = 0; i < inputData.newImages.length; i++) {
            formData.append(`newImages`, inputData.newImages[i])
        }
        dispatch(editQuestion(question._id, formData));
    };

    return (
        <div className={classes.form}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FormattedMessage id="app.title" defaultMessage="Заголовок">
                        {(label) => (
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="title"
                                label={label}
                                name="title"
                                value={inputData.title}
                                onChange={changeInputData}
                                autoComplete="title"
                            />
                        )}
                    </FormattedMessage>
                </Grid>
                <Grid item xs={12}>
                    <FormattedMessage id="app.description" defaultMessage="Описание">
                        {(label) => (
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                multiline
                                rows={5}
                                id="description"
                                label={label}
                                className={classes.descriptionInput}
                                name="description"
                                value={inputData.description}
                                onChange={changeInputData}
                                autoComplete="description"
                            />
                        )}
                    </FormattedMessage>
                </Grid>
                {inputData && inputData.category &&
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                        <FormattedMessage
                            id="app.category"
                            defaultMessage="Категория"/>
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        required
                        fullWidth
                        label="Категория"
                        value={inputData.category}
                        onChange={changeCategory}
                    >
                        {categories && categories.map(c => {
                            return <MenuItem
                                key={c._id} id={c.rusTitle}
                                value={c._id}
                            >
                                {language === kyrgyzLanguageCode ? c.kgTitle : null}
                                {language === englishLanguageCode ? c.engTitle : null}
                                {language === russianLanguageCode ? c.rusTitle : null}
                            </MenuItem>
                        })}
                    </Select>
                </FormControl>
                }
                {inputData && inputData.region &&
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel id="demo-simple-select-label">
                        Регион:
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="region-select"
                        required
                        fullWidth
                        label="Регион:"
                        value={inputData.region}
                        onChange={changeRegion}
                    >
                        {regions && regions.map(c => {
                            return <MenuItem key={c._id} id={c.rusTitle} value={c._id}>
                                {language === kyrgyzLanguageCode ? c.kgTitle : null}
                                {language === englishLanguageCode ? c.engTitle : null}
                                {language === russianLanguageCode ? c.rusTitle : null}
                            </MenuItem>
                        })}
                    </Select>
                </FormControl>
                }
                <>
                    <h3
                        style={{
                            paddingLeft: "10px",
                        }}>
                        <FormattedMessage
                            id="app.loadedImages" defaultMessage="Загруженные фотографии"
                        />:
                    </h3>
                    <div className={classes.imagesBlock}>
                        {inputData.image && inputData.image.map((name, ind) => {
                            return (
                                <div key={ind}>
                                    {
                                        name && name.filename &&
                                        <div className={classes.img}>
                                            <HighlightOffIcon
                                                className={classes.delete}
                                                value={ind} onClick={() => deleteImg(ind)}
                                            />
                                            <img
                                                className={classes.image} key={ind}
                                                src={"http://localhost:8000/uploads/" + name.filename} alt=""
                                            />
                                        </div>
                                    }
                                </div>
                            );
                        })}
                    </div>
                    <h3
                        style={{
                            paddingLeft: "10px",
                        }}
                    >
                        <FormattedMessage id="app.newImages" defaultMessage="Новые фотографии"/>:
                    </h3>
                    <div className={classes.imagesBlock}>
                        {choosenImage.images && choosenImage.images.map((img, ind) => {
                            return (
                                <div key={ind}>
                                    {
                                        img &&
                                        <div className={classes.img}>
                                            <HighlightOffIcon
                                                className={classes.delete}
                                                value={ind} onClick={() => deleteChoosenImg(ind)}
                                            />
                                            <img
                                                className={classes.image}
                                                src={img} alt=""
                                            />
                                        </div>
                                    }
                                </div>
                            );
                        })}
                        {/*{!choosenImage.images[0] && <p>Вы не загрузили новых фотографий</p>}*/}
                    </div>
                </>

                <input
                    accept="image/*"
                    style={{ display: 'none' }}
                    id="raised-button-file"
                    multiple
                    name='image'
                    ref={inputRef}
                    onChange={fileChangeHandler}
                    type="file"
                />
                <label htmlFor="raised-button-file" className={classes.label}>
                    <Button variant="contained"
                            component="span"
                            id={"add_photo"}
                            fullWidth
                            color="primary"
                            className={classes.submit}
                    >
                        <FormattedMessage
                            id="app.loadImage" defaultMessage="Загрузить фото"/>
                    </Button>
                </label>

            </Grid>
            <Button
                id={"add_answer"}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={editQuestionHandler}
            >
                <FormattedMessage
                    id="app.changeApplication" defaultMessage="Изменить заявку"
                />
            </Button>
            <AboutProjectBtn/>
        </div>
    );
};

export default EditQuestion;