import React, {useState, useRef, useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {makeStyles} from '@material-ui/core/styles';
import {getInterlocutor} from "../../store/actions/chatActions";
import {getMessages, postMessage, sendMessage} from "../../store/actions/chatActions";
import {removeChat} from "../../store/actions/userActions";
import {Link} from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Avatar from '@material-ui/core/Avatar';
import Backdrop from '@material-ui/core/Backdrop';
import {removeMessageNotification} from "../../store/actions/notificationsActions";
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import dots from "./294.gif";
import {nanoid} from "nanoid";

const useStyles = makeStyles((theme) => ({
    main: {
        marginTop: '100px'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    messages: {
        width: '100%',
        height: '500px',
        padding: '20px',
        overflow: 'auto',
        transform: 'scaleY(-1)'
    },
    name: {
        marginLeft: '20px'
    },
    inner: {
        transform: 'scaleY(-1)'
    },
    form: {
        display: 'flex',
        marginTop: '20px',
        justifyContent: 'space-between',
        alignItems: 'stretch'
    },
    field: {
        width: '80%'
    },
    title: {
        display: 'flex',
        alignItems: 'center'
    },
    singleMessageRight: {
        width: '70%',
        padding: '10px',
        marginBottom: '10px',
    },
    imgRight: {
        width: '70%',
        marginLeft: 'auto',
        marginBottom: '10px',
    },
    singleMessageLeft: {
        padding: '10px',
        marginBottom: '10px',
    },
    imgLeft: {
        width: '70%',
        marginRight: 'auto',
        marginBottom: '10px',
    },
    bigImg: {
      height: '70vh',
      width: 'auto'
    },
    msgImgLeft: {
        cursor: 'pointer',
        display: 'block',
        height: '300px',
        width: 'auto',
        marginLeft: 'auto'
    },
    chatHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    messageInner: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    msgImg: {
        height: '300px',
        width: 'auto',
        cursor: 'pointer',
    },
    userNameLink: {
        color: "black",
        textDecoration: "none",
    },
}));


const Chat = (props) => {
    const classes = useStyles();
    const ws = useRef(null);
    const inputRef = useRef();
    const [message, setMessage] = useState('');
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user)
    const interlocutor = useSelector(state => state.chat.interlocutor);
    const messages = useSelector(state => state.chat.messages);
    const [typing, setTyping] = useState(false);
    const [image, setImage] = useState({image: ''});
    const [open, setOpen] = useState(false);
    const [img, setImg] = useState('');

    useEffect(() => {
        dispatch(getInterlocutor(props.match.params.id));
        dispatch(removeMessageNotification({from: props.match.params.id, to: user._id}))
    }, [props.match.params.id, dispatch, user._id]);

    useEffect(() => {
        ws.current = new WebSocket("ws://localhost:8000/socket?id=" + user._id + "&&to=" + props.match.params.id);

        ws.current.onopen = () => {
            console.log('Chat connection established');
            ws.current.send(JSON.stringify({type: "GET_ALL_MESSAGES"}));
        };

        ws.current.onmessage = e => {
            const decodedMessage = JSON.parse(e.data);
            if (decodedMessage.type === "NEW_MESSAGE") {
                dispatch(postMessage(decodedMessage.message));
                setTyping(false);
            } else if (decodedMessage.type === "ALL_MESSAGES") {
                dispatch(getMessages(decodedMessage.result));
            } else if (decodedMessage.type === "START_TYPING") {
                setTyping(true);
            }
        };

        ws.current.onclose = () => {
            window.location.reload();
            console.log("chat connection closed");
        };

        return () => {
            ws.current.close();
        };

    }, [dispatch, props.match.params.id, user._id]);

    const inputChangeHandler = e => {
        setMessage(e.target.value);
        ws.current.send(JSON.stringify({
            type: "START_TYPING",
        }));
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleToggle = (value) => {
        setOpen(!open);
        setImg(value);
    };

    const remove = data => {
        dispatch(removeChat(data));
    };

    const fileChangeHandler = e => {
        const file = e.target.files[0];
        setImage({image: file});
    };

    const formSubmit = async e => {
        e.preventDefault();
        if (message !== '') {
            ws.current.send(JSON.stringify({
                type: "CREATE_MESSAGE",
                text: message,
                from: user._id,
                to: props.match.params.id
            }));
            ws.current.send(JSON.stringify({
                type: "YOU_HAVE_NEW_MESSAGE",
                from: user._id,
                to: props.match.params.id
            }));
            setMessage('');
        } else if (image.image !== '') {
            const formData = new FormData();
            let text = nanoid();
            formData.append('from', user._id);
            formData.append('to', props.match.params.id);
            formData.append('image', image.image);
            formData.append('text', text);
            await dispatch(sendMessage(formData));
            setImage({image: ''});
            ws.current.send(JSON.stringify({
                type: "CREATE_MESSAGE_WITH_IMAGE",
                text: text
            }));
            ws.current.send(JSON.stringify({
                type: "YOU_HAVE_NEW_MESSAGE",
                from: user._id,
                to: props.match.params.id
            }));
        }
    };

    let chatInner = messages.map(msg => {
        if (msg && msg.from === user._id) {
            return (
                <div className={classes.imgRight} key={msg._id}>
                    {msg.image ? <img src={"http://localhost:8000/uploads/" + msg.image}
                                      className={classes.msgImgLeft}
                                      alt="message img"
                                      onClick={() => handleToggle(msg.image)}
                        /> :
                        <Paper elevation={3}
                               className={classes.singleMessageRight}
                        >
                            <div className={classes.messageInner}>
                                {msg.text}
                            </div>
                        </Paper>}
                </div>
            )
        } else {
            return (
                <div className={classes.imgLeft} key={msg._id}>
                    {msg.image ? <img src={"http://localhost:8000/uploads/" + msg.image}
                                      className={classes.msgImg}
                                      alt="message img"
                                      onClick={() => handleToggle(msg.image)}
                        /> :
                        <Paper elevation={3}
                               className={classes.singleMessageLeft}
                        >
                            <div className={classes.messageInner}>
                                {msg && msg.text}
                            </div>
                        </Paper>
                    }
                </div>
            )
        }
    });


    return (
        <div className={classes.main}>
            <div>
                <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
                    <img src={"http://localhost:8000/uploads/" + img}
                        className={classes.bigImg}
                         alt="message img"/>
                </Backdrop>
            </div>
            <div>
                <div className={classes.chatHeader}>
                    <div className={classes.title}>
                        <Avatar alt="Remy Sharp"
                                src={interlocutor.image ? "http://localhost:8000/uploads/" + interlocutor.image :
                                    "https://cdn.iconscout.com/icon/free/png-256/avatar-380-456332.png"
                                }/>
                        <h2 className={classes.name}>
                            <Link to={"/user/" + interlocutor._id} className={classes.userNameLink}>
                            {interlocutor.fullName}
                            </Link></h2>
                    </div>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => remove(props.match.params.id)}
                    >
                        Удалить чат
                    </Button>
                </div>
                <Paper elevation={3}
                       className={classes.messages}
                >
                    <div className={classes.inner}>
                        {chatInner}
                        {typing ?
                            <Paper elevation={3}
                                   className={classes.singleMessageLeft}
                            >
                                <img src={dots} alt="dots"/>
                            </Paper>
                            : null}
                    </div>
                </Paper>
            </div>
            <div>
                <form
                    className={classes.form}
                    onSubmit={formSubmit}
                >
                    <TextField variant="outlined"
                               placeholder="Введите сообщение"
                               className={classes.field}
                               value={message}
                               onChange={inputChangeHandler}
                    />
                    <input
                        accept="image/*"
                        style={{display: 'none'}}
                        id="raised-button-file"
                        multiple
                        name='image'
                        ref={inputRef}
                        onChange={fileChangeHandler}
                        type="file"
                    />
                    <label htmlFor="raised-button-file" className={classes.label}>
                        <Button variant="contained"
                                component="span"
                                id={"add_photo"}
                                color="primary"
                        >
                            <PhotoCameraIcon
                                fontSize={"large"}
                            />
                        </Button>
                    </label>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                    >
                        Отправить
                    </Button>
                </form>
            </div>
        </div>
    );
};

export default Chat;