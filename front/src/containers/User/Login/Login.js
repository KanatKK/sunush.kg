import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import { login } from '../../../store/actions/userActions';
import { InputAdornment } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import {FormattedMessage} from "react-intl";
import InputMask from 'react-input-mask';
import AboutProjectBtn from "../../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "200px"
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  link: {
    marginRight: 10
  }
}));

export default function Login() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.user.loginError);
  const [inputData, setInputData] = useState({
    phone: '',
    password: '',
  });

  const [showPassword, setShowPassword] = useState(false);
  const changeInputData = event => {
    const name = event.target.name;
    const value = event.target.value;

    if (name === "phone") {
      setInputData({
        ...inputData,
        [name]: (value.replace(/[+][(]\d+[)]/,'')).replace(/\s/g, ''),
      });
    } else {
      setInputData({
        ...inputData,
        [name]: value,
      });
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const loginUser = event => {
    event.preventDefault();
    dispatch(login(inputData));
  }
  return (
    <Container component="main" maxWidth="xs" className={classes.root}>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOpenIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          <FormattedMessage
              id="app.sign_in_Title"
              defaultMessage="Вход"/>
        </Typography>
        <form className={classes.form} onSubmit={loginUser}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <FormattedMessage id="app.phoneNumber" defaultMessage="Номер телефона">
                {(label) => (
                    <InputMask
                        mask="+(\9\96) 999 99 99 99"
                        onChange={changeInputData}
                        value={inputData.phone}>
                      {(inputProps) => <TextField
                          {...inputProps} type="tel"
                          variant="outlined"
                          required
                          fullWidth
                          name="phone"
                          label={label}
                          id="phone"
                          autoComplete="current-phone" />}
                    </InputMask>
                )}
              </FormattedMessage>
            </Grid>
            <Grid item xs={12}>
             <FormattedMessage id="app.password" defaultMessage="Пароль">
               {(label) => (
                   <TextField
                       variant="outlined"
                       required
                       fullWidth
                       name="password"
                       onChange={changeInputData}
                       error={error !== null && error === 'Password is wrong'}
                       label={label}
                       value={inputData.password}
                       helperText={error === 'Password is wrong' ? "Пароль неправильный" : null}
                       type={showPassword ? 'text' : 'password'}
                       id="password"
                       autoComplete="current-password"
                       InputProps={{
                         endAdornment: (
                             <InputAdornment position="end">
                               <IconButton
                                   aria-label="toggle password visibility"
                                   onClick={handleClickShowPassword}
                                   onMouseDown={handleMouseDownPassword}
                                   edge="end"
                               >
                                 {showPassword ? <Visibility /> : <VisibilityOff />}
                               </IconButton>
                             </InputAdornment>
                         )
                       }}
                   />
               )}
             </FormattedMessage>
            </Grid>
          </Grid>
          <Button
              id={"login"}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            <FormattedMessage
                id="app.sign_in"
                defaultMessage="Войти"/>
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/forgotYourPassword" variant="body2" className={classes.link}>
                <FormattedMessage
                    id="app.forgotYourPassword"
                    defaultMessage="Забыли пароль ?"/>
              </Link>
              <Link href="/register" variant="body2">
                <FormattedMessage
                    id="app.noAccountYet"
                    defaultMessage="Нет аккаунта?"/>
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <AboutProjectBtn/>
    </Container>
  );
}