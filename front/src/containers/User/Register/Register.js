import React, {useEffect, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {useDispatch, useSelector} from 'react-redux';
import {register} from '../../../store/actions/userActions';
import {FormControl, InputAdornment, InputLabel} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import {getCategories} from "../../../store/actions/categoriesActions";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation
  from "../../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
// import {englishLanguageCode, kyrgyzLanguageCode, russianLanguageCode} from "../../../localization/Wrapper";
import firebase from "../../../constants";
import InputMask from 'react-input-mask';
import swal from 'sweetalert';
import {getRegions} from "../../../store/actions/regionsActions";
import AboutProjectBtn from "../../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(1, 0, 2),
  },
  formControl: {
    margin: "0 auto",
    width: '80%'
  },
  categoryFormControl: {
    margin: "10px auto",
    width: '96%'
  },
  code: {
    margin: theme.spacing(2, 0, 1),
  },
  link: {
    paddingBottom: 15
  },
}));

export default function SignUp() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const categories = useSelector(state => state.categories.categories);
  const regions = useSelector(state => state.regions.regions);
  const error = useSelector(state => state.user.registerError);
  const passwordTooSimpleErrorText = "Пароль слишком простой";
  const passwordTooShortErrorText = "Минимальная длина пароля 8 символов";
  const [inputData, setInputData] = useState({
    fullName: '',
    phone: '',
    password: '',
    email: '',
    role: 'user',
    address: '',
    experience: '',
    category: '',
    website: '',
    region: '',
  });

  useEffect(() => {
    dispatch(getCategories());
    dispatch(getRegions());
  }, [dispatch]);

  const [showPassword, setShowPassword] = useState(false);
  const [registerBtn, setRegisterBtn] = useState(false);
  const [codeBtn, setCodeBtn] = useState(false);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  let passwordHelperText;

      if(error && error.password){
          if(error.password.message === passwordTooShortErrorText){
              passwordHelperText = (<FormattedMessage id="app.errorPasswordIsTooShort"/>)
          }else if(error.password.message === passwordTooSimpleErrorText) {
              passwordHelperText = (<FormattedMessage id="app.errorPasswordIsTooSimple"/>)
          }else {
              passwordHelperText = (<FormattedMessage id="app.passwordInputRegister"/>);
          }
      }

  let phoneHelperText;

      if (error && error.phone){
        phoneHelperText = (<FormattedMessage id="app.errorPhone"/>)
      } else {
        phoneHelperText = (<FormattedMessage id="app.phoneHelperText" defaultMessage="В формате +996123456789"/>)
      }

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSignInSubmit = (event) => {
    event.preventDefault();
    setUpRecaptcha();
    setCodeBtn(!codeBtn);
    const phoneNumber = '+996'+(inputData.phone);
    const appVerifier = window.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
        .then((confirmationResult) => {
          window.confirmationResult = confirmationResult;
          const code = window.prompt("Enter code");
                  // const code = swal("Введите проверочный код:", {
                  //   content: "input",
                  // });
          window.confirmationResult = confirmationResult;
            confirmationResult.confirm(code).then((result) => {
              setRegisterBtn(!registerBtn);
              swal("Отлично!", "Ваш номер успешно подтвержден!", "success");
                        }).catch((error) => {
              swal("Провал :(", "Ваш номер не подтвержден!", "error");
              setUpRecaptcha();
            })
    });
  };

  const setUpRecaptcha = () => {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
        'recaptcha-container',{
      'size': 'invisible',
      'hl' : 'ru',
      'callback': (response) => {
        onSignInSubmit();
      }
    });
  };

  const changeInputData = event => {
    const name = event.target.name;
    const value = event.target.value;

    if (name === "password") {
      setInputData({
        ...inputData,
        [name]: (value.replace(/[+][(]\d+[)]/,'')).replace(/\s/g, ''),
      });
    } else if (name === "phone") {
      setInputData({
        ...inputData,
        [name]: (value.replace(/[+][(]\d+[)]/,'')).replace(/\s/g, ''),
      });
    } else {
      setInputData({
        ...inputData,
        [name]: value,
      });
    }
  };

  const changeSelectorValue = event => {
    const name = event.target.name;
    const value = event.target.value;
    setInputData({
      ...inputData,
      [name]: value,
    });
  };

  let anotherCategory;
  let anotherRegion;

  for (const key in categories) {
    if (categories[key].rusTitle === "Другое") {
      anotherCategory = categories[key]._id;
    }
  }

  for (const key in regions) {
    if (regions[key].rusTitle === "Чуйская область") {
      anotherRegion = regions[key]._id;
    }
  }

  const registerUser = event => {
    event.preventDefault();
    let data;
    if (inputData.role === 'user') {
      data = {
        fullName: inputData.fullName,
        phone: inputData.phone,
        password: inputData.password,
        role: inputData.role
      }
    } else {
      data = inputData;
      if (data.category === "") {
        data.category = anotherCategory
      }
      if(data.region === ""){
        data.region = anotherRegion;
      }
    }
      dispatch(register(data));
  }

  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <PersonAddIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            <FormattedMessage id="app.sign_up_HeaderNavigation" defaultMessage="Регистрация"/>
          </Typography>
          <form className={classes.form} onSubmit={registerUser}>
            <div id="recaptcha-container"></div>
            <Grid container spacing={2} className={classes.link}>
              <Grid item xs={12}>
                <FormattedMessage id="app.fullName" defaultMessage="ФИО">
                  {(label) => (
                      <TextField
                          variant="outlined"
                          required
                          fullWidth
                          name="fullName"
                          onChange={changeInputData}
                          value={inputData.fullName}
                          label={label}
                          type="text"
                          id="fullName"
                          autoComplete="current-password"
                      />
                  )}
                </FormattedMessage>
              </Grid>
              <Grid item xs={12} className={classes.link}>
                <FormattedMessage id="app.phoneNumber" defaultMessage="Номер телефона">
                  {(label) => (
                      <InputMask
                          mask="+(\9\96) 999 99 99 99"
                          onChange={changeInputData}
                          value={inputData.phone}>
                        {(inputProps) =>
                            <TextField
                            {...inputProps} type="tel"
                            variant="outlined"
                            required
                            fullWidth
                            error={error && !!error.phone}
                            helperText={error && error.phone ?
                                <FormattedMessage id="app.phoneNumberIsAlreadyUsed" values={{message: error.phone.value}}/>
                               : null}
                            name="phone"
                            label={label}
                            id="phone"
                            autoComplete="current-phone" />}
                      </InputMask>
                  )}
                </FormattedMessage>
              </Grid>
              <Grid item xs={12}>
                <FormattedMessage id="app.password" defaultMessage="Пароль">
                  {(label) => (
                      <TextField
                          variant="outlined"
                          required
                          fullWidth
                          name="password"
                          onChange={changeInputData}
                          error={error && !!error.password && !error.phone}
                          helperText={error && error.password && !error.phone ? passwordHelperText :
                          <FormattedMessage id="app.passwordInputRegister"/>}
                          label={label}
                          value={inputData.password}
                          type={showPassword ? 'text' : 'password'}
                          id="password"
                          autoComplete="current-password"
                          InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={handleClickShowPassword}
                                      onMouseDown={handleMouseDownPassword}
                                      edge="end"
                                  >
                                    {showPassword ? <Visibility/> : <VisibilityOff/>}
                                  </IconButton>
                                </InputAdornment>
                            )
                          }}
                      />
                  )}
                </FormattedMessage>
              </Grid>
              <FormControl className={classes.formControl}>
                <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                  <FormattedMessage id="app.role" defaultMessage="Роль"/>
                </InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={inputData.role}
                    name="role"
                    onChange={changeSelectorValue}
                >
                  <MenuItem value={'user'}>
                    <FormattedMessage
                        id="app.averageUser_Role"
                        defaultMessage="Обычный пользватель"/>
                  </MenuItem>
                  <MenuItem value={'lawyer'} id={'lawyer'}>
                    <FormattedMessage
                    id="app.lawyer_Role"
                    defaultMessage="Юрист"/>
                  </MenuItem>
                  <MenuItem value={'accountant'} id={'accountant'}>
                    <FormattedMessage
                        id="app.accountant_Role"
                        defaultMessage="Бухгалтер"/>
                  </MenuItem>
                </Select>
              </FormControl>
              {inputData.role === 'user' ? null :
                  <>
                    <Grid item xs={12}>
                      <TextField
                          variant="outlined"
                          required
                          fullWidth
                          name="email"
                          onChange={changeInputData}
                          value={inputData.email}
                          error={error && !!error.email}
                          helperText={error && error.email ? error.email.message : null}
                          label="Email"
                          type="email"
                          id="email"
                          autoComplete="current-password"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FormattedMessage
                          id="app.experienceHelperText"
                          defaultMessage="Укажите сферу деятельности юриспруденции или бухгалтерского дела, в которой вы имеете наибольший опыт">
                        {(helperText) => (
                            <FormattedMessage id="app.experience" defaultMessage="Опыт работы">
                              {(label) => (
                                  <TextField
                                      variant="outlined"
                                      required
                                      fullWidth
                                      name="experience"
                                      onChange={changeInputData}
                                      helperText={helperText}
                                      value={inputData.experience}
                                      label={label}
                                      type="text"
                                      id="experience"
                                      autoComplete="current-password"
                                  />
                              )}
                            </FormattedMessage>
                        )}
                      </FormattedMessage>

                    </Grid>
                    <Grid item xs={12}>
                      <FormattedMessage id="app.address" defaultMessage="Адрес">
                        {(label) => (
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="address"
                                onChange={changeInputData}
                                value={inputData.address}
                                label={label}
                                type="text"
                                id="address"
                                autoComplete="current-password"
                            />
                        )}
                      </FormattedMessage>
                    </Grid>
                    <Grid item xs={12}>
                      <FormattedMessage id="app.webSite" defaultMessage="Веб-сайт">
                        {(label) => (
                            <TextField
                                variant="outlined"
                                fullWidth
                                name="website"
                                onChange={changeInputData}
                                value={inputData.website}
                                label={label}
                                type="text"
                                id="website"
                                autoComplete="current-password"
                            />
                        )}
                      </FormattedMessage>
                    </Grid>

                    <FormControl className={classes.categoryFormControl} variant="outlined">
                      <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                        <FormattedMessage id="app.category" defaultMessage="Категория"/>
                      </InputLabel>
                      <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          fullWidth
                          label="Категория"
                          value={inputData.category}
                          name="category"
                          onChange={changeSelectorValue}
                      >
                        {categories && categories.map(c => {
                          return <MenuItem key={c._id} value={c._id}>
                            <CategoriesAndRegionsTranslation
                                kgTitle={c.kgTitle}
                                engTitle={c.engTitle}
                                rusTitle={c.rusTitle}/>
                          </MenuItem>
                        })}
                      </Select>
                    </FormControl>

                      <FormControl className={classes.categoryFormControl} variant="outlined">
                        <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                          <FormattedMessage id="app.region" defaultMessage="Регион"/>
                        </InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            fullWidth
                            label="Регион"
                            value={inputData.region}
                            name="region"
                            onChange={changeSelectorValue}
                        >
                          {regions && regions.map(r => {
                            return <MenuItem key={r._id} value={r._id}>
                              <CategoriesAndRegionsTranslation
                                  kgTitle={r.kgTitle}
                                  engTitle={r.engTitle}
                                  rusTitle={r.rusTitle}/>
                            </MenuItem>
                          })}
                        </Select>
                      </FormControl>


                  </>}
            </Grid>
            {codeBtn === true || inputData.phone.includes("_") || inputData.phone.length === 0 ? <Button
                id={"code"}
                type="button"
                fullWidth
                variant="contained"
                disabled={true}
                color="primary"
                className={classes.code}
                onClick={onSignInSubmit}
            >
              <FormattedMessage
                  id="codeBTN"
                  defaultMessage="Подтвердить номер телефона"/>
            </Button>:
                <Button
                    id={"code"}
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.code}
                    onClick={onSignInSubmit}
                >
                  <FormattedMessage
                      id="codeBTN"
                      defaultMessage="Подтвердить номер телефона"/>
                </Button>}
            {registerBtn === true ?
                <Button
                id={"register"}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
              <FormattedMessage
              id="app.sign_up"
              defaultMessage="Зарегистрироваться"/>
            </Button>
            :
                <Button
                    id={"register"}
                    type="submit"
                    disabled={true}
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                  <FormattedMessage
                      id="app.sign_up"
                      defaultMessage="Зарегистрироваться"/>
                </Button>
            }
            <Grid container justify="flex-end">
              <Grid item>
                <FormattedMessage
                    id="app.haveAccountAlready"
                    defaultMessage="Уже есть аккаунт?">
                  {(title) => (
                      <Link href="/login" variant="body2">
                        {title}
                      </Link>
                  )}
                </FormattedMessage>
              </Grid>
            </Grid>
          </form>
        </div>
        <AboutProjectBtn/>
      </Container>
  );
}