import React, {useEffect, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import PersonIcon from '@material-ui/icons/Person';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useDispatch, useSelector} from 'react-redux';
import {editUserInfo} from '../../../store/actions/userActions';
import {Link} from "react-router-dom";
import {getCategories} from "../../../store/actions/categoriesActions";
import {FormControl, InputLabel} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation
    from "../../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
import {getRegions} from "../../../store/actions/regionsActions";
import AboutProjectBtn from "../../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: 100,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        margin: "0 auto",
        width: '80%'
    },
    editBtns: {
        width: "40%",
        fontSize: '10px',
        padding: '10px 0',
    },
    editBtnsBox: {
        display: 'flex',
        justifyContent: 'space-evenly',
        margin: '20px 0'
    },
    media: {
        width: 200,
        height: 200,
        borderRadius: "50%",
        margin: 'auto',
        marginLeft: '50%'
    },
    categoryFormControl: {
        margin: "10px auto",
        width: '95%'
    }
}));

export default function MyProfilePage() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const categories = useSelector(state => state.categories.categories);
    const regions = useSelector(state => state.regions.regions);
    const error = useSelector(state => state.user.registerError);
    const notEditedState = {
        username: user.username ? user.username : '',
        email: user.email ? user.email : '',
        role: user.role,
        phone: user.phone ? user.phone : '',
        address: user.address ? user.address : '',
        fullName: user.fullName ? user.fullName : '',
        experience: user.experience ? user.experience : '',
        city: user.city ? user.city : '',
        website: user.website ? user.website : '',
        image: user.image ? user.image : '',
        category: user.category ? user.category : '',
        region: user.region ? user.region : ''
    };

    const [inputData, setInputData] = useState(notEditedState);

    useEffect(() => {
        dispatch(getCategories());
        dispatch(getRegions());
        console.log(inputData)
    }, [dispatch]);

    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;

        setInputData({
            ...inputData,
            [name]: value,
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const newInfo = {...inputData};
        dispatch(editUserInfo(newInfo));
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <PersonIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    <FormattedMessage
                        id="app.editProfile"
                        defaultMessage="Редактировать профиль"/>
                </Typography>
                <form className={classes.form} onSubmit={submitFormHandler}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormattedMessage id="app.username" defaultMessage="Логин">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="username"
                                        label={label}
                                        name="username"
                                        error={error && !!error.username}
                                        helperText={error && error.username ? error.username.message : null}
                                        onChange={changeInputData}
                                        value={inputData.username}
                                        autoComplete="username"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>
                        {user && user.email && <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="email"
                                onChange={changeInputData}
                                value={inputData.email}
                                error={error && !!error.email}
                                helperText={error && error.email ? error.email.message : null}
                                label="Email"
                                type="email"
                                id="email"
                                autoComplete="current-password"
                            />
                        </Grid>}
                        <Grid item xs={12}>
                            <FormattedMessage id="app.fullName" defaultMessage="ФИО">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="fullName"
                                        onChange={changeInputData}
                                        value={inputData.fullName}
                                        label={label}
                                        type="text"
                                        id="fullName"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>
                        <Grid item xs={12}>
                            <FormattedMessage id="app.phoneNumber" defaultMessage="Телефонный номер">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="phone"
                                        onChange={changeInputData}
                                        value={inputData.phone}
                                        label={label}
                                        type="text"
                                        id="phone"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>
                        {user && user.experience && <Grid item xs={12}>
                            <FormattedMessage id="app.experience" defaultMessage="Опыт работы">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="experience"
                                        onChange={changeInputData}
                                        value={inputData.experience}
                                        label={label}
                                        type="text"
                                        id="experience"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>}
                        {user && user.address && <Grid item xs={12}>
                            <FormattedMessage id="app.address" defaultMessage="Адрес">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="address"
                                        onChange={changeInputData}
                                        value={inputData.address}
                                        label={label}
                                        type="text"
                                        id="address"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>}
                        {user && user.city && <Grid item xs={12}>
                            <FormattedMessage id="app.city" defaultMessage="Город">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="city"
                                        onChange={changeInputData}
                                        value={inputData.city}
                                        label={label}
                                        type="text"
                                        id="city"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>}
                        {user && user.website && <Grid item xs={12}>
                            <FormattedMessage id="app.webSite" defaultMessage="Веб-сайт">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="website"
                                        onChange={changeInputData}
                                        value={inputData.website}
                                        label={label}
                                        type="text"
                                        id="website"
                                        autoComplete="current-password"
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>}
                        {user && user.category &&
                        <FormControl className={classes.categoryFormControl} variant="outlined">
                            <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                                <FormattedMessage id="app.category" defaultMessage="Категория"/>
                            </InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                label="Категория"
                                fullWidth
                                value={inputData.category}
                                onChange={changeInputData}
                                name="category"
                            >
                                {categories && categories.map(c => {
                                    return <MenuItem key={c._id} value={c._id}>
                                        <CategoriesAndRegionsTranslation
                                            kgTitle={c.kgTitle}
                                            engTitle={c.engTitle}
                                            rusTitle={c.rusTitle}/>
                                    </MenuItem>
                                })}
                            </Select>
                        </FormControl>}

                        {user && user.region &&
                        <FormControl className={classes.categoryFormControl} variant="outlined">
                            <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                                <FormattedMessage id="app.region" defaultMessage="Регион"/>
                            </InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                label="Регион"
                                fullWidth
                                value={inputData.region}
                                onChange={changeInputData}
                                name="region"
                            >
                                {regions && regions.map(r => {
                                    return <MenuItem key={r._id} value={r._id}>
                                        <CategoriesAndRegionsTranslation
                                            kgTitle={r.kgTitle}
                                            engTitle={r.engTitle}
                                            rusTitle={r.rusTitle}/>
                                    </MenuItem>
                                })}
                            </Select>
                        </FormControl>}

                    </Grid>
                    <div className={classes.editBtnsBox}>
                        <Button
                            type="button"
                            variant="contained"
                            color="secondary"
                            className={classes.editBtns}
                            component={Link}
                            to={"/user/" + user._id}
                        >
                            <FormattedMessage id="app.cancel" defaultMessage="Отмена"/>
                        </Button>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.editBtns}
                        >
                            <FormattedMessage id="app.saveChanges" defaultMessage="Сохранить изменения"/>
                        </Button>
                    </div>
                </form>
            </div>
            <AboutProjectBtn/>
        </Container>
    );
};