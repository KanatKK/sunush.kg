import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getMyQuestions, userDeletedQuestions} from "../../store/actions/questionActions";
import {makeStyles} from "@material-ui/core/styles";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

function TabPanel(props) {
  const {children, value, index, ...other} = props;

  return (
      <div
          role="tabpanel"
          hidden={value !== index}
          id={`vertical-tabpanel-${index}`}
          aria-labelledby={`vertical-tab-${index}`}
          {...other}
      >
        {value === index && (
            <Box p={3} style={{padding: 0}}>
              {children}
            </Box>
        )}
      </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    zIndex: "2",
    marginTop: "70px",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    backgroundColor: theme.palette.background.paper,
  },
  card: {
    fullWidth: "100%",
    padding: "20px",
    backgroundColor: "#f6f2f2",
    marginBottom: "20px",
    boxShadow: "5px 5px 17px 0px rgba(0, 0, 0, 0.3)",
    borderRadius: "5px",
    position: "relative",
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  appTabsBar: {
    marginBottom: "50px",
    alignItems: "center",
    borderRadius: "5px",
  }
}));

const MyApplications = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.user);
  const questions = useSelector(state => state.questions.questions);
  const deletedQuestions = useSelector(state => state.questions.deletedQuestions);
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    dispatch(getMyQuestions(user._id));
  }, [dispatch, user]);

  useEffect(() => {
    dispatch(userDeletedQuestions(user._id));
  }, [dispatch, user]);

  return (
      <div className={classes.root}>
        <div className={classes.content}>
          <AppBar position="static" className={classes.appTabsBar}>
            <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
              <Tab label="Опубликованные" {...a11yProps(0)} />
              <Tab label="Неопубликованные" {...a11yProps(1)} />
              <Tab label="Удаленные" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          {questions && questions.map(question => {
            return (
                <div key={question._id}>
                  <TabPanel value={value} index={0}>
                    {question.published === true ? <QuestionCard
                        image={question.user.image}
                        key={question._id}
                        classname={classes.card}
                        description={question.description}
                        username={question.user.fullName ? question.user.fullName : question.user.username}
                        datetime={question.datetime}
                        id={question._id}
                        published={question.published}
                        title={question.title}
                        userId={question.user._id}
                        answers={question.answers}
                    /> : null}
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    {question.published === false ? <QuestionCard
                        image={question.user.image}
                        key={question._id}
                        classname={classes.card}
                        description={question.description}
                        username={question.user.fullName ? question.user.fullName : question.user.username}
                        datetime={question.datetime}
                        id={question._id}
                        published={question.published}
                        title={question.title}
                        userId={question.user._id}
                        answers={question.answers}
                    /> : null}
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    {deletedQuestions.map(item => {
                      return <QuestionCard
                          image={item.user.image}
                          key={item._id}
                          classname={classes.card}
                          description={item.description}
                          username={item.user.fullName ? item.user.fullName : item.user.username}
                          datetime={item.datetime}
                          id={item._id}
                          published={item.published}
                          title={item.title}
                          userId={item.user._id}
                          answers={item.answers}
                      />
                    })}
                  </TabPanel>
                </div>
            )
          })}
          {!questions &&
          <h3>У вас пока нет заявок</h3>
          }
        </div>
        <AboutProjectBtn/>
      </div>
  );
};

export default MyApplications;