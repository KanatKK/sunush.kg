import {FormControl, InputLabel, makeStyles, MenuItem, Select} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Modal from "../../components/UI/Modal";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import {getQuestionForALawyer} from "../../store/actions/questionActions";
import {FormattedMessage} from "react-intl";
import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles(theme =>({
    root: {
        display: 'flex',
        marginTop: "200px"
    },
    content: {
        marginLeft: "30px",
        width: "100%",
    },
    navBar: {
        backgroundColor: "#925050",
        display: "block",
        marginTop: "65px",
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    addBtn: {
        display: 'block',
        '&:hover': {
            color: "green"
        }
    },
    delBtn: {
        display: 'block',
        '&:hover': {
            color: "red"
        }
    },
    card: {
        fullWidth: "100%",
        padding: "20px",
        backgroundColor: "#f6f2f2",
        marginBottom: "20px",
        boxShadow: "5px 5px 17px 0px rgba(60, 117, 50, 0.73)",
        borderRadius: "5px",
        position: "relative"
    },
    list: {
        width: "auto",
        position: "absolute",
        top: "80px",
        left: 0,
        right: 0,
        border: "1px solid #5280CE",
        padding: "15px 0 15px 0"
    },
    formControl: {
        margin: "0 auto 15px",
        width: '20%',
        backgroundColor: "white",
        marginLeft: 15
    },
    button: {
        marginLeft: 10,
        padding: 15
    }
}));

const QuestionsSuitableForMe = () => {
    const classes = useStyles();
    const questions = useSelector(state => state.questions.lawyer_questions);
    const message = useSelector(state => state.questions.successMessage);
    const user = useSelector(state => state.user.user);
    const totalQuestions = useSelector(state => state.questions.totalQuestions);
    const dispatch = useDispatch();

    const [sorting, setSorting] = useState({
        category: user.category,
        date: '',
        answers: ''
    });

    const changeDate = event => {
        setSorting({
            ...sorting,
            date: event.target.value
        });
    };

    const changeAnswers = event => {
        setSorting({
            ...sorting,
            answers: event.target.value
        });
    };

    const [currentPage, setCurrentPage] = useState(1);
    const [questionsLength, setQuestionsLength] = useState(0);
    const [fetching, setFetching] = useState(true);
    const [allQuestions, setAllQuestions] = useState([]);

    const getQuestions = () => {
        setAllQuestions([]);
        setCurrentPage(1);
        setQuestionsLength(0);
        setFetching(true);
    };

    useEffect(() => {
        if (fetching) {
            setCurrentPage(prevState => prevState + 1);
            dispatch(getQuestionForALawyer(sorting.category, sorting.date, sorting.answers, currentPage));
            if (questions) {
                setQuestionsLength(prevState => prevState + questions.length);
            }
            setFetching(false);
        }
    }, [dispatch, fetching]);

    useEffect(() => {
        if (questions) {
            const allQuestionsCopy = [...allQuestions];
            questions.map(question => {
                allQuestionsCopy.push(question);
                return allQuestionsCopy;
            });
            setAllQuestions(allQuestionsCopy);
        }
    }, [questions]);

    useEffect(() => {
        document.addEventListener('scroll', scrollHandler);
        return function () {
            document.removeEventListener('scroll', scrollHandler);
        }
    });

    const scrollHandler = (e) => {
        if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
            && questionsLength < totalQuestions) {
            setFetching(true);
        }
    };

    return (
        <div className={classes.root}>
            <div className={classes.list}>
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel id="demo-simple-select-label">
                        <FormattedMessage id="app.sortByDate" defaultMessage="Сортировать по дате"/>
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="date-select"
                        required
                        fullWidth
                        label="Сортировать по дате:"
                        value={sorting.date}
                        onChange={changeDate}
                    >
                        <MenuItem value="-1">
                            <FormattedMessage id="app.oldest" defaultMessage="Сначала старые"/>
                        </MenuItem>
                        <MenuItem value="1">
                            <FormattedMessage id="app.newest" defaultMessage="Сначала новые"/>
                        </MenuItem>
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel id="demo-simple-select-label">
                        <FormattedMessage id="app.sortByAnswers" defaultMessage="Сортировать по ответам"/>
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="answers-select"
                        required
                        fullWidth
                        label="Сортировать по ответам:"
                        value={sorting.answers}
                        onChange={changeAnswers}
                    >
                        <MenuItem value={"no_answers"}>
                            <FormattedMessage id="app.withoutAnswers" defaultMessage="Без ответов"/>
                        </MenuItem>
                        <MenuItem value={"with_answers"}>
                            <FormattedMessage id="app.withAnswers" defaultMessage="C ответами"/>
                        </MenuItem>
                    </Select>
                </FormControl>
                <Button variant="outlined" onClick={getQuestions} className={classes.button}><SearchIcon/>
                    <FormattedMessage id="app.search" defaultMessage="Поиск"/>
                </Button>
            </div>
            <div className={classes.content}>
                {message !== null ? <Modal/> : null}
                {allQuestions && allQuestions.map(question => {
                    if (question.published || user.role === 'admin' || user.role === 'moderator') {
                        return (
                            <QuestionCard
                                key={question._id}
                                classname={classes.card}
                                description={question.description}
                                username={question.user.fullName}
                                datetime={question.datetime}
                                id={question._id}
                                published={question.published}
                                title={question.title}
                                answers={question.answers}
                            />
                        )
                    } else return null
                })}
            </div>
            <AboutProjectBtn/>
        </div>
    );
};

export default QuestionsSuitableForMe;