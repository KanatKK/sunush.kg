import {FormControl, InputLabel, makeStyles, MenuItem, Select} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Modal from "../../components/UI/Modal";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import {getSortingQuestions} from "../../store/actions/questionActions";
import {getRegions} from "../../store/actions/regionsActions";
import {getCategories} from "../../store/actions/categoriesActions";
import Button from "@material-ui/core/Button";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation from "../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
import SearchIcon from "@material-ui/icons/Search";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles({
    root: {
        display: 'flex',
        marginTop: "200px"
    },
    content: {
        marginLeft: "30px",
        width: "100%",
    },
    navBar: {
        backgroundColor: "#925050",
        display: "block",
        marginTop: "65px",
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    addBtn: {
        display: 'block',
        '&:hover': {
            color: "green"
        }
    },
    delBtn: {
        display: 'block',
        '&:hover': {
            color: "red"
        }
    },
    card: {
        fullWidth: "100%",
        padding: "20px",
        backgroundColor: "#f6f2f2",
        marginBottom: "20px",
        boxShadow: "5px 5px 17px 0px rgba(0, 0, 0, 0.3)",
        borderRadius: "5px",
        position: "relative"
    },
    list: {
        width: "auto",
        position: "absolute",
        top: "80px",
        left: 0,
        right: 0,
        border: "1px solid #5280CE",
        padding: "15px 0 15px 0"
    },
    formControl: {
        margin: "0 auto 15px",
        width: '20%',
        backgroundColor: "white",
        marginLeft: 15
    },
    button: {
        marginLeft: 10,
        padding: 15
    }
});

const SortedQuestionsPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const totalQuestions = useSelector(state => state.questions.totalQuestions);
    const regions = useSelector(state => state.regions.regions);
    const categories = useSelector(state => state.categories.categories);
    const message = useSelector(state => state.questions.successMessage);
    let questions = useSelector(state => state.questions.sorted_questions);

    const [inputData, setInputData] = useState({
        region: '',
        category: '',
        answers: ''
    });

    useEffect(() => {
        dispatch(getRegions());
        dispatch(getCategories());
    }, [dispatch]);

    const changeRegion = event => {
        setInputData({
            ...inputData,
            region: event.target.value,
        });
    };

    const changeCategory = event => {
        setInputData({
            ...inputData,
            category: event.target.value,
        });
    };

    const changeAnswers = event => {
        setInputData({
            ...inputData,
            answers: event.target.value,
        });
    };

    const [currentPage, setCurrentPage] = useState(1);
    const [questionsLength, setQuestionsLength] = useState(0);
    const [fetching, setFetching] = useState(true);
    const [allQuestions, setAllQuestions] = useState([]);

    const getAllQuestions = () => {
        setAllQuestions([]);
        setCurrentPage(1);
        setQuestionsLength(0);
        setFetching(true);
        setInputData({
            region: '',
            category: '',
            answers: ''
        });
    };

    const getQuestions = () => {
        setAllQuestions([]);
        setCurrentPage(1);
        setQuestionsLength(0);
        setFetching(true);
    }

    useEffect(() => {
        if (fetching) {
            setCurrentPage(prevState => prevState + 1);
            dispatch(getSortingQuestions(inputData.category, inputData.region, inputData.answers, currentPage));
            if (questions) {
                setQuestionsLength(prevState => prevState + questions.length);
            }
            setFetching(false);
        }
    }, [dispatch, fetching]);

    useEffect(() => {
        if (questions) {
            const allQuestionsCopy = [...allQuestions];
            questions.map(question => {
                allQuestionsCopy.push(question);
                return allQuestionsCopy;
            });
            setAllQuestions(allQuestionsCopy);
        }
    }, [questions]);

    useEffect(() => {
        document.addEventListener('scroll', scrollHandler);
        return function () {
            document.removeEventListener('scroll', scrollHandler);
        }
    });

    const scrollHandler = (e) => {
        if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
            && questionsLength < totalQuestions) {
            setFetching(true);
        }
    };

    return (
        <div className={classes.root}>
            <div className={classes.list}>
                <Button variant="outlined" className={classes.button} onClick={getAllQuestions}>
                    <FormattedMessage id="app.clearFilter" defaultMessage="Очистить фильтры"/>
                </Button>
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel className={classes.userStatusSelect} id="demo-simple-select-label">
                        <FormattedMessage id="app.category" defaultMessage="Категория"/>
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        required
                        fullWidth
                        label="Категория"
                        value={inputData.category}
                        onChange={changeCategory}
                    >
                        {categories && categories.map(c => {
                            return <MenuItem key={c._id} value={c._id}>
                                <CategoriesAndRegionsTranslation
                                    kgTitle={c.kgTitle}
                                    engTitle={c.engTitle}
                                    rusTitle={c.rusTitle}/>
                            </MenuItem>
                        })}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel id="demo-simple-select-label">
                        <FormattedMessage id="app.region" defaultMessage="Регион"/>
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="region-select"
                        required
                        fullWidth
                        label="Регион:"
                        value={inputData.region}
                        onChange={changeRegion}
                    >
                        {regions && regions.map(c => {
                            return <MenuItem key={c._id} id={c.title} value={c._id}>
                                <CategoriesAndRegionsTranslation
                                    kgTitle={c.kgTitle}
                                    engTitle={c.engTitle}
                                    rusTitle={c.rusTitle}/>
                            </MenuItem>
                        })}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl} variant="outlined">
                    <InputLabel id="demo-simple-select-label">
                        Сортировать по ответам:
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="answers-select"
                        required
                        fullWidth
                        label="Сортировать по ответам:"
                        value={inputData.answers}
                        onChange={changeAnswers}
                    >
                        <MenuItem value="no_answers">Без ответов</MenuItem>
                        <MenuItem value="with_answers">С ответами</MenuItem>
                    </Select>
                </FormControl>
                <Button variant="outlined" onClick={getQuestions} className={classes.button}><SearchIcon/>
                    <FormattedMessage id="app.search" defaultMessage="Поиск"/>
                </Button>
            </div>
            <div className={classes.content}>
                {message !== null ? <Modal/> : null}
                {allQuestions !== null ? allQuestions.map(question => {
                        return (
                            <QuestionCard
                                key={question._id}
                                classname={classes.card}
                                description={question.description}
                                username={question.user.fullName}
                                datetime={question.datetime}
                                id={question._id}
                                published={question.published}
                                title={question.title}
                                answers={question.answers}
                            />
                        );
                }): <p>По вашей сортировке не найдено вопросов ...</p>}
            </div>
            <AboutProjectBtn/>
        </div>
    );
};

export default SortedQuestionsPage;