import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteUserQuestion, getQuestion} from "../../store/actions/questionActions";
import {makeStyles} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Button from '@material-ui/core/Button';
import {addAnswer, fetchQuestionAnswers} from "../../store/actions/answersActions";
import Paper from "@material-ui/core/Paper";
import LawyerRating from "../../components/Rating/Rating";
import {FormattedMessage} from "react-intl";
import {Link} from "react-router-dom";
import CategoriesAndRegionsTranslation
    from "../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
import {publicQuestion, deleteQuestion} from "../../store/actions/questionActions";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyle = makeStyles(theme => ({
    root: {
        marginTop: "130px"
    },
    card: {
        width: "900px",
        border: "1px solid black",
        margin: "0 auto ",
        padding: "10px 15px",
        borderRadius: "15px",
    },
    imagesBlock: {
        display: "flex",
        flexWrap: "wrap",
    },
    image: {
        display: "block",
        width: "100px",
        height: "100px",
        marginRight: "5px",
        border: "1px solid black",
        cursor: "pointer",
    },
    paper: {
        position: "absolute",
        width: 500,
        backgroundColor: theme.palette.background.paper,
        border: "2px solid #000",
        boxShadow: theme.shadows[5],
    },
    modalImage: {
        display: "block",
        width: "500px",
        height: "500px",
        margin: "auto",
    },
    bold: {
        fontWeight: "bold",
    },
    addAnswerSection: {
        width: "900px",
        margin: "25px auto 0"
    },
    textarea: {
        display: "block",
        width: "100%",
        resize: "none",
        height: "100px",
        border: "1px solid black",
        outline: "none"
    },
    addAnswerBtn: {
        display: "block",
        margin: "10px 0 10px auto",
    },
    answersSection: {
        width: "900px",
        margin: "25px auto 0"
    },
    answer: {
        width: "800px",
        border: "1px solid black",
        margin: "10px auto",
        padding: "10px",
    },
    fullName: {
        marginTop: 0,
    },
    fullNameLink: {
        color: 'black',
        textDecoration: "none",
    },
    description: {
        marginBottom: 0,
    },
    cardTop: {
        display: "flex",
        alignItems: "center",
    },
    category: {
        textAlign: "right",
        width: "50%",
    },
    title: {
        width: "50%",
    },
    categoryStyle: {
        background: "#4D84F4",
        color: "#ffffff",
        padding: "10px 20px"
    },
    answerDate: {
        color: "gray",
        marginBottom: "0",
    },
    buttonsBlock: {
        width: "880px",
        margin: "5px auto 0",
        textAlign: "right",
    },
    updateBtn: {
        marginLeft: "10px",
        padding: "7px 10px",
    },
    deleteBtn: {
        padding: "7px 10px",
    }
}));

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
};

const QuestionPage = (props) => {
    const classes = useStyle();
    const dispatch = useDispatch();
    const question = useSelector(state => state.questions.singleQuestion);
    const user = useSelector(state => state.user.user);
    const language = localStorage.getItem("language");
    const questionAnswers = useSelector(state => state.answers.questionAnswers);
    const ws = useRef(null);

    useEffect(() => {
        dispatch(getQuestion(props.match.params.id));
    }, [props.match.params.id, dispatch, language]);

    useEffect(() => {
        dispatch(fetchQuestionAnswers(props.match.params.id));
    }, [props.match.params.id, dispatch]);

    useEffect(() => {
        if (user && (user.role === 'lawyer' || user.role === 'accountant' || user.role === 'moderator' )) {
            ws.current = new WebSocket("ws://localhost:8000/socket?id=" + user._id);

            ws.current.onopen = () => {
                console.log('Q Connection established');
            };

            ws.current.onclose = () => {
                console.log("Q connection closed");
            };

            return () => {
                window.location.reload();
                ws.current.close();
            };
        } else return null;

    }, [user, user._id]);

    const [modalStyle] = useState(getModalStyle);
    const [open, setOpen] = useState(false);
    const [imageName, setImageName] = useState('');
    const [answer, setAnswer] = useState({
        answer: "", user: user._id, question: props.match.params.id, datetime: "", fullName: user.fullName
    });
    const localAnswers = questionAnswers

    const date = new Date();
    const getAnswerHandler = event => {
        setAnswer({
            ...answer,
            answer: event.target.value,
            datetime: date.toISOString(),
        });
    };

    const handleClose = () => {
        setOpen(false);
    };

    const getImageName = (name) => {
        setImageName(name);
        setOpen(true);
    };

    const addAnswerHandler = (event) => {
        if (answer.answer !== "") {
            event.preventDefault();
            dispatch(addAnswer(answer));
            localAnswers.push(answer);
            setAnswer({
                ...answer,
                answer: ""
            });
            ws.current.send(JSON.stringify({
                type: "NEW_ANSWER",
                to: question.user._id,
                question: question._id,
                rusState: "У вас есть новые ответы"
            }));
        }
    };

    const publ = (id) => {
        dispatch(publicQuestion(id));
        ws.current.send(JSON.stringify({
            type: "PUBLIC_QUESTION",
            to: question.user._id,
            question: question._id,
            rusState: "Ваш вопрос опубликован"
        }));
    };

    const remove = (id) => {
        dispatch(deleteQuestion(id));
        ws.current.send(JSON.stringify({
            type: "DELETE_QUESTION",
            to: question.user._id,
            question: question._id,
            rusState: "Ваш вопрос отклонен"
        }));
    };


    const deleteQuestionHandler = (id) => {
        dispatch(deleteUserQuestion(id, user._id));
    };

    const space = " ";

    return (
        <div className={classes.root}>
            {question ? <div>
                <Paper className={classes.card}>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                    >
                        <div style={modalStyle} className={classes.paper}>
                            <img className={classes.modalImage} src={"http://localhost:8000/uploads/" + imageName}
                                 alt=""/>
                        </div>
                    </Modal>
                    {(user && question) &&
                    (user.role === 'moderator' || user.role === 'admin') &&
                    question.published === false ? <Button
                        variant="contained"
                        color="default"
                        onClick={() => publ(question._id)}
                    >
                        Опубликовать
                    </Button> : null}
                    {(user && question) &&
                    (user.role === 'moderator' || user.role === 'admin') ? <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => remove(question._id)}
                    >
                        Удалить
                    </Button> : null}
                    <div className={classes.cardTop}>
                        <h1 className={classes.title}>{question.title}</h1>
                        {question.category &&
                        <p className={classes.category}>
                      <span className={classes.categoryStyle}>
                          <i>
                                <CategoriesAndRegionsTranslation
                                    kgTitle={question.category.kgTitle}
                                    engTitle={question.category.engTitle}
                                    rusTitle={question.category.rusTitle}/>
                          </i>
                      </span>
                        </p>
                        }
                    </div>
                    <div className={classes.info}>
                        {question.user &&
                        <p><span className={classes.bold}>
                        <FormattedMessage id="app.author" defaultMessage="Автор"/>
                    </span>:<i> {question.user.fullName}</i></p>}
                        <p>
                            <span className={classes.bold}>
                                <FormattedMessage id="app.date" defaultMessage="Дата"/>
                            </span>: {question.datetime && new Date(question.datetime).toISOString().slice(0, 10)}
                            {space}
                            <span className={classes.bold}>
                                <FormattedMessage id="app.time" defaultMessage="Время"/>
                            </span>: {question.datetime && new Date(question.datetime).toISOString().slice(11, 19)}
                        </p>
                        <h4 style={{marginBottom: 0}}>
                            <FormattedMessage id="app.description" defaultMessage="Описание"/>
                            :
                        </h4>
                        <p style={{marginTop: 0}}>{question.description}</p>
                    </div>
                    <div className={classes.imagesBlock}>
                        {question.image && question.image.map((name, ind) => {
                            return (
                                <img
                                    className={classes.image} key={ind}
                                    src={"http://localhost:8000/uploads/" + name.filename} alt=""
                                    onClick={() => getImageName(name.filename)}
                                />
                            );
                        })}
                    </div>
                </Paper>
                {user && question.user && user._id === question.user._id &&
                <div className={classes.buttonsBlock}>
                    <Button
                        className={classes.deleteBtn} variant="contained"
                        color="secondary" onClick={() => deleteQuestionHandler(question._id)}
                    >
                        <FormattedMessage id="app.delete" defaultMessage="Удалить"/>
                    </Button>
                    <Button
                        className={classes.updateBtn} variant="contained" component={Link}
                        color="primary" to={"/edit_question/" + question._id}
                    >
                        <FormattedMessage id="app.edit" defaultMessage="Редактировать"/>
                    </Button>
                </div>
                }

                <div className={classes.answersSection}>
                    {localAnswers && localAnswers.map((questionAnswer) => {
                        return (
                            <Paper key={questionAnswer._id} className={classes.answer}>
                                {questionAnswer.user.fullName &&
                                <h4 className={classes.fullName}>
                                    <Link className={classes.fullNameLink} to={"/user/" + questionAnswer.user._id}>
                                        {questionAnswer.user.fullName}
                                    </Link>
                                </h4>}
                                <p className={classes.answerDate}>
                                    <span>
                                        <FormattedMessage id="app.date" defaultMessage="Дата"/>
                                    </span>: {question.datetime && new Date(questionAnswer.datetime).toISOString().slice(0, 10)} {space}
                                    <span>
                                        <FormattedMessage id="app.time" defaultMessage="Время"/>
                                    </span>: {question.datetime && new Date(questionAnswer.datetime).toISOString().slice(11, 19)}
                                </p>
                                {!questionAnswer.user.fullName &&
                                <h4 className={classes.fullName}>{questionAnswer.fullName}</h4>}
                                <p className={classes.description}>{questionAnswer.answer}</p>
                                <LawyerRating
                                    question={question}
                                    user={user}
                                    lawyer={questionAnswer.user._id}
                                    answer={questionAnswer._id}
                                    rate={questionAnswer.rate}
                                />

                            </Paper>
                        )
                    })}
                </div>
                {user && user.role === "lawyer" &&
                <form className={classes.addAnswerSection}>
                    <FormattedMessage id="app.yourAnswer" defaultMessage="Ваш ответ на вопрос">
                        {(placeholder) => (
                            <textarea
                                className={classes.textarea} placeholder={placeholder}
                                value={answer.answer} onChange={getAnswerHandler}
                            />
                        )}
                    </FormattedMessage>
                    <Button
                        className={classes.addAnswerBtn}
                        variant="contained" color="primary"
                        onClick={addAnswerHandler}
                    >
                        <FormattedMessage id="app.answerQuestion" defaultMessage="Ответить"/>
                    </Button>
                </form>
                }
            </div> : <div>
                ERROR 404
            </div>}
            <AboutProjectBtn/>
        </div>
    );
}

export default QuestionPage;