import {FormControl, InputLabel, makeStyles, MenuItem, Select} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getSortedLawyersList} from "../../store/actions/usersListActions";
import Container from "@material-ui/core/Container";
import UsersCard from "../../components/UsersCard/UsersCard";
import {getRegions} from "../../store/actions/regionsActions";
import {getCategories} from "../../store/actions/categoriesActions";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation
  from "../../components/CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";
import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles({
  root: {
    display: 'flex',
    marginTop: "260px"
  },
  content: {
    marginLeft: "30px",
    width: "100%",
  },
  list: {
    width: "auto",
    position: "absolute",
    top: "80px",
    left: 0,
    right: 0,
    border: "1px solid #5280CE",
    padding: "15px 0 15px 0"
  },
  formControl: {
    margin: "0 auto 10px",
    width: '15%',
    backgroundColor: "white",
    marginLeft: 15
  },
  button: {
    marginLeft: 10,
    padding: 15
  }
});

const LawyerRating = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const lawyers = useSelector(state => state.usersList.lawyers);
  const language = localStorage.getItem("language");
  const regions = useSelector(state => state.regions.regions);
  const categories = useSelector(state => state.categories.categories);
  const totalLawyers = useSelector(state => state.questions.totalQuestions);

  const [inputData, setInputData] = useState({
    role: '',
    region: '',
    category: '',
  });

  const [currentPage, setCurrentPage] = useState(1);
  const [lawyersLength, setLawyersLength] = useState(0);
  const [fetching, setFetching] = useState(true);
  const [allLawyers, setAllLawyers] = useState([]);

  useEffect(() => {
    dispatch(getRegions());
    dispatch(getCategories());
  }, [dispatch]);

  const changeRegion = event => {
    setInputData({
      ...inputData,
      region: event.target.value,
    });
  };

  const changeCategory = event => {
    setInputData({
      ...inputData,
      category: event.target.value,
    });
  };

  const changeRole = event => {
    setInputData({
      ...inputData,
      role: event.target.value,
    });
  };

  const getLawyers = () => {
    setAllLawyers([]);
    setCurrentPage(1);
    setLawyersLength(0);
    setFetching(true);
  }

  useEffect(() => {
    if (fetching) {
      setCurrentPage(prevState => prevState + 1);
      dispatch(getSortedLawyersList(inputData.category, inputData.region, inputData.role, currentPage));
      if (lawyers) {
        setLawyersLength(prevState => prevState + lawyers.length);
      }
      setFetching(false);
    }
  }, [dispatch, fetching]);

  useEffect(() => {
    if (lawyers) {
      const allQuestionsCopy = [...allLawyers];
      lawyers.map(lawyer => {
        allQuestionsCopy.push(lawyer);
        return allQuestionsCopy;
      });
      setAllLawyers(allQuestionsCopy);
    }
  }, [lawyers]);

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler);
    return function () {
      document.removeEventListener('scroll', scrollHandler);
    }
  });

  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
        && lawyersLength < totalLawyers) {
      setFetching(true);
    }
  };

  return (
      <div className={classes.root}>
        <div className={classes.list}>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel id="demo-simple-select-label">
              <FormattedMessage id="app.chooseSpecialist" defaultMessage="Выбрать специалиста"/>
            </InputLabel>
            <Select
                labelId="demo-simple-select-label"
                id="answers-select"
                required
                fullWidth
                label="Сортировать по ответам:"
                value={inputData.role}
                onChange={changeRole}
            >
              <MenuItem value={"lawyer_accountant"}>
                <FormattedMessage id="app.allSpecialists" defaultMessage="Все специалисты"/>
              </MenuItem>
              <MenuItem value={"accountant"}>
                <FormattedMessage id="app.accountants" defaultMessage="Бухгалтера"/>
              </MenuItem>
              <MenuItem value={"lawyer"}>
                <FormattedMessage id="app.lawyers" defaultMessage="Юристы"/>
              </MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel id="demo-simple-select-label">
              <FormattedMessage id="app.category" defaultMessage="Категория"/>
            </InputLabel>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                required
                fullWidth
                label="Категория"
                value={inputData.category}
                onChange={changeCategory}
            >
              {categories && categories.map(c => {
                return <MenuItem key={c._id} id={c.title} value={c._id}>
                  <CategoriesAndRegionsTranslation
                      kgTitle={c.kgTitle}
                      engTitle={c.engTitle}
                      rusTitle={c.rusTitle}/>
                </MenuItem>
              })}
            </Select>
          </FormControl>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel id="demo-simple-select-label">
              <FormattedMessage id="app.region" defaultMessage="Регион"/>
            </InputLabel>
            <Select
                labelId="demo-simple-select-label"
                id="region-select"
                required
                fullWidth
                label="Регион:"
                value={inputData.region}
                onChange={changeRegion}
            >
              {regions && regions.map(c => {
                return <MenuItem key={c._id} id={c.title} value={c._id}>
                  <CategoriesAndRegionsTranslation
                      kgTitle={c.kgTitle}
                      engTitle={c.engTitle}
                      rusTitle={c.rusTitle}/>
                </MenuItem>
              })}
            </Select>
          </FormControl>
          <Button variant="outlined" onClick={getLawyers} className={classes.button}><SearchIcon/>
            <FormattedMessage id="app.search" defaultMessage="Поиск"/>
          </Button>
        </div>
        <div className={classes.content}>
          <Container>
            {allLawyers && allLawyers.map(lawyer => {
              return <UsersCard
                  rating={lawyer.rating}
                  key={lawyer._id}
                  id={lawyer._id}
                  fullName={lawyer.fullName}
                  categories={language === "ru-RU" ? lawyer.category.rusTitle : language === "en-US" ? lawyer.category.engTitle : lawyer.category.kgTitle}
                  city={lawyer.city}
                  image={lawyer.image}
              />
            })}
          </Container>
        </div>
        <AboutProjectBtn/>
      </div>
  );
};

export default LawyerRating;