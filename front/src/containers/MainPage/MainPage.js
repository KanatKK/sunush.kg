import {makeStyles} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import CategoriesNavBar from "../../components/CategoriesNavBar/CategoriesNavBar";
import {useDispatch, useSelector} from "react-redux";
import {getQuestions, getQuestionsSuccess} from "../../store/actions/questionActions";
import Modal from "../../components/UI/Modal";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import Search from "../../components/Search/Search";
import MainCarousel from "../../components/UI/MainCarousel/MainCarousel";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        top: '200px',
        display: 'flex',
        marginTop: "30px"
    },
    content: {
        marginTop: '20px',
        flexGrow: '1',
        marginLeft: "30px",
        width: "100%",
    },
    navBar: {
        backgroundColor: "#925050",
        display: "block",
        marginTop: "65px",
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    addBtn: {
        display: 'block',
        '&:hover': {
            color: "green"
        }
    },
    delBtn: {
        display: 'block',
        '&:hover': {
            color: "red"
        }
    },
    card: {
        width: "80%",
        padding: "20px",
        backgroundColor: "#f6f2f2",
        marginBottom: "20px",
        marginRight: '20px',
        marginLeft: "auto",
        boxShadow: "5px 5px 17px 0px rgba(0, 0, 0, 0.3)",
        borderRadius: "5px",
        position: "relative"
    },
    search: {
        position: 'fixed',
        backgroundColor: "white",
        top: '165px',
        zIndex: '1',
        padding: '10px'

    }
}));

const MainPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const questions = useSelector(state => state.questions.questions);
  const message = useSelector(state => state.questions.successMessage);
  const totalQuestions = useSelector(state => state.questions.totalQuestions);
  const user = useSelector(state => state.user.user);
  const category = useSelector(state => state.questions.category);

  const [currentPage, setCurrentPage] = useState(1);
  const [questionsLength, setQuestionsLength] = useState(0);
  const [fetching, setFetching] = useState(true);
  const [allQuestions, setAllQuestions] = useState([]);

  useEffect(() => {
    setAllQuestions([]);
    setCurrentPage(1);
    setFetching(true);
  }, [category]);

  useEffect(() => {
    if (fetching) {
      setCurrentPage(prevState => prevState + 1);
      dispatch(getQuestions(category, currentPage));
      if (questions) {
        setQuestionsLength(prevState => prevState + questions.length);
      }
      setFetching(false);
    }
  }, [dispatch, fetching]);

  useEffect(() => {
    if (questions) {
      const allQuestionsCopy = [...allQuestions];
      questions.map(question => {
        allQuestionsCopy.push(question);
        return allQuestionsCopy;
      });
      setAllQuestions(allQuestionsCopy);
      dispatch(getQuestionsSuccess(null));
    }
  }, [questions]);

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler);
    return function () {
      document.removeEventListener('scroll', scrollHandler);
    }
  });

  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
        && questionsLength < totalQuestions) {
      setFetching(true);
    }
  };

    return (
        <>
            <MainCarousel
                navButtonsAlwaysVisible={false}

            />

            <div className={classes.root}>

                <CategoriesNavBar/>
                <div className={classes.content}>
                    <div className={classes.search}><Search/></div>
                    {message !== null ? <Modal/> : null}
                    {allQuestions && allQuestions.map(question => {
                        if ((question.published ||
                            (user && (user.role === 'admin' || user.role === 'moderator'))) && !question.deleted) {
                            return (
                                <QuestionCard
                                    key={question._id}
                                    image={question.user.image}
                                    classname={classes.card}
                                    description={question.description}
                                    username={question.user.fullName ? question.user.fullName : question.user.username}
                                    userId={question.user._id}
                                    datetime={question.datetime}
                                    id={question._id}
                                    published={question.published}
                                    title={question.title}
                                    answers={question.answers}
                                />
                            )
                        } else return null
                    })}
                    {allQuestions.length < 1 && <p className={classes.text}>По вашему запросу ничего не найдено...</p>}
                </div>
                <AboutProjectBtn/>
            </div>
        </>
    );
};

export default MainPage;