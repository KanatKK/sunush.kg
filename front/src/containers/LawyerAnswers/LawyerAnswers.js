import {makeStyles} from '@material-ui/core';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Modal from "../../components/UI/Modal";
import QuestionCard from "../../components/QuestionCard/QuestionCard";
import {getMyAnswers} from "../../store/actions/answersActions";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles({
    root: {
        display: 'flex',
        marginTop: "130px"
    },
    content: {
        marginLeft: "30px",
        width: "100%",
    },
    navBar: {
        backgroundColor: "#925050",
        display: "block",
        marginTop: "65px",
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    addBtn: {
        display: 'block',
        '&:hover': {
            color: "green"
        }
    },
    delBtn: {
        display: 'block',
        '&:hover': {
            color: "red"
        }
    },
    card: {
        fullWidth: "100%",
        padding: "20px",
        backgroundColor: "#f6f2f2",
        marginBottom: "20px",
        boxShadow: "5px 5px 17px 0px rgba(0, 0, 0, 0.3)",
        borderRadius: "5px",
        position: "relative"
    }
});

const LawyerAnswers = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const answers = useSelector(state => state.answers.answers);
    const message = useSelector(state => state.questions.successMessage);
    const user = useSelector(state => state.user.user);
    const totalAnswers = useSelector(state => state.questions.totalQuestions);

    const [currentPage, setCurrentPage] = useState(1);
    const [answersLength, setAnswersLength] = useState(0);
    const [fetching, setFetching] = useState(true);
    const [allAnswers, setAllAnswers] = useState([]);

    useEffect(() => {
        if (fetching) {
            setCurrentPage(prevState => prevState + 1);
            dispatch(getMyAnswers(user._id, currentPage));
            if (answers) {
                setAnswersLength(prevState => prevState + answers.length);
            }
            setFetching(false);
        }
    }, [dispatch, fetching]);

    useEffect(() => {
        if (answers) {
            const allQuestionsCopy = [...allAnswers];
            answers.map(answer => {
                allQuestionsCopy.push(answer);
                return allQuestionsCopy;
            });
            setAllAnswers(allQuestionsCopy);
        }
    }, [answers]);

    useEffect(() => {
        document.addEventListener('scroll', scrollHandler);
        return function () {
            document.removeEventListener('scroll', scrollHandler);
        }
    });

    const scrollHandler = (e) => {
        if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
            && answersLength < totalAnswers) {
            setFetching(true);
        }
    };

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                {message !== null ? <Modal/> : null}
                {allAnswers !== null ? allAnswers.map(answer => {
                    return (
                        <QuestionCard
                            key={answer.question._id}
                            classname={classes.card}
                            description={answer.question.description}
                            username={answer.question.user.fullName}
                            userId={answer.question.user._id}
                            datetime={answer.question.datetime}
                            id={answer.question._id}
                            published={answer.question.published}
                            title={answer.question.title}
                            answers={answer.question.answers}
                        />
                    );
                }): <p>Вы еще не отвечали на вопросы ...</p>}
            </div>
            <AboutProjectBtn/>
        </div>
    );
};

export default LawyerAnswers;