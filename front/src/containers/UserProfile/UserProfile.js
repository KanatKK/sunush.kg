import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from '@material-ui/core/styles';
import {Link} from "react-router-dom";
import {apiURL, defaultAvatar} from "../../constants";
import Grid from '@material-ui/core/Grid';
import Divider from "@material-ui/core/Divider";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import {FormattedMessage} from "react-intl";
import {fetchGetUserProfileData, createChat} from "../../store/actions/userActions";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Rating from "@material-ui/lab/Rating";
import AboutProjectBtn from "../../components/AboutProjectBtn/AboutProjectBtn";

const useStyles = makeStyles({
    root: {
        width: "70%",
        marginTop: 100
    },
    media: {
        width: 200,
        height: 200,
        borderRadius: "50%",
        margin: 'auto'
    },
    padding: {
        paddingBottom: 20
    },
    rating: {
        marginLeft: '15px'
    },
    message: {
        position: 'absolute',
        top: '20px',
        right: '20px'
    },
    profileHeader: {
        position: 'relative'
    },
    messageLink: {
        color: '#ffffff',
        textDecoration: 'none'
    }
});

const UserProfile = props => {
    const user = useSelector(state => state.user.user);
    const anotherUser = useSelector(state => state.user.userProfileData);
    let userDataArray;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchGetUserProfileData(props.match.params.id));
    }, [dispatch, props.match.params.id]);

   if(anotherUser !== undefined){
       if(user._id === anotherUser._id){
           userDataArray = user;
       }else{
           userDataArray = anotherUser;
       }
   }
    let image = defaultAvatar;
    if (userDataArray && userDataArray.image) {
        image = apiURL + "/uploads/" + userDataArray.image;
    }

    const newChat = () => {
      dispatch(createChat({
          with: anotherUser._id,
          nameWith: anotherUser.fullName
      }));
    };

    const classes = useStyles();

    return (
        <Box py={4}>
            <Grid container direction="row" alignItems="center" justify="center">
                <Card className={classes.root}>
                    <div className={classes.profileHeader}>
                        <Grid item>
                            <Box p={2}>
                                <Typography component='h4' variant='h4'>
                                    {userDataArray ? userDataArray.fullName !== undefined ?
                                        userDataArray.fullName : userDataArray.username : null}
                                </Typography>
                            </Box>
                        </Grid>
                        {anotherUser && anotherUser.role !== 'user' ?
                        <Rating
                            className={classes.rating}
                            readOnly
                            defaultValue={anotherUser.rating}
                            precision={0.1}
                            emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                        /> : null}
                        {(user && anotherUser) && user._id !== anotherUser._id ?
                            < Button variant="contained"
                                     color="primary"
                                     onClick={newChat}
                                     className={classes.message}
                                     component={Link} to={anotherUser && "/chat/" + anotherUser._id}>
                                Написать
                            </Button> : null}
                    </div>
                    <Divider/>
                    <Box p={2}>
                        <Grid container direction="row" alignItems="center" spacing={2}>
                            <Grid item xs={12} md={4}>
                                <Box m='auto' width={{xs: 100, lg: 200}} height={{xs: 100, lg: 200}}>
                                    <CardMedia
                                        className={classes.media}
                                        image={image}
                                    />
                                </Box>
                            </Grid>
                            <Grid item xs={12} md={8}>
                                {userDataArray && userDataArray.fullName &&
                                <Typography variant="h5" color="textSecondary" component="h5" className={classes.padding}>
                                    <FormattedMessage id="app.fullName" defaultMessage="ФИО">
                                        {(label) => (
                                            <span>{label}: {userDataArray.fullName}</span>
                                        )}
                                    </FormattedMessage>
                                </Typography>}

                                {userDataArray && userDataArray.city && <Typography variant="h5" color="textSecondary" component="h5" className={classes.padding}>
                                    <FormattedMessage id="app.city" defaultMessage="Город">
                                        {(label) => (
                                            <span>{label}: {userDataArray.city}</span>
                                        )}
                                    </FormattedMessage>
                                </Typography>}
                                {userDataArray && userDataArray.experience &&
                                <Typography variant="h5" color="textSecondary" component="h5"
                                            className={classes.padding}>
                                    <FormattedMessage
                                        id="app.experience_user_info"
                                        defaultMessage="Опыт работы"
                                        values={{message: userDataArray.experience}}
                                    >
                                    </FormattedMessage>
                                </Typography>}
                                {userDataArray && userDataArray.address &&
                                <Typography variant="h5" color="textSecondary" component="h5"
                                            className={classes.padding}>
                                    <FormattedMessage id="app.address" defaultMessage="Адрес">
                                        {(label) => (
                                            <span>{label}: {userDataArray.address}</span>
                                        )}
                                    </FormattedMessage>
                                </Typography>}
                                {userDataArray && userDataArray.phone &&
                                <Typography variant="h5" color="textSecondary" component="h5"
                                            className={classes.padding}>
                                    <FormattedMessage id="app.phoneNumber" defaultMessage="Телефон">
                                        {(label) => (
                                            <span>{label}: {userDataArray.phone}</span>
                                        )}
                                    </FormattedMessage>
                                </Typography>}
                            </Grid>
                        </Grid>
                    </Box>
                    <Divider/>
                    <Box p={2}>
                        {userDataArray && userDataArray._id === user._id && <Grid container justify="flex-end">
                            <Button component={Link} color="primary" variant="contained" to="/edit_my_profile">
                                <FormattedMessage
                                    id="app.editProfile"
                                    defaultMessage="Редактировать профиль"/>
                            </Button>
                        </Grid>}
                    </Box>
                </Card>
            </Grid>
            <AboutProjectBtn/>
        </Box>
    );
};

export default UserProfile;