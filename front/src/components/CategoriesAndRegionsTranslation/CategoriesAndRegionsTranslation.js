import React from 'react';
import {englishLanguageCode, kyrgyzLanguageCode, russianLanguageCode} from "../../localization/Wrapper";

const CategoriesAndRegionsTranslation = props => {
    const language = localStorage.getItem('language');
    return (
        <>
            {language === kyrgyzLanguageCode ? props.kgTitle : null}
            {language === englishLanguageCode ? props.engTitle : null}
            {language === russianLanguageCode ? props.rusTitle : null}
            {language === null ? props.rusTitle : null}
        </>
    );
};

export default CategoriesAndRegionsTranslation;