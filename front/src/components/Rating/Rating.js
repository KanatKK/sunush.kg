import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import Chip from '@material-ui/core/Chip';
import {useDispatch, useSelector} from "react-redux";
import {addRating} from "../../store/actions/ratingActions";
import {FormattedMessage} from "react-intl";

const useStyles = makeStyles({
  root: {
    marginTop: '15px',
    marginLeft: 'auto',
    marginRight: '10px',
    width: 200,
    display: 'flex',
    alignItems: 'center',
  },
  done: {
    marginLeft: '10px'
  },
  root2: {
    marginTop: '15px',
    marginLeft: 'auto',
    marginRight: '10px',
    width: 120,
    display: 'flex',
    alignItems: 'center',
  }
});

const LawyerRating = (props) => {
  const user = useSelector(state => state.user.user);
  const [rating, setRating] = useState({
    user: user,
    lawyer: props.lawyer,
    answer: props.answer,
    rate: props.rate
  });
  const [rateBtn, setRateBtn] = useState(true);
  const classes = useStyles();
  const dispatch = useDispatch();

  const changeRatingHandler = event => {
    setRating({
      ...rating,
      rate: event.target.value
    });
    setRateBtn(false);
  };

  const postRating = data => {
    dispatch(addRating(data));
    setRateBtn(true);
  }

  let stars;

    if ((user && props.question && props.question.user) && user._id === props.question.user._id) {
        stars = (<div className={classes.root}>
            <Rating
                value={rating.rate}
                precision={1}
                onChange={changeRatingHandler}
            />
            <FormattedMessage id="app.rate" defaultMessage="Оценить">
                {(label) => (
                    <Chip
                        label={label}
                        clickable
                        disabled={rateBtn}
                        color="primary"
                        variant="outlined"
                        className={classes.done}
                        onClick={() => postRating(rating)}
                    />
                )}
            </FormattedMessage>
        </div>);
    } else {
        stars = (
            <div className={classes.root2}>
                <Rating value={rating.rate} readOnly/>
            </div>
        );
    }

  return (
      <>
        {stars}
      </>
  );
};

export default LawyerRating;