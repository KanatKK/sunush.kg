import React, {useEffect, useState} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SearchIcon from '@material-ui/icons/Search';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch, useSelector} from "react-redux";
import {getCategories, addCategory, deleteCategory} from "../../store/actions/categoriesActions";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {getQuestions} from "../../store/actions/questionActions";
import {NavLink} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import CategoriesAndRegionsTranslation from "../CategoriesAndRegionsTranslation/CategoriesAndRegionsTranslation";


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    zIndex: "2",
  },
  form: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
        width: '50px',
      flexShrink: 0,
    },
  },
  toolbar: {
      height: "40px",
  },
  toolbar2: {
    minHeight: "56px"
  },
  drawerPaper: {
      marginTop: '120px',
    width: '300px',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  field: {
    marginBottom: '5px'
  },
  del: {
    '&:hover': {
      color: "red"
    }
  },
  link: {
    textDecoration: "none",
    fontSize: "1rem",
    fontFamily: "Roboto, Helvetica, Arial, sans-serif",
    fontWeight: 400,
    lineHeight: 1.5,
    letterSpacing: "0.00938em",
    color: "rgba(0, 0, 0, 0.87)"
  }
}));

const ResponsiveDrawer = () => {
    const classes = useStyles();
    const categories = useSelector(state => state.categories.categories);
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();
    const [category, setCategory] = useState({
        rusTitle: "",
        kgTitle: "",
        engTitle: "",
    });

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  const onChangeHandler = e => {
    setCategory({
      ...category,
      [e.target.name]: e.target.value,
    });
  };

  const add = async () => {
    if (category !== '') {
      await dispatch(addCategory(category));
      dispatch(getCategories());
      setCategory({
        rusTitle: "",
        kgTitle: "",
        engTitle: "",
      });
    }
  };

  const del = async (id) => {
    await dispatch(deleteCategory(id));
    dispatch(getCategories());
  };


  let textField = null;
  if (user && user.role === 'admin') {
    textField = (
        <div className={classes.form}>
          <TextField id="outlined-basic"
                     label="Русский"
                     variant="outlined"
                     fullWidth
                     name="rusTitle"
                     value={category.rusTitle}
                     onChange={onChangeHandler}
                     className={classes.field}
          />
          <TextField id="outlined-basic"
                     label="Кыргызсча"
                     variant="outlined"
                     fullWidth
                     name="kgTitle"
                     value={category.kgTitle}
                     onChange={onChangeHandler}
                     className={classes.field}
          />
          <TextField id="outlined-basic"
                     label="English"
                     variant="outlined"
                     name="engTitle"
                     fullWidth
                     value={category.engTitle}
                     onChange={onChangeHandler}
                     className={classes.field}
          />
          <Button variant="contained"
                  color="primary"
                  fullWidth
                  onClick={add}
          >
            Добавить категорию
          </Button>
        </div>
    )
  }

  const getQuestionsByCategory = (id) => {
    dispatch(getQuestions(id, 1));
  };

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <nav className={classes.drawer} aria-label="mailbox folders">
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        <div>
                            <div className={classes.toolbar}/>
                            <Divider/>
                            <List>
                                <ListItem>
                                    <ListItemIcon><SearchIcon/></ListItemIcon>
                                    <NavLink color="primary" to="/sorting" className={classes.link}>
                                        <FormattedMessage id="app.advancedSearch" defaultMessage="Расширенный поиск"/>
                                    </NavLink>
                                </ListItem>
                                <ListItem
                                    button
                                    onClick={() => getQuestionsByCategory(null)}
                                >
                                    <ListItemIcon><InboxIcon/></ListItemIcon>
                                    <FormattedMessage id="app.allQuestions_Category" defaultMessage="Все вопросы">
                                        {(text) => (
                                            <ListItemText primary={text}/>
                                        )}
                                    </FormattedMessage>
                                </ListItem>
                                {categories.map(category => (
                                    <ListItem
                                        button key={category._id}
                                        onClick={() => getQuestionsByCategory(category._id)}
                                    >
                                        <ListItemIcon>{user && user.role === 'admin' ?
                                            <HighlightOffIcon
                                                className={classes.del}
                                                onClick={() => del(category._id)}
                                            />
                                            : <InboxIcon/>}</ListItemIcon>
                                        <ListItemText primary={category.title}>
                                            <CategoriesAndRegionsTranslation
                                                kgTitle={category.kgTitle}
                                                engTitle={category.engTitle}
                                                rusTitle={category.rusTitle}/>
                                        </ListItemText>
                                    </ListItem>
                                ))}
                            </List>
                        </div>
                        {textField}
                    </Drawer>
                </Hidden>
            </nav>
        </div>
    );
};

export default ResponsiveDrawer;