import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core';
import {FormattedMessage} from "react-intl";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Avatar from "@material-ui/core/Avatar";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import Typography from "@material-ui/core/Typography";
import InputMask from "react-input-mask";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {findUser} from "../../store/actions/userActions";
import swal from "sweetalert";
import firebase from "../../constants";
import {push} from "connected-react-router";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
    buttons: {
        padding: "15px 15px",
        position: "absolute",
        marginLeft: "15px"
    },
    search: {
        marginTop: '100px'
    },
    buttons2: {
        padding: "15px 15px",
        position: "absolute",
        marginLeft: "130px"
    },
    root: {
        marginTop: "200px"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(2, 0, 0),
    },
    link: {
        marginRight: 10
    },
    editPhone: {
        cursor: "pointer",
    },
    set: {
        marginTop: 5
    }
}));

const FindAUser = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const success = useSelector(state => state.user.setPassword);
    const user = useSelector(state => state.user.find_user);
    const [codeBtn, setCodeBtn] = useState(false);
    const [password, setPassword] = useState(true);
    const [phone, setPhone] = useState({
        phone: ''
    });

    const confirmTheNumber = () => {
        setUpRecaptcha();
        setPassword(!password);
        const phoneNumber = '+996' + (user[0].phone);
        const appVerifier = window.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
                swal("Отлично :)", "Вам было отправлено смс с кодом", "success");
                window.confirmationResult = confirmationResult;
                const code = window.prompt("Enter code");
                window.confirmationResult = confirmationResult;
                confirmationResult.confirm(code).then((result) => {
                    swal("Отлично!", "Ваш номер успешно подтвержден!", "success");
                    dispatch(push(`/setPassword`));
                }).catch((error) => {
                    swal("Провал :(", "Ваш номер не подтвержден!", "error");
                    setUpRecaptcha();
                })
            });
    };

    const setUpRecaptcha = () => {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
            'recaptcha-container', {
                'size': 'invisible',
                'hl': 'ru',
                'callback': (response) => {
                    confirmTheNumber();
                }
            });
    };

    const findAUser = event => {
        event.preventDefault();
        if (phone.phone !== '') {
            dispatch(findUser(phone.phone));
            setCodeBtn(!codeBtn);
            setPassword(true);
            swal("Отлично :)", "Ваш аккаунт найден!", "success");
        } else {
            setPassword(!password);
            swal("Провал :(", "Введите номер!", "error");
        }
    };

    const changePassword = event => {
        event.preventDefault();
        confirmTheNumber();
    };

    const changePhone = event => {
        const value = event.target.value;
        setPhone({
            ...phone,
            phone: (value.replace(/[+][(]\d+[)]/, '')).replace(/\s/g, ''),
        });
    };

    const editPhone = () => {
        setCodeBtn(false);
        setPassword(false);
        setPhone({
            ...phone,
            phone: '',
        });
    };

    return (
        <Container component="main" maxWidth="xs" className={classes.root}>
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    <FormattedMessage
                        id="app.forgotYourPassword"
                        defaultMessage="Вход"/>
                </Typography>
                <form className={classes.form}>
                    <div id="recaptcha-container"></div>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FormattedMessage id="app.phoneNumber" defaultMessage="Номер телефона">
                                {(label) => (
                                    <InputMask
                                        mask="+(\9\96) 999 99 99 99"
                                        onChange={changePhone}
                                        value={phone.phone}>
                                        {(inputProps) => <TextField
                                            {...inputProps} type="tel"
                                            variant="outlined"
                                            required={true}
                                            fullWidth
                                            name="phone"
                                            label={label}
                                            id="phone"
                                            autoComplete="current-phone"/>}
                                    </InputMask>
                                )}
                            </FormattedMessage>
                        </Grid>
                    </Grid>
                    {codeBtn === true ? <Button
                            id={"code"}
                            type="button"
                            fullWidth
                            variant="contained"
                            disabled={true}
                            color="primary"
                            className={classes.submit}
                            onClick={findAUser}
                        >
                            <FormattedMessage
                                id="app.findAccount"
                                defaultMessage="Найти аккаунт"/>
                        </Button> :
                        <Button
                            id={"code"}
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={findAUser}
                        >
                            <FormattedMessage
                                id="app.findAccount"
                                defaultMessage="Найти аккаунт"/>
                        </Button>}
                    {success === true && password === true? <Button
                            id={"register"}
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={changePassword}
                        >
                            <FormattedMessage
                                id="app.setPassword"
                                defaultMessage="Сменить пароль"/>
                        </Button> :
                        <Button
                            id={"register"}
                            type="submit"
                            disabled={true}
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={changePassword}
                        >
                            <FormattedMessage
                                id="app.setPassword"
                                defaultMessage="Сменить пароль"/>
                        </Button>
                    }
                    <Grid container justify="flex-end" className={classes.set}>
                        <Grid item>
                            <FormattedMessage
                                id="app.anotherNumber"
                                defaultMessage="Ввести другой номер">
                                {(title) => (
                                    <Link onClick={editPhone} variant="body2" className={classes.editPhone}>
                                        {title}
                                    </Link>
                                )}
                            </FormattedMessage>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
};

export default FindAUser;