import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import { InputAdornment } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import {FormattedMessage} from "react-intl";
import {editPassword} from "../../store/actions/userActions";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "200px"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    link: {
        marginRight: 10
    }
}));

export default function ForgotYourPassword() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.find_user);
    const [inputData, setInputData] = useState({
        password: ''
    });

    const setPassword = () => {
        dispatch(editPassword(inputData.password, user[0].token));
    };

    const [showPassword, setShowPassword] = useState(false);

    const changeInputData = event => {
        const name = event.target.name;
        const value = event.target.value;
            setInputData({
                ...inputData,
                [name]: value,
            });
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    return (
        <Container component="main" maxWidth="xs" className={classes.root}>
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    <FormattedMessage
                        id="app.si"
                        defaultMessage="Сменить пароль"/>
                </Typography>
                <form className={classes.form} onClick={setPassword}>
                    <Grid>
                        <Grid item xs={12}>
                            <FormattedMessage id="app.password" defaultMessage="Пароль">
                                {(label) => (
                                    <TextField
                                        variant="outlined"
                                        required={true}
                                        fullWidth
                                        name="password"
                                        onChange={changeInputData}
                                        label={label}
                                        value={inputData.password}
                                        type={showPassword ? 'text' : 'password'}
                                        id="password"
                                        autoComplete="current-password"
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={handleClickShowPassword}
                                                        onMouseDown={handleMouseDownPassword}
                                                        edge="end"
                                                    >
                                                        {showPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                )}
                            </FormattedMessage>
                        </Grid>
                    </Grid>
                    <Button
                        id={"login"}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        <FormattedMessage
                            id="app.sin"
                            defaultMessage="Сменить пароль"/>
                    </Button>
                </form>
            </div>
        </Container>
    );
}