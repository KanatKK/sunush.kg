import React, {useContext, useEffect, useRef, useState} from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import {Button, makeStyles, MenuItem, Select} from "@material-ui/core";
import {Link} from "react-router-dom";
import UserMenu from "../../Menu/UserMenu";
import Anonymous from "../../Menu/Anonymous";
import {useDispatch, useSelector} from "react-redux";
import ChatMenu from "../../Menu/ChatMenu";
import TranslateIcon from '@material-ui/icons/Translate';
import {Context, kyrgyzLanguageCode, russianLanguageCode, englishLanguageCode} from "../../../localization/Wrapper";
import {FormattedMessage} from "react-intl";
import {getCategories} from "../../../store/actions/categoriesActions";
import {getLawyersList} from "../../../store/actions/usersListActions";
import kgFlag from "../../../accepts/images/localization/kg.svg";
import {createChat} from "../../../store/actions/userActions";
import {
    getNotifications,
    addNotificationSuccess,
    receiveNewMessages,
    getMessageNotices
} from "../../../store/actions/notificationsActions";
import usFlag from "../../../accepts/images/localization/us.svg";
import rusFlag from "../../../accepts/images/localization/ru.svg";
import NoticeMenu from "../../Menu/NoticeMenu";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        zIndex: "100",
        position: "fixed",
        top: "0",
        right: "0",
        left: "0",
        marginBottom: '50px',

    },
    inner: {
        display: "flex",
        justifyContent: 'space-between'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {},
    logo: {
        height: '60px',
        width: 'auto',
        marginLeft: '20px',
        marginBottom: '10px',
        marginTop: '10px'
    },
    loginLink: {
        color: "#fff",
        textDecoration: "none",
    },
    dropdownBtn: {
        color: "#FFFFFF"
    },
    logoCont: {
        display: 'flex'
    },
    sunush: {
        color: 'white',
        textDecoration: 'none'
    },
    sunushTitle: {
        margin: '0'
    },
    link: {
        color: 'white',
        textDecoration: 'none',
        textTransform: 'uppercase',
        fontSize: '16px',
    },
    userMenuBox: {
        display: "flex",
        flexDirection: "row",
    },
    selectLanguage: {
        opacity: 0,
        width: 0
    },
    selectLanguageButton: {
        color: "white"
    },
    iconBox: {
        position: "relative"
    },
    flagImage: {
        position: "absolute",
        bottom: "0",
        left: "-10px",
        width: "20px"
    },
}));

const AppToolbar = () => {
    const user = useSelector(state => state.user.user);
    const classes = useStyles();
    const dispatch = useDispatch();
    const context = useContext(Context);
    const messageNotices = useSelector(state => state.notifications.messageNotices);
    const [anchorEl, setAnchorEl] = useState(null);
    const ws = useRef(null);

    useEffect(() => {
        if (user && user.role === "user") {
            dispatch(getNotifications(user._id));
        }
        if (user && (user.role === "user" || user.role === "lawyer" || user.role === "accountant")) {
            dispatch(getMessageNotices());
        }
    }, [user, dispatch]);

    useEffect(() => {
        if (user && (user.role === "lawyer" || user.role === "accountant" || user.role === "user")) {
            ws.current = new WebSocket("ws://localhost:8000/socket?id=" + user._id);
            let refresh;
            ws.current.onopen = () => {
                clearInterval(refresh);
                console.log('Main connection established');
            };

            ws.current.onmessage = e => {
                const decodedMessage = JSON.parse(e.data);
                if (decodedMessage.type === "PUBLIC_YOUR_QUESTION") {
                    dispatch(addNotificationSuccess(decodedMessage.data));
                } else if (decodedMessage.type === "DELETE_YOUR_QUESTION") {
                    dispatch(addNotificationSuccess(decodedMessage.data));
                } else if (decodedMessage.type === "ANSWER_YOUR_QUESTION") {
                    dispatch(addNotificationSuccess(decodedMessage.data));
                } else if (decodedMessage.type === "HAVE_NEW_MESSAGE") {
                    dispatch(receiveNewMessages(decodedMessage.notice));
                    dispatch(createChat(decodedMessage.interlocutor));
                }
            };

            ws.current.onclose = () => {
                refresh = setInterval(() => {
                    ws.current = new WebSocket("ws://localhost:8000/socket?id=" + user._id);
                    console.log('Connect failure');
                }, 1000);
                console.log("Main connection closed");
            };

            return () => {
                ws.current.close();
            };
        } else return null;
    }, [user, dispatch]);


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const changeCategoryTitleLanguage = () =>{
        dispatch(getCategories());
        dispatch(getLawyersList());
    };

    let roleMenu;
    if (user && user.role === 'user') {
        roleMenu = (
            <Breadcrumbs aria-label="breadcrumb">
                <Link className={classes.link}
                      id={"add_application"}
                      to="/add_application">
                    <FormattedMessage
                        id="app.askQuestion_HeaderNavigation"
                        defaultMessage="Задать вопрос"/>
                </Link>
                <Link className={classes.link}
                      to={"/applications/" + user._id}
                >
                    <FormattedMessage
                        id="app.myQuestions_HeaderNavigation"
                        defaultMessage="Мои вопросы"
                    />
                </Link>
                <Link className={classes.link}
                      to="/lawyers_Rating/">
                    <FormattedMessage
                        id="app.findLawyer_HeaderNavigation"
                        defaultMessage="Найти юриста"/>
                </Link>
                <ChatMenu invisible={!(messageNotices.length > 0)}
                />
                <NoticeMenu/>
            </Breadcrumbs>
        );
    } else if (user && (user.role === 'lawyer' || user.role === "accountant")) {
        roleMenu = (
            <Breadcrumbs aria-label="breadcrumb">
                <Link className={classes.link}
                      to="/lawyer_applications">
                    <FormattedMessage
                        id="app.applicationsForMe_HeaderNavigation"
                        defaultMessage="Заявки подходящие мне"/>
                </Link>
                <Link className={classes.link}
                      to="/lawyer_answers">
                    <FormattedMessage
                        id="app.myAnswers_HeaderNavigation"
                        defaultMessage="Мои ответы"/>
                </Link>
                <Link className={classes.link}
                      to="/lawyers_Rating/">
                    <FormattedMessage
                        id="app.lawyerRating_HeaderNavigation"
                        defaultMessage="Рейтинг юристов"/>
                </Link>
                <ChatMenu
                    invisible={!(messageNotices.length > 0)}
                />
            </Breadcrumbs>
        );
    } else if (user && (user.role === 'admin' || user.role === 'moderator')) {
        roleMenu = (
            <Breadcrumbs aria-label="breadcrumb">
                <Link className={classes.link}
                      to="/lawyers_Rating/">
                    Рейтинг юристов
                </Link>
                <Link className={classes.link}
                      to="/under_consideration_questions">
                    Вопросы на рассмотрении
                </Link>
            </Breadcrumbs>
        );
    } else {
        roleMenu = (
            <Breadcrumbs aria-label="breadcrumb">
                <Link className={classes.link}
                      id={"add_application"}
                      to="/add_application">
                    <FormattedMessage
                        id="app.askQuestion_HeaderNavigation"
                        defaultMessage="Задать вопрос"/>
                </Link>
                <Link className={classes.link}
                      to="/lawyers_Rating/">
                    <FormattedMessage
                        id="app.findLawyer_HeaderNavigation"
                        defaultMessage="Найти юриста"/>
                </Link>
            </Breadcrumbs>
        );
    }


    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.inner}>
                    <Typography variant="h6" className={classes.title}>
                        <Link to="/" className={classes.sunush}>
                            <h2 className={classes.sunushTitle}>SUNUSH.KG</h2>
                        </Link>
                    </Typography>
                    <div>
                        {roleMenu}
                    </div>
                    <div className={classes.userMenuBox}>
                        <div>
                            {user && user.role ? <UserMenu user={user} className={classes.dropdownBtn}/> : <Anonymous/>}
                        </div>
                        <div>
                            <Button
                                aria-controls="simple-menu"
                                aria-haspopup="true"
                                onClick={handleClick}
                                className={classes.selectLanguageButton}
                            >
                                <div className={classes.iconBox}>
                                    <TranslateIcon/>
                                    <span className={classes.flagImage}>
                                        {context.locale === "ru-RU" ? <img src={rusFlag} alt="Russian flag"/> : null}
                                        {context.locale === "ky" ? <img src={kgFlag} alt="Kyrgyz flag"/> : null}
                                        {context.locale === "en-US" ? <img src={usFlag} alt="USA flag"/> : null}
                                    </span>
                                </div>
                            </Button>
                            <Select
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                                className={classes.selectLanguage}
                                value={context.locale}
                                onChange={context.selectLang}>
                                <MenuItem value={russianLanguageCode} onClick={changeCategoryTitleLanguage}>
                                    Русский язык
                                </MenuItem>
                                <MenuItem value={kyrgyzLanguageCode} onClick={changeCategoryTitleLanguage}>
                                    Кыргыз тили
                                </MenuItem>
                                <MenuItem value={englishLanguageCode} onClick={changeCategoryTitleLanguage}>
                                    English
                                </MenuItem>
                            </Select>
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default AppToolbar;