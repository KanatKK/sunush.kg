import React from 'react';
import Carousel from 'react-material-ui-carousel'
import {Paper, Button} from '@material-ui/core'
import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles({
    carousel: {
        background: 'white',
        position: 'fixed',
        zIndex: '20',
        width: '100%',
        marginTop: '30px',
        marginBottom: '20px'
    },
    img: {
        width: "100%",
        height: '100px'
    }
});

const CarouselMain = (props) => {
    const classes = useStyles();

    let items = [
        {
            // name: "Random Name #1",
            // description: "Probably the most random thing you have ever seen!",
            img: "https://www.popesculawgroup.com/wp-content/uploads/2013/03/business-law-wide.jpg"
        },
        {
            // name: "Random Name #2",
            // description: "Hello World!"
            img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr3fzyvvegx7SWtm_rBmF9sVbJtMV9mw75ow&usqp=CAU"
        }
    ];

    function Item(props) {
        return (
            <Paper>
                {/*<h2>{props.item.name}</h2>*/}
                {/*<p>{props.item.description}</p>*/}

                <img src={props.item.img} alt='carousel' className={classes.img}/>
            </Paper>
        )
    }

    return (
        <Carousel className={classes.carousel}
                  animation='fade'
                  indicators={false}
                  autoPlay
                  interval={3000}
                  navButtonsAlwaysInvisible={true}>
            {
                items.map((item, i) => <Item key={i} item={item}/>)
            }
        </Carousel>
    );
};

export default CarouselMain;