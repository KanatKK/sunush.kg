import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from "@material-ui/core/Typography";
import {useDispatch, useSelector} from 'react-redux';
import {successMessageToNull} from '../../store/actions/questionActions';
import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles({
    dialog: {
        position: 'absolute',
        top: "20%",
        right: '20%'
    }
});

export default function Modal() {
    const classes = useStyles();
    const message = useSelector(state => state.questions.successMessage);
    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch(successMessageToNull());
    };

    return (
        <div>
            <Dialog
                open={message}
                className={classes.dialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Успех"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Typography component={'span'} variant={'body2'}>
                            Ваша заявка отправлена и находится на рассмотрении у модераторов
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Закрыть
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}