import React from 'react';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
// import {FormattedMessage} from "react-intl";
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';

const useStyles = makeStyles({
    chats: {
        color: '#ffffff',
        fontSize: '16px',
        padding: '0'
    },
    chatItem: {
        display: 'flex',
        width: '100%',
        justifyContent: 'space-between',
        alignContent: 'center',
        margin: '10px'
    },
    remove: {
        color: 'black',
        marginLeft: '15px',
        '&:hover': {
            color: 'red'
        }
    },

});

const ChatMenu = (props) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const user = useSelector(state => state.user.user);
    const messageNotices = useSelector(state => state.notifications.messageNotices);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    let chatList;
    if (user && user.chats && user.chats.length !== 0) {
        chatList = user.chats.map(chat => {
            let notice = messageNotices.find(msgNot => msgNot.from === chat.with);

            return (
                <MenuItem key={chat.with} className={classes.chatItem}>

                    <Link
                        to={"/chat/" + chat.with}
                        onClick={handleClose}
                    >
                        <Badge color="secondary" variant="dot" invisible={!notice}>
                            {chat.nameWith}
                        </Badge>
                    </Link>

                </MenuItem>
            )
        })
    } else {
        chatList = (<MenuItem>
            Список чатов пока что пуст...
        </MenuItem>)
    }

    return (
        <>
            <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                className={classes.chats}
            >
                <Badge color="secondary" variant="dot" invisible={props.invisible}>
                    <MailIcon
                        onClick={props.click}
                    />
                </Badge>
            </Button>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                className={classes.menu}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {chatList}
            </Menu>
        </>
    );
};

export default ChatMenu;