import React from 'react';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {useDispatch} from "react-redux";
import {logout} from "../../store/actions/userActions";
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";

const UserMenu = ({user, className}) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const dispatch = useDispatch();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logoutUser = () => {
        dispatch(logout());
        window.location.reload();
    };


    return (
        <>
            <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                className={className}
            >
                {user.role === 'lawyer' ? user.fullName : user.fullName}
            </Button>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem component={Link} to={"/user/" + user._id}>
                    <FormattedMessage id="app.myOwnProfile" defaultMessage="Личный кабинет"/>
                </MenuItem>
                <MenuItem onClick={logoutUser}>
                    <FormattedMessage id="app.log_out" defaultMessage="Выйти из аккаунта"/>
                </MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;