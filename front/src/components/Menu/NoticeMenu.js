import React, {useEffect, useState} from 'react';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {useSelector, useDispatch} from "react-redux";
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {viewNotification} from "../../store/actions/notificationsActions";
// import {FormattedMessage} from "react-intl";
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';

const useStyles = makeStyles({
    chats: {
        color: '#ffffff',
        fontSize: '16px',
        padding: '0'
    },
    chatItem: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignContent: 'center',
        margin: '10px'
    },
    remove: {
        color: 'black',
        marginLeft: '15px',
        '&:hover': {
            color: 'red'
        }
    },
    status: {
        fontSize: '12px',
        color: 'darkgrey',
        fontStyle: 'italic'
    }
});

const NoticeMenu = (props) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const notifications = useSelector(state => state.notifications.notifications);
    const dispatch = useDispatch();
    const [count, setCount] = useState(0);

    useEffect(() => {
        const viewedCount = notifications.filter(notif => notif.viewed === false).length;
        setCount(viewedCount);
    }, [notifications]);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (id) => {
        dispatch(viewNotification(id));
        setAnchorEl(null);
    };

    let notificationList;
    if (notifications && notifications.length !== 0) {
        notificationList = notifications.map(notification => {
            return (
                <MenuItem key={notification._id} className={classes.chatItem}>
                    <p className={classes.status}>{notification.rusState}</p>
                    <Link
                        to={"/question/" + notification.question._id}
                        onClick={() => handleClose(notification._id)}
                    >
                        <Badge color="secondary" variant="dot" invisible={notification.viewed}>
                            {notification.question.title}
                        </Badge>
                    </Link>

                </MenuItem>
            )
        })
    } else {
        notificationList = (<MenuItem>
            Уведомлений пока нет...
        </MenuItem>)
    }

    return (
        <>
            <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
                className={classes.chats}
            >
                <Badge color="secondary" badgeContent={count}>
                    <NotificationsIcon
                        onClick={props.click}
                    />
                </Badge>
            </Button>
            <Menu
                anchorEl={anchorEl}
                keepMounted
                className={classes.menu}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {notificationList}
            </Menu>
        </>
    );
};

export default NoticeMenu;