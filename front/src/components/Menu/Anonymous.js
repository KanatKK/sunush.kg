import React from 'react';
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";
import {FormattedMessage} from "react-intl";

const Anonymous = () => {
  return (
      <div>
        <Button color="inherit" component={NavLink} to="/register">
            <FormattedMessage id="app.sign_up_HeaderNavigation" defaultMessage="Регистрация"/>
        </Button>
          <span>  |  </span>
        <Button color="inherit" component={NavLink} to="/login">
            <FormattedMessage id="app.sign_in_HeaderNavigation" defaultMessage="Вход"/>
        </Button>
      </div>
  );
};

export default Anonymous;