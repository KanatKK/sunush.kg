import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '20%',
        position: 'fixed',
        bottom: '30px',
        right: '30px'
    },
}));

const Notice = props => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Collapse in={props.open}>
                <Alert
                    severity="success"
                    action={
                        props.action
                    }
                >
                    Ваш вопрос был опубликован
                </Alert>
            </Collapse>
        </div>
    );
};

export default Notice;