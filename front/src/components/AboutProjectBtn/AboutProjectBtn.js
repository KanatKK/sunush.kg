import React from 'react';
import {Link} from "react-router-dom";
import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles({
    btn: {
        position: "fixed",
        bottom: "1%",
        right: "1%",
        display: "inline-block",
        fontSize: "12px",
        fontWeight: "bold",
        textDecoration: "none",
        userSelect: "none",
        width: "40px",
        height: "40px",
        outline: "none",
        border: "1px solid rgba(0,0,0,.1)",
        borderRadius: "50%",
        background: "linear-gradient(135deg, rgba(30,87,153,1) 0%,rgba(41,137,216,1) 50%,rgba(32,124,202,1) 51%,rgba(125,185,232,1) 100%)",
        opacity: "0.4",
        color: "white",
        cursor: "pointer",
        "&:hover": {
          opacity: "1"
        },
    },
});

const AboutProjectBtn = () => {
    const classes = useStyles();

    return (

            <Link
                to="/about_project"
            >
                <button type="button" className={classes.btn}>
                <p
                    style={{textAlign: "center"}}
                >
                    ?
                </p>
                </button>
            </Link>
    );
};

export default AboutProjectBtn;