import React from 'react';
import CardMedia from "@material-ui/core/CardMedia";
import defaultUserImage from "../../accepts/images/defUserImage.png";
import Rating from '@material-ui/lab/Rating';
import {Link as RouterLink} from "react-router-dom";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import {apiURL} from "../../constants";
import {makeStyles} from "@material-ui/core";
import {FormattedMessage} from "react-intl";
import Link from "@material-ui/core/Link";
const useStyle = makeStyles({

  userPhoto: {
    width: "70px",
    height: "70px",
    borderRadius: "50%",
    marginRight: "20px",
  },
  card: {
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
    padding: "15px",
    backgroundColor: "#f6f1f1",
    borderRadius: "6px",
    boxShadow: "5px 5px 17px 0px rgba(0, 0, 0, 0.73)",
  },
  link: {
    fontWeight: 'bold',
    fontSize: '20px'
  },
  fullNameLink: {
    color: 'black',
    textDecoration: "none",
  },
})

const UsersCard = ({fullName, city, categories, image, rating, id}) => {
  const classes = useStyle();

  let userImage = defaultUserImage;
  if(image){
    userImage = apiURL + "/uploads/" + image;
  }

  return (
      <div>
        <div className={classes.card}>
          <CardMedia className={classes.userPhoto} image={userImage}/>
          <div>
            <h3>
              <Link component={RouterLink}  to={"/user/" + id} className={classes.link}>
                {fullName}
              </Link>
            </h3>
            <p>
              <FormattedMessage id="app.city" defaultMessage="Город"/>
                : <b>{city}</b></p>
            <p>
              <FormattedMessage id="app.categories" defaultMessage="Категории"/>
              : {categories}</p>
            <Rating
                readOnly
                defaultValue={rating}
                precision={0.1}
                emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
          </div>
        </div>
      </div>
  );
};

export default UsersCard;