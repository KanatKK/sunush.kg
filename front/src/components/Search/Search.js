import React, {useState} from 'react';
import {Button, makeStyles} from '@material-ui/core';
import {FormattedMessage} from "react-intl";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import SearchIcon from "@material-ui/icons/Search";
import {useDispatch} from "react-redux";
import {searchQuestions} from "../../store/actions/questionActions";


const useStyles = makeStyles({
    buttons: {
        padding: "15px 15px",
        position: "absolute",
        marginLeft: "15px"
    },
    search: {
        width: '700px',
        marginLeft: '380px',
    },
    buttons2: {
        padding: "15px 15px",
        position: "absolute",
        marginLeft: "130px"
    }
})

const Search = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [searchText, setSearchText] = useState({
        text: ''
    });

    const searchForQuestionsInTheText = () => {
        dispatch(searchQuestions(searchText.text));
        setSearchText({
            ...searchText,
            text: '',
        });
    };

    const changeSearchText = event => {
        setSearchText({
            ...searchText,
            text: event.target.value,
        });
    };

    const clearSearchText = () => {
        setSearchText({
            ...searchText,
            text: "",
        });
    };
    return (
            <Grid item xs={12} className={classes.search}>
                <FormattedMessage id="app.text" defaultMessage="Введите ключевое слово для поиска:">
                    {(label) => (
                        <TextField
                            variant="outlined"
                            required= {true}
                            fullWidth
                            name="text"
                            onChange={changeSearchText}
                            value={searchText.text}
                            label={label}
                            type="text"
                            id="text"
                        />
                    )}
                </FormattedMessage>
                <Button variant="outlined" color="primary" onClick={searchForQuestionsInTheText} className={classes.buttons} type="button"><SearchIcon/>
                    <FormattedMessage id="app.search" defaultMessage="Поиск"/>
                </Button>
                <Button variant="outlined" color="primary" onClick={clearSearchText} className={classes.buttons2} type="button">
                    <FormattedMessage id="app.clear" defaultMessage="Очистить"/>
                </Button>
            </Grid>
    );
};

export default Search;