import React from 'react';
import Paper from "@material-ui/core/Paper";
import {Link} from "react-router-dom";
import {makeStyles} from '@material-ui/core';
import CardMedia from "@material-ui/core/CardMedia";
import defaultUserImage from '../../accepts/images/defUserImage.png';
import Chip from '@material-ui/core/Chip';
import {FormattedMessage} from "react-intl";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  delete: {
    '&:hover': {
      color: "red"
    }
  },
  public: {
    '&:hover': {
      color: "green"
    }
  },
  userBlock: {
    display: "flex",
    marginBottom: "20px"
  },
  userPhoto: {
    width: "50px",
    height: "50px",
    marginRight: "30px",
    borderWidth: "2px",
    borderColor: "#000000"
  },
  dateBlock: {
    display: "flex",
    flexDirection: "column",
  },
  questionBtn: {
    backgroundColor: "#aaa3a3",
    padding: "5px",
    textDecoration: "none",
    color: "#fff",
    borderRadius: "10px",
    position: "absolute",
    bottom: "15px",
    right: "25px",
  },
  date: {
    color: "#a08a8a"
  },
  userNameLink: {
    color: "black",
    textDecoration: "none",
  },
  dateTime: {
    marginRight: "5px"
  },
  questionTitle: {
    textDecoration: "none",
    color: "#324384",
  }
});

const QuestionCard = ({classname, description, username, datetime, id, published, title, userId, answers, image}) => {
  const classes = useStyles();


  return (
      <Paper className={classname}>
        <div className={classes.userBlock}>
          <CardMedia className={classes.userPhoto} image={image ? image : defaultUserImage}/>
          <div className={classes.dateBlock}>
            <b style={{fontSize: "18px"}}>
              <Link to={"/user/" + userId} className={classes.userNameLink}>
                {username}
              </Link>
            </b>
            {datetime && <i className={classes.date}>
              <span className={classes.dateTime}>
                <FormattedMessage id="app.date" defaultMessage="Дата"/>
                : {new Date(datetime).toISOString().slice(0, 10)}
              </span>
              <span className={classes.dateTime}>
                <FormattedMessage id="app.time" defaultMessage="Время"/>
                : {new Date(datetime).toISOString().slice(11, 19)}
              </span>

            </i>}
          </div>
        </div>
        <Typography
            variant="h4"
            className={classes.questionTitle}
            component={Link}
            to={"/question/" + id}
        >{title}</Typography>
        <p>{description.slice(0, 50)}...</p>
        {published &&
        <p>
          <FormattedMessage id="app.answers" defaultMessage="Ответы"/>
          ({answers})
        </p>}
        {!published ? <FormattedMessage id="app.notPublished" defaultMessage="Неопубликовано">
          {(label) => (
              <Chip label={label} variant="outlined" color="primary"/>
          )}
        </FormattedMessage> : null}
        <Link to={"/question/" + id} className={classes.questionBtn}>
          <FormattedMessage id="app.questionDetails" defaultMessage="Подробнее >"/>
        </Link>
      </Paper>
  );
};

export default QuestionCard;