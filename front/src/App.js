import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Container from "@material-ui/core/Container";
import {Switch, Route, Redirect} from "react-router";
import MainPage from "./containers/MainPage/MainPage";
import AddApplication from "./containers/AddApplication/AddApplication";
import MyApplications from "./containers/MyApplications/MyApplications";
import LawyersRating from "./containers/LawyersRating/LawyersRating";
import Register from "./containers/User/Register/Register";
import Login from "./containers/User/Login/Login";
import LawyerApplications from "./containers/LawyerApplications/LawyerApplications";
import LawyerAnswers from "./containers/LawyerAnswers/LawyerAnswers";
import QuestionPage from "./containers/QuestionPage/QuestionPage";
import UserProfile from "./containers/UserProfile/UserProfile";
import Chat from "./containers/Chat/Chat";
import MyProfilePage from "./containers/User/MyProfilePage/MyProfilePage";
import SortedQuestionsPage from "./containers/SortedQuestionsPage/SortedQuestionsPage";
import {useSelector} from "react-redux";
import UnderConsiderationQuestions from "./containers/UnderConsiderationQuestions/UnderConsiderationQuestions";
import EditQuestion from "./containers/EditQuestion/EditQuestion";
import FindAUser from "./components/ForgotYourPassword/FindAUser";
import ForgotYourPassword from "./components/ForgotYourPassword/ForgotYourPassword";
import AboutProject from "./containers/AboutProject/AboutProject";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

function App() {
    const user = useSelector(state => state.user.user);

    return (
        <>
            <CssBaseline/>
            <AppToolbar/>

            <main>

                    <Switch>
                        <Route path="/" exact component={MainPage}/>
                        <ProtectedRoute
                            path="/add_application"
                            exact
                            component={AddApplication}
                            isAllowed={user && user.role === "user"}
                        />
                        <Route path="/applications/:id" component={MyApplications}/>
                        <Route path="/sorting" component={SortedQuestionsPage}/>
                        <Route path="/lawyers_Rating" component={LawyersRating}/>
                        <Route path="/register" component={Register}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/forgotYourPassword" component={FindAUser}/>
                        <Route path="/setPassword" component={ForgotYourPassword}/>
                        <ProtectedRoute
                            path="/chat/:id"
                            exact
                            component={Chat}
                            isAllowed={user &&
                            (user.role === "lawyer" || user.role === "accountant" || user.role === "user")}
                        />
                        <ProtectedRoute
                            path="/lawyer_answers"
                            exact
                            component={LawyerAnswers}
                            isAllowed={user && (user.role === "lawyer" || user.role === "accountant")}
                        />
                        <ProtectedRoute
                            path="/lawyer_applications"
                            exact
                            component={LawyerApplications}
                            isAllowed={user && (user.role === "lawyer" || user.role === "accountant")}
                        />
                        <Route path="/question/:id" component={QuestionPage}/>
                        <Route path="/user/:id" component={UserProfile}/>
                        <Route path="/edit_my_profile" component={MyProfilePage}/>
                        <Route path="/under_consideration_questions" component={UnderConsiderationQuestions}/>
                        <Route path="/edit_question/:id" component={EditQuestion}/>
                        <Route path="/about_project" component={AboutProject}/>
                    </Switch>
            </main>
        </>
    );
}

export default App;
