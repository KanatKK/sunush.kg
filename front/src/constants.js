import  firebase from "firebase";

let host = "http://localhost";
let port = 8000;

if (process.env.REACT_APP_NODE_ENV === "test") {
    port = 8010;
}
if (process.env.REACT_APP_CI) {
    host = "http://188.166.48.52";
}

if (process.env.REACT_APP_NODE_ENV === "production") {
    host = "http://188.166.48.52";
    port = 81;
}

export const apiURL = host + ":" + port;
export const defaultAvatar =
    "https://www.unipulse.tokyo/en/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png";

let firebaseConfig = {
    apiKey: "AIzaSyAVK4ckORxPkm_vXsnvQ8X_b2HRDy9v0Ko",
    authDomain: "sunush-kg-e6f4b.firebaseapp.com",
    projectId: "sunush-kg-e6f4b",
    storageBucket: "sunush-kg-e6f4b.appspot.com",
    messagingSenderId: "396215848827",
    appId: "1:396215848827:web:c8bf43106ff768bc2c37de"
};

firebase.initializeApp(firebaseConfig);

export default firebase;