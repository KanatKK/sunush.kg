import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunkMiddleware from "redux-thunk";
import userReducer from './reducers/userReducer';
import { loadFromLocalStorage, saveToLocalStorage } from './localStorage';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import categoriesReducer from "./reducers/categoriesReducer";
import regionsReducer from "./reducers/regionsReducer";
import questionsReducer from "./reducers/questionsReducer";
import answersReducer from "./reducers/answersReducer";
import ratingReducer from "./reducers/ratingReducer";
import userListReducer from "./reducers/usersListReducer";
import chatReducer from "./reducers/chatReducer";
import notificationsReducer from "./reducers/notificationsReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  rating: ratingReducer,
  chat: chatReducer,
  user: userReducer,
  categories: categoriesReducer,
  regions: regionsReducer,
  questions: questionsReducer,
  notifications: notificationsReducer,
  answers: answersReducer,
  usersList: userListReducer,
  router: connectRouter(history)
});

const middleWare = [
  thunkMiddleware,
  routerMiddleware(history),
];

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(...middleWare)));

store.subscribe(() => {
  saveToLocalStorage({
    user: {
      user: store.getState().user.user
    }
  });
});


export default store;