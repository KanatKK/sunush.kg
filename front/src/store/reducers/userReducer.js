import {
    FETCH_EDIT_FAILURE,
    GET_USER,
    REGISTER_SUCCESS,
    REGISTER_FAILURE,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_USER,
    LOGOUT_USER_ERROR, GET_USER_PROFILE_DATA,
    CREATE_CHAT_WITH,
    REMOVE_CHAT_WITH, FIND_USER
} from "../actionsTypes";

const initialState = {
    user: null,
    userProfileData: [],
    loginError: null,
    registerError: null,
    logoutError: null,
    userInfoError: null,
    find_user: null,
    setPassword: null
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {...state, user: action.value, registerError: null, loginError: null};
        case REGISTER_FAILURE:
            return {...state, registerError: action.error};
        case LOGIN_SUCCESS:
            return {...state, user: action.value, registerError: null, loginError: null};
        case LOGIN_FAILURE:
            return {...state, loginError: action.error};
        case LOGOUT_USER:
            return {...state, user: []};
        case LOGOUT_USER_ERROR:
            return {...state, logoutError: action.error};
        case FETCH_EDIT_FAILURE:
            return {...state, userInfoError: action.userInfoError};
        case GET_USER:
            return {...state, user: action.userData};
        case FIND_USER:
            if(action.userData.length){
                return {...state, find_user: action.userData, setPassword: true};
            } else {
                return {...state, find_user: action.userData, setPassword: false};
            }
            case GET_USER_PROFILE_DATA:
            return {...state, userProfileData: action.userData};
        case CREATE_CHAT_WITH:
            const newUser = state.user;
            const newChat = newUser.chats.find(chat => chat.with === action.value.with);
            if (newChat) {
                return {...state};
            } else {
                newUser.chats.push(action.value);
                return {...state, user: newUser};
            }
        case REMOVE_CHAT_WITH:
            const newUserRemove = state.user;
            const newChatRemove = newUserRemove.chats.find(chat => chat.with === action.value);
            if (!newChatRemove) {
                return {...state};
            } else {
                const index = newUserRemove.chats.indexOf(newChatRemove);
                newUserRemove.chats.splice(index, 1);
                return {...state, user: newUserRemove};
            }
        default:
            return state;
    }
};

export default userReducer;