import {
    ADD_RATING_FAILURE
} from "../actionsTypes";

const initialState = {
    addRatingError: null,

};

const ratingReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_RATING_FAILURE:
            return {...state, addRatingError: action.error};
        default:
            return state;
    }
};

export default ratingReducer;