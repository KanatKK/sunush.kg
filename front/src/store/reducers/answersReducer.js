import {ADD_USER_ANSWERS_SUCCESS, GET_ANSWERS_FAILURE, GET_ANSWERS_SUCCESS, ADD_ANSWER_FAILURE, GET_QUESTION_ANSWERS} from "../actionsTypes";

const initialState = {
  userAnswers: [],
  answers: null,
  questionAnswers: null,
  error: null,
};

const answersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_QUESTION_ANSWERS:
      return {...state, questionAnswers: action.value};
    case ADD_ANSWER_FAILURE:
      return {...state, error: action.error};
    case ADD_USER_ANSWERS_SUCCESS:
      return {...state, userAnswers: action.answers};
    case GET_ANSWERS_SUCCESS:
      return {...state, answers: action.answers.length ? action.answers : null};
    case GET_ANSWERS_FAILURE:
      return {...state, error: action.error}
    default:
      return state;
  }
};

export default answersReducer;