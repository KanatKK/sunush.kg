import {GET_LAWYERS_LIST_SUCCESS, GET_SORTED_LAWYERS_LIST_SUCCESS} from "../actionsTypes";

const initialState = {
  users: [],
  lawyers: [],
  error: null,
};

const userListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LAWYERS_LIST_SUCCESS:
      return {...state, lawyers: action.lawyersData};
    case GET_SORTED_LAWYERS_LIST_SUCCESS:
      return {...state, lawyers: action.lawyers};
    default:
      return state;
  }
};

export default userListReducer;