import {
    GET_REGIONS_FAILURE,
    GET_REGIONS_SUCCESS,
    ADD_REGIONS_FAILURE,
    DELETE_REGIONS_FAILURE
} from "../actionsTypes";

const initialState = {
    regions: [],
    error: null,
    addError: null,
    deleteError: null
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_REGIONS_SUCCESS:
            return {...state, regions: action.regions, error: null};
        case GET_REGIONS_FAILURE:
            return {...state, error: action.error};
        case ADD_REGIONS_FAILURE:
            return {...state, addError: action.error};
        case DELETE_REGIONS_FAILURE:
            return {...state, deleteError: action.error};
        default:
            return state;
    }
};

export default categoriesReducer;