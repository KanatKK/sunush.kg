import {
    GET_NOTIFICATIONS_SUCCESS,
    GET_NOTIFICATIONS_FAILURE,
    VIEW_NOTIFICATION_FAILURE,
    VIEW_NOTIFICATION_SUCCESS,
    ADD_NOTIFICATION_SUCCESS,
    RECEIVE_NEW_MESSAGES,
    GET_MESSAGE_NOTIFICATIONS_FAILURE,
    GET_MESSAGE_NOTIFICATIONS_SUCCESS,
    REMOVE_MESSAGE_NOTIFICATIONS_FAILURE,
    REMOVE_MESSAGE_NOTIFICATIONS_SUCCESS,
} from "../actionsTypes";


const initialState = {
    notifications: [],
    getNotificationsError: null,
    viewedError: null,
    messageNotices: [],
    messageNoticesError: null,
    removeMessageNoticesError: null,
};

const notificationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_NOTIFICATIONS_FAILURE:
            return {...state, getNotificationsError: action.error};
        case GET_NOTIFICATIONS_SUCCESS:
            return {...state, notifications: action.value};
        case VIEW_NOTIFICATION_FAILURE:
            return {...state, viewedError: action.error};
        case VIEW_NOTIFICATION_SUCCESS:
            const newNotifications = [...state.notifications];
            const notification = newNotifications.find(notif => notif._id === action.id);
            const index = newNotifications.indexOf(notification);
            newNotifications[index].viewed = true;
            return {...state, notifications: newNotifications};
        case ADD_NOTIFICATION_SUCCESS:
            const newNot = [...state.notifications];
            newNot.unshift(action.data);
            return {...state, notifications: newNot};
        case RECEIVE_NEW_MESSAGES:
            const newMsg = [...state.messageNotices];
            newMsg.push(action.value);
            return {...state, messageNotices: newMsg};
        case GET_MESSAGE_NOTIFICATIONS_SUCCESS:
            return {...state, messageNotices: action.value};
        case GET_MESSAGE_NOTIFICATIONS_FAILURE:
            return {...state, messageNoticesError: action.error};
        case REMOVE_MESSAGE_NOTIFICATIONS_FAILURE:
            return {...state, removeMessageNoticesError: action.error};
        case REMOVE_MESSAGE_NOTIFICATIONS_SUCCESS:
            const newMsgNot = [...state.messageNotices];
            const newNotRemove = newMsgNot.find(notice => notice.from === action.value.from);
            if (!newNotRemove) {
                return {...state};
            } else {
                const index = newMsgNot.indexOf(newNotRemove);
                newMsgNot.splice(index, 1);
                return {...state, messageNotices: newMsgNot};
            }
        default:
            return {...state};
    }
};

export default notificationsReducer;