import {
    GET_CATEGORIES_FAILURE,
    GET_CATEGORIES_SUCCESS,
    ADD_CATEGORIES_FAILURE,
    DELETE_CATEGORIES_FAILURE
} from "../actionsTypes";

const initialState = {
    categories: [],
    error: null,
    addError: null,
    deleteError: null
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories, error: null};
        case GET_CATEGORIES_FAILURE:
            return {...state, error: action.error};
        case ADD_CATEGORIES_FAILURE:
            return {...state, addError: action.error};
        case DELETE_CATEGORIES_FAILURE:
            return {...state, deleteError: action.error};
        default:
            return state;
    }
};

export default categoriesReducer;