import {
    ADD_QUESTION_FAILURE,
    ADD_QUESTION_SUCCESS,
    GET_QUESTIONS_SUCCESS,
    GET_CATEGORIES_FAILURE,
    GET_SINGLE_QUESTION,
    SUCCESS_MESSAGE_TO_NULL,
    DELETE_QUESTION_FAILURE,
    DELETE_QUESTION_SUCCESS,
    PUBLIC_QUESTION_FAILURE,
    PUBLIC_QUESTION_SUCCESS,
    FETCH_QUESTIONS_CATEGORY,
    GET_SORTED_QUESTIONS_SUCCESS,
    GET_UNPUBLISHED_QUESTIONS_SUCCESS,
    GET_UNPUBLISHED_QUESTIONS_FAILURE, GET_QUESTIONS_FOR_A_LAWYER, GET_TOTAL_COUNT, USER_DELETED_QUESTIONS_SUCCESS,

} from "../actionsTypes";

const initialState = {
    questions: [],
    singleQuestion: [],
    unpublishedQuestions: [],
    totalQuestions: null,
    getQuestionError: null,
    error: null,
    successMessage: false,
    deleteQuestionError: null,
    publicQuestionError: null,
    category: null,
    sorted_questions: [],
    lawyer_questions: [],
    deletedQuestions: []
};

const questionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TOTAL_COUNT:
            return {...state, totalQuestions: action.value};
        case GET_UNPUBLISHED_QUESTIONS_FAILURE:
            return {...state, error: action.error};
        case GET_UNPUBLISHED_QUESTIONS_SUCCESS:
            return {...state, unpublishedQuestions: action.value};
        case GET_SINGLE_QUESTION:
            return {...state, singleQuestion: action.questionData};
        case GET_QUESTIONS_SUCCESS:
            return {...state, questions: action.questions, getQuestionError: null};
        case GET_QUESTIONS_FOR_A_LAWYER:
            return {...state, lawyer_questions: action.lawyer_questions, getQuestionError: null};
        case GET_SORTED_QUESTIONS_SUCCESS:
            return {...state, sorted_questions: action.questions.length ? action.questions : null, getQuestionError: null};
        case GET_CATEGORIES_FAILURE:
            return {...state, getQuestionError: action.error}
        case ADD_QUESTION_SUCCESS:
            return {...state, successMessage: true, error: null};
        case ADD_QUESTION_FAILURE:
            return {...state, error: action.error};
        case SUCCESS_MESSAGE_TO_NULL:
            return {...state, successMessage: false};
        case DELETE_QUESTION_SUCCESS:
            const question = state.questions.find(quest => quest._id === action.id);
            const index = state.questions.indexOf(question);
            const newQuestions = [...state.questions];
            newQuestions.splice(index, 1);
            return {...state, questions: newQuestions};
        case DELETE_QUESTION_FAILURE:
            return {...state, deleteQuestionError: action.error};
        case PUBLIC_QUESTION_FAILURE:
            return {...state, publicQuestionError: action.error};
        case PUBLIC_QUESTION_SUCCESS:
            const quest = state.questions.find(quest => quest._id === action.id);
            const ind = state.questions.indexOf(quest);
            const newQuests = [...state.questions];
            newQuests[ind].published = true;
            return {...state, questions: newQuests};
        case FETCH_QUESTIONS_CATEGORY:
            return {...state, category: action.category};
        case USER_DELETED_QUESTIONS_SUCCESS:
            return {...state, deletedQuestions: action.deletedQuestions};
        default:
            return state;
    }
};

export default questionsReducer;