import {
    GET_REGIONS_FAILURE,
    GET_REGIONS_SUCCESS,
    ADD_REGIONS_FAILURE,
    DELETE_REGIONS_FAILURE
} from "../actionsTypes";

import axiosApi from "../../axiosApi";

const getRegionsSuccess = regions => {
    return {type: GET_REGIONS_SUCCESS, regions};
};

const getRegionsFailure = error => {
    return {type: GET_REGIONS_FAILURE, error};
};

const addRegionsFailure = error => {
    return {type: ADD_REGIONS_FAILURE, error}
};

const deleteRegionsFailure = error => {
    return {type: DELETE_REGIONS_FAILURE, error}
};

export const getRegions = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/regions');
            dispatch(getRegionsSuccess(response.data));
        } catch (e) {
            dispatch(getRegionsFailure(e));
        }
    }
};

export const addRegion = (data) => {
    return async dispatch => {
        try {
            await axiosApi.post('/regions', {title: data});
        } catch (e) {
            dispatch(addRegionsFailure(e.response.error));
        }
    }
}

export const deleteRegion = (id) => {
    return async dispatch => {
        try {
            await axiosApi.delete('/regions/' + id);
        } catch (e) {
            dispatch(deleteRegionsFailure(e.response.error));
        }
    }
}