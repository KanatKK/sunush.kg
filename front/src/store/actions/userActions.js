import {push} from "connected-react-router";
import axiosApi from "../../axiosApi";
import {
    FETCH_EDIT_FAILURE,
    GET_USER,
    REGISTER_SUCCESS,
    REGISTER_FAILURE,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_USER,
    LOGOUT_USER_ERROR, GET_USER_PROFILE_DATA,
    CREATE_CHAT_WITH,
    REMOVE_CHAT_WITH, FIND_USER
} from "../actionsTypes";
import swal from 'sweetalert';


const registerSuccess = value => {
    return {type: REGISTER_SUCCESS, value};
};

const registerFailure = error => {
    return {type: REGISTER_FAILURE, error};
};

const loginSuccess = value => {
    return {type: LOGIN_SUCCESS, value};
};

const loginFailure = error => {
    return {type: LOGIN_FAILURE, error};
};

const logoutUser = () => {
    return {type: LOGOUT_USER};
};

const logoutUserError = error => {
    return {type: LOGOUT_USER_ERROR, error};
};

const fetchEditFailure = userInfoError => {
    return {type: FETCH_EDIT_FAILURE, userInfoError};
};

export const register = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/users', userData);
            dispatch(registerSuccess(response.data));
            dispatch(push('/'));
        } catch (error) {
            if (error.response === undefined) {
                dispatch(registerFailure({error: 'Something went wrong...'}));
            } else {
                dispatch(registerFailure(error.response.data.errors));
            }
        }
    };
};

export const login = userData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/users/sessions', userData);
            dispatch(loginSuccess(response.data));
            dispatch(push('/'));
        } catch (error) {
            if (error.response === undefined) {
                dispatch(loginFailure({error: 'Something went wrong...'}));
            } else {
                dispatch(loginFailure(error && error.response.data.error));
            }
        }
    };
};

export const logout = () => {
    return async (dispatch) => {
        try {
            await axiosApi.delete("/users/sessions");
            dispatch(logoutUser());
            dispatch(push("/"));
        } catch (e) {
            dispatch(logoutUserError(e));
        }
    }
};

const getUser = userData => {
    return {type: GET_USER, userData};
};

const findUserData = userData => {
    return {type: FIND_USER, userData};
};

export const getUserData = () => {
    return async (dispatch, getState) => {
        try {
            const id = getState().user.user._id;
            const response = await axiosApi.get("/users/" + id);
            await dispatch(getUser(response.data));
        } catch (e) {
            await dispatch(e.response.data);
        }
    }
};

const getUserProfileData = userData => {
    return {type: GET_USER_PROFILE_DATA, userData};
};

export const fetchGetUserProfileData = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/users/" + id);
            dispatch(getUserProfileData(response.data));
        } catch (e) {
            dispatch(push("/"));
        }
    };
};

export const editUserInfo = (inputData) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().user.user.token;
            const id = getState().user.user._id;
            const headers = {'Authorization': token};
            await axiosApi.put(`/users`, inputData, {headers});
            await dispatch(getUserData(id));
            await dispatch(push(`/user/` + id));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchEditFailure(e.response.data));
            } else {
                dispatch(fetchEditFailure({global: "No internet"}));
            }
        }
    };
};

const createChatWith = value => {
    return {type: CREATE_CHAT_WITH, value};
};

export const createChat = data => {
    return async dispatch => {
        try {
            await axiosApi.patch("users/chats", data);
            dispatch(createChatWith(data));
        } catch (e) {
            console.log(e);
        }
    };
};

const removeChatWith = value => {
    return {type: REMOVE_CHAT_WITH, value};
};

export const removeChat = value => {
  return async dispatch => {
      try {
          await axiosApi.patch("users/chats/remove/" + value);
          dispatch(removeChatWith(value));
          dispatch(push("/"));
      } catch(e) {
          console.log(e);
      }
  };
};

export const findUser = (phone) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/users?phone=" + phone);
            await dispatch(findUserData(response.data));
            if(!response.data.length) {
                swal("Провал :(", "Номер не найден!", "error");
            }
        } catch (e) {
            await dispatch(e.response.data);
        }
    }
};

export const editPassword = (password, token) => {
    return async dispatch => {
        try {
            const headers = {'Authorization': token};
            await axiosApi.put("/users/password", password, {headers});
            swal("Успех ! :(", "Пароль изменен!", "success");
            dispatch(push("/login"));
        } catch (e) {
            alert(321)
            if (e.response && e.response.data) {
                dispatch(fetchEditFailure(e.response.data));
            } else {
                dispatch(fetchEditFailure({global: "No internet"}));
            }
        }
    };
};