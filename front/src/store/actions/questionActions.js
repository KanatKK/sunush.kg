import {
    ADD_QUESTION_FAILURE,
    ADD_QUESTION_SUCCESS,
    GET_QUESTIONS_SUCCESS,
    GET_CATEGORIES_FAILURE,
    SUCCESS_MESSAGE_TO_NULL,
    DELETE_QUESTION_FAILURE,
    DELETE_QUESTION_SUCCESS,
    PUBLIC_QUESTION_FAILURE,
    PUBLIC_QUESTION_SUCCESS,
    GET_SINGLE_QUESTION,
    FETCH_QUESTIONS_CATEGORY,
    GET_SORTED_QUESTIONS_SUCCESS,
    GET_UNPUBLISHED_QUESTIONS_SUCCESS,
    GET_UNPUBLISHED_QUESTIONS_FAILURE, GET_QUESTIONS_FOR_A_LAWYER, GET_TOTAL_COUNT, USER_DELETED_QUESTIONS_SUCCESS
} from "../actionsTypes";
import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const getQuestionsSuccess = questions => {
    return {type: GET_QUESTIONS_SUCCESS, questions};
};

const getQuestionsForALawyerSuccess = lawyer_questions => {
    return {type: GET_QUESTIONS_FOR_A_LAWYER, lawyer_questions};
};

export const getSortedQuestionsSuccess = questions => {
    return {type: GET_SORTED_QUESTIONS_SUCCESS, questions};
};

const fetchQuestionsCategory = category => {
    return {type: FETCH_QUESTIONS_CATEGORY, category};
};

const getQuestionFailure = error => {
  return {type: GET_CATEGORIES_FAILURE, error};
};

const addQuestionSuccess = () => {
    return {type: ADD_QUESTION_SUCCESS};
};

const addQuestionFailure = error => {
    return {type: ADD_QUESTION_FAILURE, error};
};

export const successMessageToNull = () => {
    return {type: SUCCESS_MESSAGE_TO_NULL};
};

export const getTotalCount = value => {
    return{type: GET_TOTAL_COUNT, value}
}

export const getQuestions = (category, page) => {
    return async dispatch => {
        try {
            if (category) {
                const response = await axiosApi.get('/questions?category=' + category +'&page=' + page);
                dispatch(getQuestionsSuccess(response.data.docs));
                dispatch(fetchQuestionsCategory(category));
                dispatch(getTotalCount(response.data.totalDocs));
            } else {
                const response = await axiosApi.get('/questions?page=' + page);
                dispatch(getQuestionsSuccess(response.data.docs));
                dispatch(fetchQuestionsCategory(null));
                dispatch(getTotalCount(response.data.totalDocs));
            }
        } catch (e) {
            console.log(e)
        }
    };
};

export const getSortingQuestions = (category, region, answers, page) => {
    return async dispatch => {
        try {
            let query;
            if(category === "all_category"){
                query = ''
            }
            if (category !== "all_category") {
                query = category
            }
            if (category && region && answers) {
                const response = await axiosApi.get('/questions?category=' + query + "&region=" + region + "&answers=" + answers+ '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (category && region && !answers) {
                const response = await axiosApi.get('/questions?category=' + query + "&region=" + region+ '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (region && answers && !category) {
                const response = await axiosApi.get('/questions?region=' + region + "&answers=" + answers+ '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (category && answers && !region) {
                const response = await axiosApi.get('/questions?category=' + query + "&answers=" + answers + '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (category && !region && !answers) {
                const response = await axiosApi.get('/questions?category=' + query + '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (region && !category && !answers) {
                const response = await axiosApi.get('/questions?region=' + region + '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (answers && !category && !region) {
                const response = await axiosApi.get('/questions?answers=' + answers + '&page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (!category && !region && !answers) {
                const response = await axiosApi.get('/questions?page=' + page);
                dispatch(getSortedQuestionsSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
        } catch (e) {
            dispatch(getQuestionFailure(e));
        }
    };
};

export const getQuestionForALawyer = (category, date, answers, page) => {
    return async dispatch => {
        try {
            if (category && date && answers) {
                const response = await axiosApi.get('/questions/sort?category=' + category + "&date=" + date + "&answers=" + answers + '&page=' + page);
                dispatch(getQuestionsForALawyerSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (category && answers && !date) {
                const response = await axiosApi.get('/questions/sort?category=' + category + "&answers=" + answers + '&page=' + page);
                dispatch(getQuestionsForALawyerSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (category && date && !answers) {
                const response = await axiosApi.get('/questions/sort?category=' + category + "&date=" + date + '&page=' + page);
                dispatch(getQuestionsForALawyerSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
            if (!answers && !date && category) {
                const response = await axiosApi.get('/questions?category=' + category + '&page=' + page);
                console.log(response.data)
                dispatch(getQuestionsForALawyerSuccess(response.data.docs));
                dispatch(getTotalCount(response.data.totalDocs));
            }
        } catch (e) {
            dispatch(getQuestionFailure(e));
        }
    };
};

export const getMyQuestions = (id) => {
    return async (dispatch) => {
        if (id) {
            const response = await axiosApi.get("/questions/myQuestions/" + id);
            dispatch(getQuestionsSuccess(response.data));
        } else {
            dispatch(getQuestionsSuccess(null));
        }
    }
};

export const fetchAddQuestion = data => {
    return async dispatch => {
        try {
            await axiosApi.post("/questions", data);
            dispatch(addQuestionSuccess());
            dispatch(push("/"));
        } catch (e) {
            dispatch(addQuestionFailure(e && e.response.data));
        }
    };
};

const deleteQuestionSuccess = id => {
    return {type: DELETE_QUESTION_SUCCESS, id};
};

const deleteQuestionFailure = error => {
    return {type: DELETE_QUESTION_FAILURE, error};
};

export const deleteQuestion = (id) => {
    return async dispatch => {
        try {
            await axiosApi.delete('/questions/' + id);
            dispatch(deleteQuestionSuccess(id));
            dispatch(push("/under_consideration_questions"));
        } catch (e) {
            dispatch(deleteQuestionFailure(e))
        }
    }
};

const publicQuestionSuccess = id => {
    return {type: PUBLIC_QUESTION_SUCCESS, id};
};

const publicQuestionFailure = error => {
    return {type: PUBLIC_QUESTION_FAILURE, error};
};

export const publicQuestion = (id) => {
    return async dispatch => {
        try {
            await axiosApi.patch('/questions/' + id, {published: true});
            dispatch(push("/under_consideration_questions"));
            dispatch(publicQuestionSuccess(id));
        } catch (e) {
            dispatch(publicQuestionFailure(e));
        }
    }
}

const getSingleQuestion = questionData => {
    return {type: GET_SINGLE_QUESTION, questionData};
};

export const getQuestion = questionId => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/questions/" + questionId);
            dispatch(getSingleQuestion(response.data));
        } catch (e) {
            dispatch(getSingleQuestion(null));
        }
    }
}

export const getUnpublishedQuestionsSuccess = value => {
    return {type: GET_UNPUBLISHED_QUESTIONS_SUCCESS, value};
}

export const getUnpublishedQuestionsFailure = error => {
    return {type :GET_UNPUBLISHED_QUESTIONS_FAILURE, error};
};

export const fetchUnpublishedQuestions = (page) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/questions/unpublished/all?page=" + page);
            dispatch(getUnpublishedQuestionsSuccess(response.data.docs));
            dispatch(getTotalCount(response.data.totalDocs));
        } catch (e) {
            dispatch(getUnpublishedQuestionsFailure(e));
        }
    }
};

export const editQuestion = (id, data) => {
    return async dispatch => {
        try {
            await axiosApi.put("/questions/edit/" + id, data);
            dispatch(addQuestionSuccess());
            dispatch(push("/"));
        } catch (e) {
            dispatch(addQuestionFailure(e));
        }
    }
};

export const deleteUserQuestion = (id, userId) => {
    return async dispatch => {
        try {
            await axiosApi.delete("/questions/delete/" + id);
            dispatch(push("/applications/" + userId));
        } catch (e) {
            dispatch(deleteQuestionFailure(e));
        }
    }
};

const userDeletedQuestionsSuccess = (deletedQuestions) => {
    return {type: USER_DELETED_QUESTIONS_SUCCESS, deletedQuestions};
};

export const userDeletedQuestions = (userId) => {
    return async dispatch => {
        try{
            const response = await axiosApi.get("/questions/myQuestions/deleted/" + userId);
            dispatch(userDeletedQuestionsSuccess(response.data));
        }catch (e){
            dispatch(e.response.data)
        }
    }
}

export const searchQuestions = (text) => {
    return async dispatch => {
        try {
            if (text) {
                const response = await axiosApi.get('/get?text='+text);
                dispatch(getQuestionsSuccess(response.data));
            } else {
                const response = await axiosApi.get('/questions');
                dispatch(getQuestionsSuccess(response.data));
            }
        } catch (e) {
            console.log(e)
        }
    };
};