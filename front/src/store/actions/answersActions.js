import {
  ADD_USER_ANSWERS_SUCCESS,
  GET_QUESTION_ANSWERS,
  ADD_ANSWER_FAILURE,
  GET_ANSWERS_FAILURE,
  GET_ANSWERS_SUCCESS,
  GET_TOTAL_COUNT
} from "../actionsTypes";
import axiosApi from "../../axiosApi";

const addUserAnswersSuccess = answers => {
    return {type: ADD_USER_ANSWERS_SUCCESS, answers};
};

const getAnswersSuccess = answers => {
    return {type: GET_ANSWERS_SUCCESS, answers};
};

const getAnswersFailure = error => {
    return {type: GET_ANSWERS_FAILURE, error};
};

const addAnswerFailure = error => {
    return {type: ADD_ANSWER_FAILURE, error};
};

const getQuestionAnswers = value => {
    return {type: GET_QUESTION_ANSWERS, value};
};

const getTotalCount = value => {
  return{type: GET_TOTAL_COUNT, value}
}

export const addAnswersById = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/answers/?question=" + id);
            dispatch(addUserAnswersSuccess(response.data));
        } catch (e) {
            dispatch(e.response.data);
        }
    }
};

export const getMyAnswers = (userId, page) => {
  return async dispatch => {
    try {
      const response = await axiosApi.get('/answers?user=' + userId + '&page=' + page);
      dispatch(getAnswersSuccess(response.data[0]));
      dispatch(getTotalCount(response.data[1].totalCount));
    } catch (e) {
      dispatch(getAnswersFailure(e));
    }
  };
};


export const addAnswer = (data) => {
    return async dispatch => {
        try {
            await axiosApi.post("/answers/", data);
        } catch (e) {
            return dispatch => {
                if (e.response && e.response.data) {
                    dispatch(addAnswerFailure(e.response.data.error));
                }
            }
        }
    }
};

export const fetchQuestionAnswers = (id) => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/answers/question/" + id);
            dispatch(getQuestionAnswers(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};