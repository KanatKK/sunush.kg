import {GET_LAWYERS_LIST_FAILURE, GET_LAWYERS_LIST_SUCCESS, GET_SORTED_LAWYERS_LIST_SUCCESS} from "../actionsTypes";
import axiosApi from "../../axiosApi";
import {getTotalCount} from "./questionActions";

const getLawyersListSuccess = lawyersData => {
  return {type: GET_LAWYERS_LIST_SUCCESS, lawyersData};
};

const getSortedLawyersListSuccess = lawyers => {
  return {type: GET_SORTED_LAWYERS_LIST_SUCCESS, lawyers};
};

const getLawyersListFailure = error => {
  return {type: GET_LAWYERS_LIST_FAILURE, error};
};

export const getLawyersList = () => {
  return async dispatch => {
    try{
      const response = await axiosApi.get("/users?role=lawyer_accountant");
      dispatch(getLawyersListSuccess(response.data));
    }catch (e) {
      dispatch(e.response.data);
    }
  }
};

export const getSortedLawyersList = (category, region, role, page) => {
  return async dispatch => {
    try {
      if (category && region && role) {
        const response = await axiosApi.get('/users?role='+ role +'&category=' + category + "&region=" + region + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (category && region && !role) {
        const response = await axiosApi.get('/users?role=lawyer_accountant&category=' + category + "&region=" + region + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (category && role && !region) {
        const response = await axiosApi.get('/users?role=' + role + '&category=' + category + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (region && role && !category) {
        const response = await axiosApi.get('/users?role='+ role +'&region=' + region + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (category && !region && !role) {
        const response = await axiosApi.get('/users?role=lawyer_accountant&category=' + category + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (role && !category && !region) {
        const response = await axiosApi.get('/users?role=' + role + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (region && !role && !category) {
        const response = await axiosApi.get('/users?role=lawyer_accountant&region=' + region + "&page=" + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
      if (!category && !region && !role) {
        const response = await axiosApi.get('/users?role=lawyer_accountant&page=' + page);
        dispatch(getSortedLawyersListSuccess(response.data[0]));
        dispatch(getTotalCount(response.data[1].totalDocs));
      }
    } catch (e) {
      dispatch(getLawyersListFailure(e));
    }
  };
};