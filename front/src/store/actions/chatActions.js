import {
    GET_MESSAGES_SUCCESS,
    POST_MESSAGES_SUCCESS,
    GET_INTERLOCUTOR_SUCCESS,
    RECEIVE_NEW_MESSAGES, SEND_MESSAGE_FAILURE
} from "../actionsTypes";
import axiosApi from "../../axiosApi";


export const getMessages = value => {
    return {type: GET_MESSAGES_SUCCESS, value};
};

export const postMessage = value => {
    return {type: POST_MESSAGES_SUCCESS, value};
};

const getInterlocutorSuccess = (data) => {
    return {type: GET_INTERLOCUTOR_SUCCESS, data};
};

export const getInterlocutor = id => {
    return async (dispatch) => {
        try {
            const response = await axiosApi.get("/users/" + id);
            await dispatch(getInterlocutorSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const receiveNewMessages = (value) => {
    return {type: RECEIVE_NEW_MESSAGES, value};
};

const sendMessageError = error => {
    return {type: SEND_MESSAGE_FAILURE, error};
};

export const sendMessage = data => {
    return async dispatch => {
        try {
            await axiosApi.post("/messages", data);
        }  catch (e) {
            dispatch(sendMessageError(e));
        }
    };
};