import {
    GET_NOTIFICATIONS_SUCCESS,
    GET_NOTIFICATIONS_FAILURE,
    VIEW_NOTIFICATION_SUCCESS,
    VIEW_NOTIFICATION_FAILURE,
    ADD_NOTIFICATION_SUCCESS,
    RECEIVE_NEW_MESSAGES,
    GET_MESSAGE_NOTIFICATIONS_FAILURE,
    GET_MESSAGE_NOTIFICATIONS_SUCCESS,
    REMOVE_MESSAGE_NOTIFICATIONS_FAILURE,
    REMOVE_MESSAGE_NOTIFICATIONS_SUCCESS,
} from "../actionsTypes";

import axiosApi from "../../axiosApi";

const getNotificationsSuccess = value => {
    return {type: GET_NOTIFICATIONS_SUCCESS, value};
};

const getNotificationsFailure = error => {
    return {type: GET_NOTIFICATIONS_FAILURE, error};
};

export const getNotifications = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/notifications/" + id);
            dispatch(getNotificationsSuccess(response.data));
        } catch (e) {
            dispatch(getNotificationsFailure(e));
        }
    };
};

const viewNotificationSuccess = id => {
    return {type: VIEW_NOTIFICATION_SUCCESS, id};
};

const viewNotificationFailure = error => {
    return {type: VIEW_NOTIFICATION_FAILURE, error}
};

export const viewNotification = id => {
    return async dispatch => {
        try {
            await axiosApi.patch("/notifications/" + id, {viewed: true});
            dispatch(viewNotificationSuccess(id));
        } catch (e) {
            dispatch(viewNotificationFailure(e));
        }
    };
};

export const addNotificationSuccess = data => {
    return {type: ADD_NOTIFICATION_SUCCESS, data};
};

export const receiveNewMessages = value => {
    return {type: RECEIVE_NEW_MESSAGES, value};
};

const getMessageNoticesSuccess = value => {
    return {type: GET_MESSAGE_NOTIFICATIONS_SUCCESS, value};
};

const getMessageNoticesFailure = error => {
    return {type: GET_MESSAGE_NOTIFICATIONS_FAILURE, error};
};

export const getMessageNotices = () => {
  return async dispatch => {
      try {
          const result = await axiosApi.get("/newMessageNotices");
          dispatch(getMessageNoticesSuccess(result.data));
      } catch (e) {
          dispatch(getMessageNoticesFailure(e));
      }
  }
};

const removeMessageNoticesSuccess = value => {
    return {type: REMOVE_MESSAGE_NOTIFICATIONS_SUCCESS, value};
};

const removeMessageNoticesFailure = error => {
    return {type: REMOVE_MESSAGE_NOTIFICATIONS_FAILURE, error};
};

export const removeMessageNotification = value => {
    return async dispatch => {
        try {
            dispatch(removeMessageNoticesSuccess(value));
            await axiosApi.delete("/newMessageNotices", value);
        } catch (e) {
            dispatch(removeMessageNoticesFailure(e))
        }
    }
};
