import {
    ADD_RATING_FAILURE

} from "../actionsTypes";
import axiosApi from "../../axiosApi";

const addRatingFailure = error => {
    return {type: ADD_RATING_FAILURE, error};
};

export const addRating = data => {
    return async dispatch => {
        try {
            await axiosApi.post("/ratings", data);
        } catch (e) {
            dispatch(addRatingFailure(e));
        }
    }
};