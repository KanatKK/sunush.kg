import {
    GET_CATEGORIES_FAILURE,
    GET_CATEGORIES_SUCCESS,
    ADD_CATEGORIES_FAILURE,
    DELETE_CATEGORIES_FAILURE
} from "../actionsTypes";
import axiosApi from "../../axiosApi";

const getCategoriesSuccess = categories => {
    return {type: GET_CATEGORIES_SUCCESS, categories};
};

const getCategoriesFailure = error => {
    return {type: GET_CATEGORIES_FAILURE, error};
};

const addCategoriesFailure = error => {
  return {type: ADD_CATEGORIES_FAILURE, error}
};

const deleteCategoriesFailure = error => {
    return {type: DELETE_CATEGORIES_FAILURE, error}
};

export const getCategories = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('/categories');
            dispatch(getCategoriesSuccess(response.data));
        } catch (e) {
            dispatch(getCategoriesFailure(e));
        }
    }
}

export const addCategory = (data) => {
    return async dispatch => {
        try {
            await axiosApi.post('/categories', data);
        } catch (e) {
            dispatch(addCategoriesFailure(e));
        }
    }
}

export const deleteCategory = (id) => {
    return async dispatch => {
        try {
            await axiosApi.delete('/categories/' + id);
        } catch (e) {
            dispatch(deleteCategoriesFailure(e.response.error));
        }
    }
}