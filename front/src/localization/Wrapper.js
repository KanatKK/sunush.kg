import React, {useState} from "react";
import Russian from "../localization/russianLanguage.json";
import Kyrgyz from "../localization/kyrgyzLanguage.json";
import English from "../localization/englishLanguage.json";
import {IntlProvider} from "react-intl";

export const Context = React.createContext();
export const russianLanguageCode = 'ru-RU';
export const kyrgyzLanguageCode = 'ky';
export const englishLanguageCode = 'en-US';

let local;
let lang;
export const localStorageLanguage = localStorage.getItem("language");

if(!localStorageLanguage){
    local = navigator.language;
}else{
    local = localStorageLanguage;
}

if(local === kyrgyzLanguageCode){
    lang = Kyrgyz;
}else if(local === russianLanguageCode){
    lang = Russian;
}else if(local === englishLanguageCode) {
    lang = English;
}
const Wrapper = props => {
    const [locale, setLocale] = useState(local);
    const [messages, setMessages] = useState(lang);

    const selectLang = event => {
        const newLocale = event.target.value;
        setLocale(newLocale);
        if (newLocale === russianLanguageCode) {
            setMessages(Russian);
        } else if(newLocale === kyrgyzLanguageCode){
            setMessages(Kyrgyz);
        } else if(newLocale === englishLanguageCode){
            setMessages(English);
        }
        localStorage.setItem("language", newLocale);
    }

    return (
        <Context.Provider value={{locale, selectLang}}>
            <IntlProvider messages={messages} locale={locale}>
                {props.children}
            </IntlProvider>
        </Context.Provider>
    );
};

export default Wrapper;
