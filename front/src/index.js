import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from './store/configureStore';
import axiosApi from "./axiosApi";
import Wrapper from "./localization/Wrapper";

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().user.user.token;
    } catch(e) {
        // no token exists
    }
    return config;
});

ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
          <Wrapper>
              <App/>
          </Wrapper>
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

reportWebVitals();
