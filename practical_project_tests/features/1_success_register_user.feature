# language: ru

Функция: Успешная регистрация
  Находясь на сайте под анонимным пользователем
  Пытаюсь зарегестрироваться на сайте через форму регистрации
  Данные должны сохраняться в базе

  Сценарий: Успешное регистрация нового пользователя
    Допустим я нахожусь на странице "/register"
    Если я ввожу "user3" в поле "username"
    И я ввожу "12345678Kk" в поле "password"
    И я нажимаю на кнопку "register"
    И я вижу текст "USER3"

