##Установка
Для установки docker и docker-compose, нам нужны команды:
1. `sudo apt install docker.io`
2. `sudo apt update`
3. `sudo apt install curl`
4. `sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
5. `sudo chmod +x /usr/local/bin/docker-compose`

Все эти команды можно прописывать не в самом проекте, потому что docker и docker-compose - глобальные программные обеспечения, т.е установка идёт не в папку package.json, а в сам ноутбук.

## Запуск

Для запуска нам потребуйется зайти в папку sunush-composer и прописать команду: \
`sudo docker-compose up --build` \
Если впервые запускаете проект с помощью docker-compose, то вам стоит немного подождать :) Ибо docker загружает в контейнер образы node, mongo, front и back. Весят они достаточно много(в общем 4 гб). В дальнейшем проект будет запускаться быстрее, если вы не удаляли один из этих образов. \
Docker не может запустить фикстуры сам. Поэтому нужно сделать это вручную. 
Командой \
`sudo docker exec -it backend_container_ID sh` (получить id контейнера бэкенда можно с помощью команды `sudo docker container ls -a`) можно зайти в контейнер бекэнда и запустить фикстуры уже знакомой нам командой \
`npm run seed`. \
Прописываем "exit". \
Всё! Проект готов к работе :)

##Ошибки
1. ###### error : no space left on device
Если при установке образов выходит ошибка \
`error : no space left on device`
, то это значит что у вас не хватает места на ноутбуке. Попробуйте очистить примерно 6 гб и повторно запустите проэкт. Советую проверить список образов командой \
`sudo docker images` и удалить образы с названием none (если есть такие) командой `sudo docker rmi --force image_ID`. Вместо image_ID прописываем id образа с именем none.  

2. ###### MongoNetworkError
После установки образов, docker начинает запуск контейнеров. Бывает, что docker начинает запуск api и выходит ошибка о том, что бэк не может подключится к mongo. Происходит это из-за того, что docker запускает сначало бэк, потом уже приступает к mongo. Нужно зайти в список контейнеров командой \
`sudo docker container ls -a` и найти контейнер с бэкэндом. Необходимо прописать команду \
`sudo docker container start ID`.

##Полезные команды 

1. `sudo docker-compose up --build` - запуск проекта.
2. `sudo docker container ls -a` - показ списка контейнеров.
3. `sudo docker images` - показ списка образов. 
4. `sudo docker rmi --force *image_ID*` - удаление образа.
5. `sudo docker container rm *container_ID*` - удаление контейнера.
6. `sudo docker container stop *container_ID*` - остановка контейнера.
7. `sudo docker container start *container_ID*` - запуск контейнера.
8. `sudo docker exec -it backend_ID sh` - команда для запуска фикстур.